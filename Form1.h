//***********************************************************************
//Form1.h
//������������ ���� � ������������ � ���������
//��������� WindowsFomrs
//***********************************************************************
#pragma once
#include "Render.h"

namespace HelloWorldWindow {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	///
	/// ��������! ��� ��������� ����� ����� ������ ���������� ����� ��������
	///          �������� ����� ����� �������� ("Resource File Name") ��� �������� ���������� ������������ �������,
	///          ���������� �� ����� ������� � ����������� .resx, �� ������� ������� ������ �����. � ��������� ������,
	///          ������������ �� ������ ��������� �������� � ���������������
	///          ���������, ��������������� ������ �����.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		static Form1 ^Instance;
		Form1(void)
		{
			Instance = this;
			InitializeComponent();
			//window = gcnew COpenGL(this, 0, 0, 200, 200);
			Render *r = new Render((HWND)panel1->Handle.ToPointer());
		}


	protected:
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	protected: 

	private: System::Windows::Forms::TextBox^  txtRes;



	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;


	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  ����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  �������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  �����ToolStripMenuItem;
	private: System::Windows::Forms::StatusStrip^  statusStrip1;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;
	public: System::Windows::Forms::PropertyGrid^  propertyGrid1;


	private: System::Windows::Forms::ToolStrip^  toolStrip1;

	private: System::Windows::Forms::ToolStripButton^  toolStripButton2;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton3;
	private: System::Windows::Forms::ToolStripButton^  FurnitureModeButton;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������������ToolStripMenuItem;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	public: System::Windows::Forms::ToolStripLabel^  toolStripLabel1;
	private: System::Windows::Forms::ToolStripDropDownButton^  toolStripDropDownButton1;
	public: System::Windows::Forms::ToolStripMenuItem^  ����1ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����2ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����3ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
	private: System::Windows::Forms::ToolStripButton^  PeopleModeButton;
	private: System::Windows::Forms::ToolStripMenuItem^  ��������������ToolStripMenuItem;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog2;
	public: System::Windows::Forms::ToolStripMenuItem^  ����4ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����5ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����6ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����7ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����8ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����9ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����10ToolStripMenuItem;
	public: System::Windows::Forms::ToolStripMenuItem^  ����11ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  IDModeButton;
	public: System::Windows::Forms::ToolStripButton^  MarkingModeButton;
	private: System::Windows::Forms::ToolStripMenuItem^  ������������������ToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog2;
	private: System::Windows::Forms::ToolStripMenuItem^  �������������������ToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog3;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton1;
	private: System::Windows::Forms::ToolStripDropDownButton^  toolStripDropDownButton2;
	private: System::Windows::Forms::ToolStripMenuItem^  ��������ToolStripMenuItem;
	public: System::Windows::Forms::Panel^  panel2;
	public: System::Windows::Forms::Button^  button2;
	public: System::Windows::Forms::Button^  button1;
	public: System::Windows::Forms::TextBox^  textBox1;
	public: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ToolStripMenuItem^  �������ToolStripMenuItem;
	public: System::Windows::Forms::Button^  button4;
	public: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::ToolStripMenuItem^  �������������������ToolStripMenuItem;
	public: System::Windows::Forms::Panel^  panel3;
	public: System::Windows::Forms::Label^  label2;
	public: System::Windows::Forms::Panel^  panel4;
	public: 
	public: System::Windows::Forms::TextBox^  textBox2;
	public: System::Windows::Forms::Label^  label3;
	public: System::Windows::Forms::Button^  button6;
	public: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::ToolStripMenuItem^  ��������������ToolStripMenuItem;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog3;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;


public: System::Windows::Forms::Label^  label4;
public: System::Windows::Forms::ToolStripMenuItem^  ����������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  �������������ToolStripMenuItem;
public: 
private: System::Windows::Forms::ToolStripMenuItem^  ������������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ����������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ��������ToolStripMenuItem;
public: System::Windows::Forms::ToolStripButton^  toolStripButton4;
private: System::Windows::Forms::ToolStripMenuItem^  ����������������ToolStripMenuItem;
private: System::Windows::Forms::OpenFileDialog^  openFileDialog4;
public: 
	public: 

	public: 









	private: System::Windows::Forms::Panel^  panel1;




	private:


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->txtRes = (gcnew System::Windows::Forms::TextBox());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->panel4 = (gcnew System::Windows::Forms::Panel());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->propertyGrid1 = (gcnew System::Windows::Forms::PropertyGrid());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripButton2 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton3 = (gcnew System::Windows::Forms::ToolStripButton());
			this->FurnitureModeButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->PeopleModeButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripDropDownButton2 = (gcnew System::Windows::Forms::ToolStripDropDownButton());
			this->��������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->IDModeButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton1 = (gcnew System::Windows::Forms::ToolStripButton());
			this->MarkingModeButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton4 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripLabel1 = (gcnew System::Windows::Forms::ToolStripLabel());
			this->toolStripDropDownButton1 = (gcnew System::Windows::Forms::ToolStripDropDownButton());
			this->����1ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����2ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����3ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����4ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����5ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����6ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����7ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����8ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����9ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����10ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����11ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->saveFileDialog2 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->openFileDialog2 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->openFileDialog3 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->saveFileDialog3 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->openFileDialog4 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1->SuspendLayout();
			this->panel1->SuspendLayout();
			this->panel4->SuspendLayout();
			this->panel3->SuspendLayout();
			this->panel2->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->contextMenuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// txtRes
			// 
			this->txtRes->Enabled = false;
			this->txtRes->Location = System::Drawing::Point(1222, 436);
			this->txtRes->Multiline = true;
			this->txtRes->Name = L"txtRes";
			this->txtRes->ReadOnly = true;
			this->txtRes->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->txtRes->Size = System::Drawing::Size(122, 95);
			this->txtRes->TabIndex = 2;
			this->txtRes->Visible = false;
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"13gymnasium11.xml";
			this->openFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog1_FileOk);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->����ToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1356, 24);
			this->menuStrip1->TabIndex = 7;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// ����ToolStripMenuItem
			// 
			this->����ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {this->�������ToolStripMenuItem, 
				this->�������������������ToolStripMenuItem, this->������������������ToolStripMenuItem, this->����������������ToolStripMenuItem, 
				this->���������������ToolStripMenuItem, this->��������������ToolStripMenuItem, this->��������������ToolStripMenuItem, this->�����ToolStripMenuItem});
			this->����ToolStripMenuItem->Name = L"����ToolStripMenuItem";
			this->����ToolStripMenuItem->Size = System::Drawing::Size(48, 20);
			this->����ToolStripMenuItem->Text = L"����";
			// 
			// �������ToolStripMenuItem
			// 
			this->�������ToolStripMenuItem->Name = L"�������ToolStripMenuItem";
			this->�������ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->�������ToolStripMenuItem->Text = L"������� ���� � ����������";
			this->�������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::�������ToolStripMenuItem_Click);
			// 
			// �������������������ToolStripMenuItem
			// 
			this->�������������������ToolStripMenuItem->Name = L"�������������������ToolStripMenuItem";
			this->�������������������ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->�������������������ToolStripMenuItem->Text = L"������� ���� � �������";
			this->�������������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::�������������������ToolStripMenuItem_Click);
			// 
			// ������������������ToolStripMenuItem
			// 
			this->������������������ToolStripMenuItem->Name = L"������������������ToolStripMenuItem";
			this->������������������ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->������������������ToolStripMenuItem->Text = L"������� ���� � ������";
			this->������������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::������������������ToolStripMenuItem_Click);
			// 
			// ����������������ToolStripMenuItem
			// 
			this->����������������ToolStripMenuItem->Name = L"����������������ToolStripMenuItem";
			this->����������������ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->����������������ToolStripMenuItem->Text = L"������� ���� � �������";
			this->����������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����������������ToolStripMenuItem_Click);
			// 
			// ���������������ToolStripMenuItem
			// 
			this->���������������ToolStripMenuItem->Name = L"���������������ToolStripMenuItem";
			this->���������������ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->���������������ToolStripMenuItem->Text = L"��������� ������";
			this->���������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::���������������ToolStripMenuItem_Click);
			// 
			// ��������������ToolStripMenuItem
			// 
			this->��������������ToolStripMenuItem->Name = L"��������������ToolStripMenuItem";
			this->��������������ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->��������������ToolStripMenuItem->Text = L"��������� �����";
			this->��������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::��������������ToolStripMenuItem_Click);
			// 
			// ��������������ToolStripMenuItem
			// 
			this->��������������ToolStripMenuItem->Name = L"��������������ToolStripMenuItem";
			this->��������������ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->��������������ToolStripMenuItem->Text = L"��������� �����";
			this->��������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::��������������ToolStripMenuItem_Click);
			// 
			// �����ToolStripMenuItem
			// 
			this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
			this->�����ToolStripMenuItem->Size = System::Drawing::Size(230, 22);
			this->�����ToolStripMenuItem->Text = L"�����";
			this->�����ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::�����ToolStripMenuItem_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Location = System::Drawing::Point(0, 509);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(1356, 22);
			this->statusStrip1->TabIndex = 8;
			this->statusStrip1->Text = L"gfhfghg";
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 16;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->panel4);
			this->panel1->Controls->Add(this->panel3);
			this->panel1->Controls->Add(this->panel2);
			this->panel1->Location = System::Drawing::Point(12, 52);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(1140, 622);
			this->panel1->TabIndex = 10;
			this->panel1->MouseWheel += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::panel1_MouseWheel);
			this->panel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::panel1_Paint);
			this->panel1->PreviewKeyDown += gcnew System::Windows::Forms::PreviewKeyDownEventHandler(this, &Form1::panel1_PreviewKeyDown);
			this->panel1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::panel1_MouseMove);
			this->panel1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::panel1_MouseClick);
			this->panel1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::panel1_MouseDown);
			this->panel1->Enter += gcnew System::EventHandler(this, &Form1::panel1_Enter);
			this->panel1->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::panel1_MouseUp);
			// 
			// panel4
			// 
			this->panel4->Controls->Add(this->button6);
			this->panel4->Controls->Add(this->button5);
			this->panel4->Controls->Add(this->textBox2);
			this->panel4->Controls->Add(this->label3);
			this->panel4->Location = System::Drawing::Point(248, 3);
			this->panel4->Name = L"panel4";
			this->panel4->Size = System::Drawing::Size(311, 118);
			this->panel4->TabIndex = 2;
			this->panel4->Visible = false;
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(29, 86);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 23);
			this->button6->TabIndex = 3;
			this->button6->Text = L"������";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Visible = false;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(202, 86);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(75, 23);
			this->button5->TabIndex = 2;
			this->button5->Text = L"����";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Visible = false;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(102, 40);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 1;
			this->textBox2->Text = L"0";
			this->textBox2->Visible = false;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(1, 11);
			this->label3->MaximumSize = System::Drawing::Size(307, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(307, 26);
			this->label3->TabIndex = 0;
			this->label3->Text = L"��� ������� ��������� ����� ��� ����������� (��� / �^2) ��� ����� ���������� ����" 
				L"� ������������ �������";
			this->label3->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->label3->Visible = false;
			// 
			// panel3
			// 
			this->panel3->Controls->Add(this->label2);
			this->panel3->Location = System::Drawing::Point(0, 0);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(302, 42);
			this->panel3->TabIndex = 1;
			this->panel3->Visible = false;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(20, 13);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(268, 13);
			this->label2->TabIndex = 0;
			this->label2->Text = L"�������� �������, �� ������� ����������� �����";
			this->label2->Visible = false;
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->button4);
			this->panel2->Controls->Add(this->button3);
			this->panel2->Controls->Add(this->button2);
			this->panel2->Controls->Add(this->button1);
			this->panel2->Controls->Add(this->textBox1);
			this->panel2->Controls->Add(this->label1);
			this->panel2->Location = System::Drawing::Point(3, 3);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(242, 118);
			this->panel2->TabIndex = 0;
			this->panel2->Visible = false;
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(164, 86);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 5;
			this->button4->Text = L"����";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Visible = false;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(6, 86);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 4;
			this->button3->Text = L"������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Visible = false;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(162, 57);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 3;
			this->button2->Text = L"����";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Visible = false;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(6, 57);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Visible = false;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(74, 31);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 1;
			this->textBox1->Text = L"0";
			this->textBox1->Visible = false;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(3, 11);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(234, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"������� ���������� ����� ��� �����������";
			this->label1->Visible = false;
			// 
			// propertyGrid1
			// 
			this->propertyGrid1->Location = System::Drawing::Point(1158, 52);
			this->propertyGrid1->Name = L"propertyGrid1";
			this->propertyGrid1->Size = System::Drawing::Size(190, 401);
			this->propertyGrid1->TabIndex = 9;
			this->propertyGrid1->TabStop = false;
			// 
			// toolStrip1
			// 
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(13) {this->toolStripButton2, 
				this->toolStripButton3, this->FurnitureModeButton, this->PeopleModeButton, this->toolStripDropDownButton2, this->IDModeButton, 
				this->toolStripButton1, this->MarkingModeButton, this->toolStripButton4, this->toolStripSeparator1, this->toolStripLabel1, this->toolStripDropDownButton1, 
				this->toolStripSeparator2});
			this->toolStrip1->Location = System::Drawing::Point(0, 24);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(1356, 25);
			this->toolStrip1->TabIndex = 11;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// toolStripButton2
			// 
			this->toolStripButton2->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton2->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton2.Image")));
			this->toolStripButton2->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton2->Name = L"toolStripButton2";
			this->toolStripButton2->Size = System::Drawing::Size(23, 22);
			this->toolStripButton2->Text = L"���������";
			this->toolStripButton2->Click += gcnew System::EventHandler(this, &Form1::toolStripButton2_Click);
			// 
			// toolStripButton3
			// 
			this->toolStripButton3->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton3->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton3.Image")));
			this->toolStripButton3->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton3->Name = L"toolStripButton3";
			this->toolStripButton3->Size = System::Drawing::Size(23, 22);
			this->toolStripButton3->Text = L"���������";
			this->toolStripButton3->Click += gcnew System::EventHandler(this, &Form1::toolStripButton3_Click);
			// 
			// FurnitureModeButton
			// 
			this->FurnitureModeButton->CheckOnClick = true;
			this->FurnitureModeButton->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->FurnitureModeButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"FurnitureModeButton.Image")));
			this->FurnitureModeButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->FurnitureModeButton->Name = L"FurnitureModeButton";
			this->FurnitureModeButton->Size = System::Drawing::Size(23, 22);
			this->FurnitureModeButton->Text = L"����������� ������";
			this->FurnitureModeButton->Click += gcnew System::EventHandler(this, &Form1::FurnitureModeButton_Click);
			// 
			// PeopleModeButton
			// 
			this->PeopleModeButton->CheckOnClick = true;
			this->PeopleModeButton->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->PeopleModeButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"PeopleModeButton.Image")));
			this->PeopleModeButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->PeopleModeButton->Name = L"PeopleModeButton";
			this->PeopleModeButton->Size = System::Drawing::Size(23, 22);
			this->PeopleModeButton->Text = L"����������� �����";
			this->PeopleModeButton->Click += gcnew System::EventHandler(this, &Form1::PeopleModeButton_Click);
			// 
			// toolStripDropDownButton2
			// 
			this->toolStripDropDownButton2->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripDropDownButton2->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->��������ToolStripMenuItem, 
				this->�������ToolStripMenuItem, this->�������������������ToolStripMenuItem});
			this->toolStripDropDownButton2->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripDropDownButton2.Image")));
			this->toolStripDropDownButton2->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripDropDownButton2->Name = L"toolStripDropDownButton2";
			this->toolStripDropDownButton2->Size = System::Drawing::Size(29, 22);
			this->toolStripDropDownButton2->Text = L"���� ����������� �����";
			// 
			// ��������ToolStripMenuItem
			// 
			this->��������ToolStripMenuItem->Name = L"��������ToolStripMenuItem";
			this->��������ToolStripMenuItem->Size = System::Drawing::Size(209, 22);
			this->��������ToolStripMenuItem->Text = L"��������";
			this->��������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::��������ToolStripMenuItem_Click);
			// 
			// �������ToolStripMenuItem
			// 
			this->�������ToolStripMenuItem->Name = L"�������ToolStripMenuItem";
			this->�������ToolStripMenuItem->Size = System::Drawing::Size(209, 22);
			this->�������ToolStripMenuItem->Text = L"� ������";
			this->�������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::�������ToolStripMenuItem_Click);
			// 
			// �������������������ToolStripMenuItem
			// 
			this->�������������������ToolStripMenuItem->Name = L"�������������������ToolStripMenuItem";
			this->�������������������ToolStripMenuItem->Size = System::Drawing::Size(209, 22);
			this->�������������������ToolStripMenuItem->Text = L"�� ���������� �������";
			this->�������������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::�������������������ToolStripMenuItem_Click);
			// 
			// IDModeButton
			// 
			this->IDModeButton->CheckOnClick = true;
			this->IDModeButton->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->IDModeButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"IDModeButton.Image")));
			this->IDModeButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->IDModeButton->Name = L"IDModeButton";
			this->IDModeButton->Size = System::Drawing::Size(23, 22);
			this->IDModeButton->Text = L"���������� ID";
			this->IDModeButton->Click += gcnew System::EventHandler(this, &Form1::IDModeButton_Click);
			// 
			// toolStripButton1
			// 
			this->toolStripButton1->CheckOnClick = true;
			this->toolStripButton1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton1.Image")));
			this->toolStripButton1->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton1->Name = L"toolStripButton1";
			this->toolStripButton1->Size = System::Drawing::Size(23, 22);
			this->toolStripButton1->Text = L"���������� �����";
			this->toolStripButton1->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// MarkingModeButton
			// 
			this->MarkingModeButton->CheckOnClick = true;
			this->MarkingModeButton->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->MarkingModeButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"MarkingModeButton.Image")));
			this->MarkingModeButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->MarkingModeButton->Name = L"MarkingModeButton";
			this->MarkingModeButton->Size = System::Drawing::Size(23, 22);
			this->MarkingModeButton->Text = L"�������� ������";
			this->MarkingModeButton->Click += gcnew System::EventHandler(this, &Form1::MarkingModeButton_Click);
			// 
			// toolStripButton4
			// 
			this->toolStripButton4->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton4->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton4.Image")));
			this->toolStripButton4->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton4->Name = L"toolStripButton4";
			this->toolStripButton4->Size = System::Drawing::Size(23, 22);
			this->toolStripButton4->Text = L"�������� ��������";
			this->toolStripButton4->Click += gcnew System::EventHandler(this, &Form1::toolStripButton4_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(6, 25);
			// 
			// toolStripLabel1
			// 
			this->toolStripLabel1->Name = L"toolStripLabel1";
			this->toolStripLabel1->Size = System::Drawing::Size(43, 22);
			this->toolStripLabel1->Text = L"���� 1";
			// 
			// toolStripDropDownButton1
			// 
			this->toolStripDropDownButton1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripDropDownButton1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(11) {this->����1ToolStripMenuItem, 
				this->����2ToolStripMenuItem, this->����3ToolStripMenuItem, this->����4ToolStripMenuItem, this->����5ToolStripMenuItem, this->����6ToolStripMenuItem, 
				this->����7ToolStripMenuItem, this->����8ToolStripMenuItem, this->����9ToolStripMenuItem, this->����10ToolStripMenuItem, this->����11ToolStripMenuItem});
			this->toolStripDropDownButton1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripDropDownButton1.Image")));
			this->toolStripDropDownButton1->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripDropDownButton1->Name = L"toolStripDropDownButton1";
			this->toolStripDropDownButton1->Size = System::Drawing::Size(29, 22);
			this->toolStripDropDownButton1->Text = L"����� �����";
			// 
			// ����1ToolStripMenuItem
			// 
			this->����1ToolStripMenuItem->Name = L"����1ToolStripMenuItem";
			this->����1ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����1ToolStripMenuItem->Text = L"���� 1";
			this->����1ToolStripMenuItem->Visible = false;
			this->����1ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����1ToolStripMenuItem_Click);
			// 
			// ����2ToolStripMenuItem
			// 
			this->����2ToolStripMenuItem->Name = L"����2ToolStripMenuItem";
			this->����2ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����2ToolStripMenuItem->Text = L"���� 2";
			this->����2ToolStripMenuItem->Visible = false;
			this->����2ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����2ToolStripMenuItem_Click);
			// 
			// ����3ToolStripMenuItem
			// 
			this->����3ToolStripMenuItem->Name = L"����3ToolStripMenuItem";
			this->����3ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����3ToolStripMenuItem->Text = L"���� 3";
			this->����3ToolStripMenuItem->Visible = false;
			this->����3ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����3ToolStripMenuItem_Click);
			// 
			// ����4ToolStripMenuItem
			// 
			this->����4ToolStripMenuItem->Name = L"����4ToolStripMenuItem";
			this->����4ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����4ToolStripMenuItem->Text = L"���� 4";
			this->����4ToolStripMenuItem->Visible = false;
			this->����4ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����4ToolStripMenuItem_Click);
			// 
			// ����5ToolStripMenuItem
			// 
			this->����5ToolStripMenuItem->Name = L"����5ToolStripMenuItem";
			this->����5ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����5ToolStripMenuItem->Text = L"���� 5";
			this->����5ToolStripMenuItem->Visible = false;
			this->����5ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����5ToolStripMenuItem_Click);
			// 
			// ����6ToolStripMenuItem
			// 
			this->����6ToolStripMenuItem->Name = L"����6ToolStripMenuItem";
			this->����6ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����6ToolStripMenuItem->Text = L"���� 6";
			this->����6ToolStripMenuItem->Visible = false;
			this->����6ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����6ToolStripMenuItem_Click);
			// 
			// ����7ToolStripMenuItem
			// 
			this->����7ToolStripMenuItem->Name = L"����7ToolStripMenuItem";
			this->����7ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����7ToolStripMenuItem->Text = L"���� 7";
			this->����7ToolStripMenuItem->Visible = false;
			this->����7ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����7ToolStripMenuItem_Click);
			// 
			// ����8ToolStripMenuItem
			// 
			this->����8ToolStripMenuItem->Name = L"����8ToolStripMenuItem";
			this->����8ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����8ToolStripMenuItem->Text = L"���� 8";
			this->����8ToolStripMenuItem->Visible = false;
			this->����8ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����8ToolStripMenuItem_Click);
			// 
			// ����9ToolStripMenuItem
			// 
			this->����9ToolStripMenuItem->Name = L"����9ToolStripMenuItem";
			this->����9ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����9ToolStripMenuItem->Text = L"���� 9";
			this->����9ToolStripMenuItem->Visible = false;
			this->����9ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����9ToolStripMenuItem_Click);
			// 
			// ����10ToolStripMenuItem
			// 
			this->����10ToolStripMenuItem->Name = L"����10ToolStripMenuItem";
			this->����10ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����10ToolStripMenuItem->Text = L"���� 10";
			this->����10ToolStripMenuItem->Visible = false;
			this->����10ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����10ToolStripMenuItem_Click);
			// 
			// ����11ToolStripMenuItem
			// 
			this->����11ToolStripMenuItem->Name = L"����11ToolStripMenuItem";
			this->����11ToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->����11ToolStripMenuItem->Text = L"���� 11";
			this->����11ToolStripMenuItem->Visible = false;
			this->����11ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����11ToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(6, 25);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->DefaultExt = L"xml";
			this->saveFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::saveFileDialog1_FileOk);
			// 
			// saveFileDialog2
			// 
			this->saveFileDialog2->DefaultExt = L"xml";
			this->saveFileDialog2->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::saveFileDialog2_FileOk);
			// 
			// openFileDialog2
			// 
			this->openFileDialog2->FileName = L"openFileDialog2";
			this->openFileDialog2->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog2_FileOk);
			// 
			// openFileDialog3
			// 
			this->openFileDialog3->FileName = L"openFileDialog3";
			this->openFileDialog3->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog3_FileOk);
			// 
			// saveFileDialog3
			// 
			this->saveFileDialog3->DefaultExt = L"xml";
			this->saveFileDialog3->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::saveFileDialog3_FileOk);
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {this->������������ToolStripMenuItem, 
				this->�������������ToolStripMenuItem, this->����������ToolStripMenuItem, this->��������ToolStripMenuItem, this->����������ToolStripMenuItem});
			this->contextMenuStrip1->Name = L"contextMenuStrip1";
			this->contextMenuStrip1->Size = System::Drawing::Size(163, 114);
			// 
			// ������������ToolStripMenuItem
			// 
			this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
			this->������������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->������������ToolStripMenuItem->Text = L"������� �����";
			this->������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::������������ToolStripMenuItem_Click);
			// 
			// �������������ToolStripMenuItem
			// 
			this->�������������ToolStripMenuItem->Name = L"�������������ToolStripMenuItem";
			this->�������������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->�������������ToolStripMenuItem->Text = L"������� ������";
			this->�������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::�������������ToolStripMenuItem_Click);
			// 
			// ����������ToolStripMenuItem
			// 
			this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
			this->����������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->����������ToolStripMenuItem->Text = L"����������";
			this->����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����������ToolStripMenuItem_Click);
			// 
			// ��������ToolStripMenuItem
			// 
			this->��������ToolStripMenuItem->Name = L"��������ToolStripMenuItem";
			this->��������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->��������ToolStripMenuItem->Text = L"��������";
			this->��������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::��������ToolStripMenuItem_Click);
			// 
			// ����������ToolStripMenuItem
			// 
			this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
			this->����������ToolStripMenuItem->Size = System::Drawing::Size(162, 22);
			this->����������ToolStripMenuItem->Text = L"����������";
			this->����������ToolStripMenuItem->Visible = false;
			this->����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::����������ToolStripMenuItem_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(1158, 456);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(102, 13);
			this->label4->TabIndex = 12;
			this->label4->Text = L"����� �� �����: 0";
			// 
			// openFileDialog4
			// 
			this->openFileDialog4->FileName = L"openFileDialog4";
			this->openFileDialog4->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog4_FileOk);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1356, 531);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->toolStrip1);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->propertyGrid1);
			this->Controls->Add(this->txtRes);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"EvaEdit - �������� ��������� ���������";
			this->WindowState = System::Windows::Forms::FormWindowState::Maximized;
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->Shown += gcnew System::EventHandler(this, &Form1::Form1_Shown);
			this->Resize += gcnew System::EventHandler(this, &Form1::Form1_Resize);
			this->PreviewKeyDown += gcnew System::Windows::Forms::PreviewKeyDownEventHandler(this, &Form1::Form1_PreviewKeyDown);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->panel1->ResumeLayout(false);
			this->panel4->ResumeLayout(false);
			this->panel4->PerformLayout();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->contextMenuStrip1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


		//������� ������ �������� �����
private: System::Void �������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 openFileDialog1->ShowDialog();
//			 GetModuleFileName(
			/*TCHAR szPath[MAX_PATH];
			GetModuleFileName( NULL, szPath, MAX_PATH );
			char prefix[100] = "Bin\\EVA_Edit\\ScEvacEdit.exe";
			char szPath_char[MAX_PATH];
			CharToOem(szPath, szPath_char);
			if (strstr(szPath_char, prefix) != NULL)
			{
				*strstr(szPath_char, prefix) = 0;
			}
			strcat(szPath_char, "project.ini");
			freopen(szPath_char, "r", stdin);
			char filename_input[255];
			cin >> filename_input;
			fclose(stdin);
			System::String^ s;
			s = gcnew String(filename_input);
			txtRes->Text = s;*/
			/*openFileDialog1->ShowDialog();
			txtRes->Text += openFileDialog1->FileName;
			using namespace System::Runtime::InteropServices;
			const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(openFileDialog1->FileName);
			
			if (strstr(str, "13gymnasium11.xml") != NULL)
			{
				txtRes->Text += "DA!";
				render->SetInputFile(str);
				panel1->Enabled = true;
				���������������ToolStripMenuItem->Enabled = true;
				��������������ToolStripMenuItem->Enabled = true;
				toolStripButton2->Enabled = true;
				toolStripButton3->Enabled = true;
				FurnitureModeButton->Enabled = true;
				PeopleModeButton->Enabled = true;
				toolStripLabel1->Visible = true;
				toolStripLabel1->Enabled = true;
				toolStripDropDownButton1->Enabled = true;
				propertyGrid1->Enabled = true;
				txtRes->Enabled = true;
//				saveFileDialog1->FileName = openFileDialog1->FileName ;
			}
			else
				txtRes->Text = "NET.";*/
			
//			Marshal::FreeHGlobal(str);

			/*freopen(cfilename, "r", stdin);
			string strTemp;
			cin >> strTemp;
			String^ s = gcnew String(strTemp.c_str());
			txtRes->Text = s;*/
		 }


		 //������� ���� �������
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 render->Draw();
		 }


		 //�������, ���������� ��� ��������� �������� ����
private: System::Void panel1_MouseWheel(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e )
      {
         if ( e->Delta > 0 )
         {
            render->Zoom_in();
         }
		 else
		 {
			 render->Zoom_out();
		 }
      }

		//���������� ��� ����������� ��������� ���������� ����� ����
		int LastRightButtunClick_X, LastRightButtunClick_Y;

		//�������, ���������� ��� ����� ������
private: System::Void panel1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			 if(e->Button == System::Windows::Forms::MouseButtons::Right)
			 {
				 LastRightButtunClick_X = e->X;
				 LastRightButtunClick_Y = e->Y;
				 contextMenuStrip1->Show(e->X + 12, e->Y + 73);
			 }
		 }

		 //����������, ������������, ��� ������ ������ ����
		 bool isDown;

		 //�������, ���������� ��� ������� ������ ���� (�������)
private: System::Void panel1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if(e->Button == System::Windows::Forms::MouseButtons::Left)
			{
				isDown = true;
				render->MousePressed(0, 0, e->X, e->Y);
			}
		 }

		 //�������, ���������� ��� ������� ������ ���� (�������)
private: System::Void panel1_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if(e->Button == System::Windows::Forms::MouseButtons::Left)
			{
				isDown = false;
				render->MousePressed(0, 1, e->X, e->Y);
			}
			 
		 }

		 //�������, ���������� ��� ����������� ������� ��� ������� ������ ����
private: System::Void panel1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if(isDown)
			 {
				 render->mouseMove(e->X, e->Y);
			 }
		 }
private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		 }

		 //�������, ���������� ��� ������� ������ ���������������
private: System::Void toolStripButton2_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->Zoom_in();
		 }
private: System::Void toolStripButton3_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->Zoom_out();
		 }

		 //�������, ���������� ��� ������� ������ ����������� ������
private: System::Void FurnitureModeButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (FurnitureModeButton->Checked == true)
			 {
				 PeopleModeButton->Checked = false;
				 MarkingModeButton->Checked = false;
				 render->FurnitureModeON();
			 }
			 else
				 render->FurnitureModeOFF();
		 }

		 //�������, ���������� ��� ������� ������ ���������� ������ � XML ����
private: System::Void ���������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 saveFileDialog1->ShowDialog();
			 /*txtRes->Text = saveFileDialog1->FileName;
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(saveFileDialog1->FileName);
			 render->SaveMebel(str);*/
//			 Marshal::FreeHGlobal(str);
		 }


		 //�������� �����
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			/*abcCLI ^ obj2 = gcnew abcCLI();
			obj2->original = &render->obj[0];
			propertyGrid1->SelectedObject = obj2;*/
			/*TCHAR szPath[MAX_PATH];
			GetModuleFileName( NULL, szPath, MAX_PATH );
			char prefix[100] = "Bin\\EVA_Edit\\ScEvacEdit.exe";
			char szPath_char[MAX_PATH];
			CharToOem(szPath, szPath_char);
			if (strstr(szPath_char, prefix) != NULL)
			{
				*strstr(szPath_char, prefix) = 0;
			}
			strcat(szPath_char, "project.ini");*/
			/*System::String^ s = gcnew System::String(szPath_char);
			txtRes->Visible = true;
			txtRes->Text = s;*/
			/*if (freopen(szPath_char, "r", stdin) == NULL)
			{
				txtRes->Visible = true;
				txtRes->Text = "NO";
			}
			else
			{*/
			string STR = render->filename_input;
			int n = STR.rfind("\\");
			string STR2 = &STR[n + 1];
			STR2[STR2.length() - 4] = 0;
			STR[n] = 0;
			System::String^ s = gcnew System::String(STR.c_str());
			System::String^ s2 = gcnew System::String(STR2.c_str());
			saveFileDialog1->FileName = s2 + "_furniture.xml";
			saveFileDialog2->FileName = s2 + "_people.xml";
			saveFileDialog3->FileName = s2 + "_doors.xml";
			openFileDialog1->FileName = s2 + ".xml";
			openFileDialog2->FileName = s2 + "_people.xml";
			openFileDialog3->FileName = s2 + "_furniture.xml";
			openFileDialog4->FileName = s2 + "_doors.xml";
			saveFileDialog1->InitialDirectory = s;
			saveFileDialog2->InitialDirectory = s;
			saveFileDialog3->InitialDirectory = s;
			openFileDialog1->InitialDirectory = s;
			openFileDialog2->InitialDirectory = s;
			openFileDialog3->InitialDirectory = s;
			openFileDialog4->InitialDirectory = s;
//			txtRes->Visible = true;
//			txtRes->Text = s + " AAA " + s2;
//			char ch[100];
//			itoa(render->floor.size(), ch, 10);
//			System::String^ stri = gcnew System::String(ch);
//			txtRes->Text = stri;
			string sss = "��������� ����";
			int minus = 0;
			if (render->floor[0].name == sss)
				minus = 1;
			if (render->floor[0].number == 0)
				minus = 1;
			switch(render->floor.size() - minus)
			{
			case 1:
				{
					����1ToolStripMenuItem->Visible = true;
				}
				break;
			case 2:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
				}
				break;
			case 3:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
				}
				break;
			case 4:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
				}
				break;
			case 5:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
					����5ToolStripMenuItem->Visible = true;
				}
				break;
			case 6:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
					����5ToolStripMenuItem->Visible = true;
					����6ToolStripMenuItem->Visible = true;
				}
				break;
			case 7:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
					����5ToolStripMenuItem->Visible = true;
					����6ToolStripMenuItem->Visible = true;
					����7ToolStripMenuItem->Visible = true;
				}
				break;
			case 8:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
					����5ToolStripMenuItem->Visible = true;
					����6ToolStripMenuItem->Visible = true;
					����7ToolStripMenuItem->Visible = true;
					����8ToolStripMenuItem->Visible = true;
				}
				break;
			case 9:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
					����5ToolStripMenuItem->Visible = true;
					����6ToolStripMenuItem->Visible = true;
					����7ToolStripMenuItem->Visible = true;
					����8ToolStripMenuItem->Visible = true;
					����9ToolStripMenuItem->Visible = true;
				}
				break;
			case 10:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
					����5ToolStripMenuItem->Visible = true;
					����6ToolStripMenuItem->Visible = true;
					����7ToolStripMenuItem->Visible = true;
					����8ToolStripMenuItem->Visible = true;
					����9ToolStripMenuItem->Visible = true;
					����10ToolStripMenuItem->Visible = true;
				}
				break;
			case 11:
				{
					����1ToolStripMenuItem->Visible = true;
					����2ToolStripMenuItem->Visible = true;
					����3ToolStripMenuItem->Visible = true;
					����4ToolStripMenuItem->Visible = true;
					����5ToolStripMenuItem->Visible = true;
					����6ToolStripMenuItem->Visible = true;
					����7ToolStripMenuItem->Visible = true;
					����8ToolStripMenuItem->Visible = true;
					����9ToolStripMenuItem->Visible = true;
					����10ToolStripMenuItem->Visible = true;
					����11ToolStripMenuItem->Visible = true;
				}
				break;
			}
//			}
		 }


		 //�������, ���������� ��� ������� ������ �� � ������� "����������"
private: System::Void saveFileDialog1_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 txtRes->Text = saveFileDialog1->FileName;
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(saveFileDialog1->FileName);
			 render->SaveMebel(str);
		 }


		 //�������, ���������� ��� ������� ������� ����� ����� 
private: System::Void ����1ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(1);
			 toolStripLabel1->Text = "���� 1";
		 }
private: System::Void ����2ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(2);
			 toolStripLabel1->Text = "���� 2";
		 }
private: System::Void ����3ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(3);
			 toolStripLabel1->Text = "���� 3";
		 }
private: System::Void ����4ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(4);
			 toolStripLabel1->Text = "���� 4";
		 }
private: System::Void ����5ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(5);
			 toolStripLabel1->Text = "���� 5";
		 }
private: System::Void ����6ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(6);
			 toolStripLabel1->Text = "���� 6";
		 }
private: System::Void ����7ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(7);
			 toolStripLabel1->Text = "���� 7";
		 }
private: System::Void ����8ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(8);
			 toolStripLabel1->Text = "���� 8";
		 }
private: System::Void ����9ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(9);
			 toolStripLabel1->Text = "���� 9";
		 }
private: System::Void ����10ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(10);
			 toolStripLabel1->Text = "���� 10";
		 }
private: System::Void ����11ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->SwitchFloor(11);
			 toolStripLabel1->Text = "���� 11";
		 }


		 //�������, ���������� ��� ������� ������ ������ �� ����������
private: System::Void �����ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 Application::Exit();
		 }

		 //�������, ���������� ��� ��������� ������� ����
private: System::Void Form1_Resize(System::Object^  sender, System::EventArgs^  e) {
			 panel1->Size = System::Drawing::Size( this->Size.Width - propertyGrid1->Size.Width - 40, this->Size.Height - statusStrip1->Size.Height - 100 );
			 render->ResizePanel(panel1->Size.Width, panel1->Size.Height);
			 propertyGrid1->Location = System::Drawing::Point(panel1->Location.X + panel1->Size.Width + 5, propertyGrid1->Location.Y);
			 label4->Location = System::Drawing::Point(panel1->Location.X + panel1->Size.Width + 5, label4->Location.Y);
//			 propertyGrid1->TabIndex = 1;
//			 propertyGrid1->Text = "FFFDFDFD";
//			 int i = 7;
//			 System::Object^ o = i;
//			 private PersonData _personData = new PersonData();
//			 propertyGrid1.SelectedObject = _personData;
			 txtRes->Location = System::Drawing::Point(panel1->Location.X + panel1->Size.Width + 48, txtRes->Location.Y);
			 txtRes->Text += "panel1: " + panel1->Size.Width + " " + panel1->Size.Height + " " + propertyGrid1->Location.X;
		 }


		 //�������, ���������� ��� ������� ������  ������ ����������� �����
private: System::Void PeopleModeButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (PeopleModeButton->Checked == true)
			 {
				 FurnitureModeButton->Checked = false;
				 MarkingModeButton->Checked = false;
				 render->PeopleModeON();
			 }
			 else
				 render->PeopleModeOFF();
		 }


		 //�������, ���������� ��� ������� ������ ���������� ������ ����� � ����
private: System::Void ��������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 saveFileDialog2->ShowDialog();
			 /*txtRes->Text = saveFileDialog2->FileName;
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(saveFileDialog2->FileName);
			 render->SavePeople(str);*/
		 }
private: System::Void Form1_Shown(System::Object^  sender, System::EventArgs^  e) {
			 /*const char * filename_input2 = "C:\\Downloadss\\ScEvacEdit\\YEAH\\�����2_1\\gymnasium.xml";
			render->SetInputFile(filename_input2);*/
		 }

		 //�������, ���������� ��� ������� ������ �� � ������� "����������"
private: System::Void saveFileDialog2_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 txtRes->Text = saveFileDialog2->FileName;
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(saveFileDialog2->FileName);
			 render->SavePeople(str);
		 }

		 //�������, ���������� ��� ������� ������ ��������� ������ ������ ID ��������
private: System::Void IDModeButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (IDModeButton->Checked == true)
				 render->IDModeON();
			 else
				 render->IDModeOFF();
		 }

		 //�������, ���������� ��� ������� ������ ����� ���������
private: System::Void MarkingModeButton_Click(System::Object^  sender, System::EventArgs^  e) {
			  if (MarkingModeButton->Checked == true)
			  {
				  FurnitureModeButton->Checked = false;
				  PeopleModeButton->Checked = false;
				  render->MarkingModeON();
			  }
			  else
				  render->MarkingModeOFF();
		 }

		 //�������, ���������� ��� ������� ������ �������� ����� � ������
private: System::Void ������������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 openFileDialog2->ShowDialog();
		 }

		 //�������, ���������� ��� ������� ������ �� � ������� "�������� �����"
private: System::Void openFileDialog2_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(openFileDialog2->FileName);
			 render->LoadPeople(str);
		 }

		 //�������, ���������� ��� ������� ������ �������� ����� � �������
private: System::Void �������������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 openFileDialog3->ShowDialog();
		 }

		 //�������, ���������� ��� ������� ������ �� � ������� "�������� �����"
private: System::Void openFileDialog3_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(openFileDialog3->FileName);
			 render->LoadMebel(str);
		 }


		 //�������, ���������� ��� ������� ������ �� � ������� "�������� �����"
private: System::Void openFileDialog1_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(openFileDialog1->FileName);
			 render->filename_build = str;
//			 panel1->Enabled = false;
			 render->BuildReady = false;
			 System::String^ ss = openFileDialog1->FileName;
			 System::String^ s2;
			 int n = ss->LastIndexOf("\\");
			 s2 = ss->Substring(n + 1);
//			 s2->Remove(s2->Length - 4);
			 s2 = s2->Substring(0, s2->Length - 4);
			 saveFileDialog1->FileName = s2 + "_furniture.xml";
			 saveFileDialog2->FileName = s2 + "_people.xml";
			 saveFileDialog3->FileName = s2 + "_doors.xml";
			 openFileDialog1->FileName = s2 + ".xml";
			 openFileDialog2->FileName = s2 + "_people.xml";
			 openFileDialog3->FileName = s2 + "_furniture.xml";
			 openFileDialog4->FileName = s2 + "_doors.xml";
		 }


		 //�������, ���������� ��� ������� ������ �� ����������
private: System::Void Form1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
			 if (e->KeyCode == Keys::Delete)
			 {
				 render->DeleteSelectedObject();
			 }
		 }
private: System::Void panel1_PreviewKeyDown(System::Object^  sender, System::Windows::Forms::PreviewKeyDownEventArgs^  e) {
			 if (e->KeyCode == Keys::Delete)
			 {
				 render->DeleteSelectedObject();
			 }
		 }


		 //�������, ���������� ��� ������� ������ ������ ������ �����
private: System::Void toolStripButton1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (toolStripButton1->Checked == true)
			 {
				 render->GridEnabled = true;
			 }
			 else
			 {
				 render->GridEnabled = false;
			 }
		 }

		 //�������, ���������� ��� ������� ������ ��������� ����������� �����
private: System::Void ��������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel2->Visible = true;
			 label1->Visible = true;
			 textBox1->Visible = true;
			 button1->Visible = true;
			 button2->Visible = true;
		 }

		 //�������, ���������� ��� ������� ������ ���� � ���� ����������� ����� �����
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel2->Visible = false;
			 label1->Visible = false;
			 textBox1->Visible = false;
			 textBox1->Text = "0";
			 button1->Visible = false;
			 button2->Visible = false;
		 }

		 //�������, ���������� ��� ������� ������ ������ � ���� ���������� ����� �����
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel2->Visible = false;
			 label1->Visible = false;
			 textBox1->Visible = false;
			 button1->Visible = false;
			 button2->Visible = false;
			 render->RandomPeopleOnFloor(int::Parse(textBox1->Text));
			 textBox1->Text = "0";
		 }


		 //�������, ���������� ��� ������� ������ ����������� ����� � ������
private: System::Void �������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->PeopleOnMebel();
		 }

		 //�������, ���������� �������� �������� �� ������� Delete
private: System::Void Form1_PreviewKeyDown(System::Object^  sender, System::Windows::Forms::PreviewKeyDownEventArgs^  e) {
			 if (e->KeyCode == Keys::Delete)
			 {
				 render->DeleteSelectedObject();
			 }
		 }

		 //�������, ���������� ��� ������� ������ ����������� ����� �� ���������� �������
private: System::Void �������������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel3->Visible = true;
			 label2->Visible = true;
			 MarkingModeButton->Checked = false;
			 render->MarkingModeOFF();
			 FurnitureModeButton->Checked = false;
			 render->PeopleModeOFF();
			 PeopleModeButton->Checked = false;
			 render->FurnitureModeOFF();
			 render->SelectingArea();
		 }

		 //�������, ���������� ��� ������� ������ ���� � ���� ���������� ����� �����
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel4->Visible = false;
			 panel2->Visible = false;
			 label1->Visible = false;
			 label3->Visible = false;
			 textBox1->Visible = false;
			 textBox1->Text = "0";
			 textBox2->Visible = false;
			 textBox2->Text = "0";
			 button3->Visible = false;
			 button4->Visible = false;
			 button5->Visible = false;
			 button6->Visible = false;
			 render->StopShowingSelectedArea();

		 }


		 //�������, ���������� ��� ������� ������ ������ � ���� ���������� ����� �����
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel4->Visible = false;
			 panel2->Visible = false;
			 label1->Visible = false;
			 label3->Visible = false;
			 textBox1->Visible = false;
			 textBox2->Visible = false;
			 textBox2->Text = "0";
			 button3->Visible = false;
			 button4->Visible = false;
			 button5->Visible = false;
			 button6->Visible = false;
			 render->RandomPeopleOnSelectedArea(int::Parse(textBox1->Text));
			 textBox1->Text = "0";
		 }



		 //�������, ���������� ��� ������� ������ ���� � ���� ���������� ����� �����
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel4->Visible = false;
			 panel2->Visible = false;
			 label1->Visible = false;
			 label3->Visible = false;
			 textBox1->Visible = false;
			 textBox1->Text = "0";
			 textBox2->Visible = false;
			 textBox2->Text = "0";
			 button3->Visible = false;
			 button4->Visible = false;
			 button5->Visible = false;
			 button6->Visible = false;
			 render->StopShowingSelectedArea();
		 }


		 //�������, ���������� ��� ������� ������ ������ � ���� ���������� ����� �����
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
			 panel4->Visible = false;
			 panel2->Visible = false;
			 label1->Visible = false;
			 label3->Visible = false;
			 textBox1->Visible = false;
			 textBox2->Visible = false;
			 textBox1->Text = "0";
			 button3->Visible = false;
			 button4->Visible = false;
			 button5->Visible = false;
			 button6->Visible = false;
			 render->DensityPeopleOnSelectedArea(System::Convert::ToDouble(textBox2->Text));
			 textBox2->Text = "0";
		 }


		 //�������, ���������� ��� ������� ������ ���������� ������
private: System::Void ��������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 saveFileDialog3->ShowDialog();
		 }
private: System::Void saveFileDialog3_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(saveFileDialog3->FileName);
			 render->SaveDoors(str);
		 }
private: System::Void panel1_Enter(System::Object^  sender, System::EventArgs^  e) {
			 //���� ����
		 }
private: System::Void ������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void ����ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		 }


		 //�������, ���������� �� ���� �� ������ ������
private: System::Void ����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->ShowInformation();
		 }
private: System::Void �������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->DeleteAllFurniture();
		 }
private: System::Void ������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->DeleteAllPeople();
		 }
private: System::Void ����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->CopyObjects();
		 }
private: System::Void ��������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->PasteObjects(LastRightButtunClick_X, LastRightButtunClick_Y);
		 }
private: System::Void toolStripButton4_Click(System::Object^  sender, System::EventArgs^  e) {
			 render->CancelAction();
		 }
private: System::Void ����������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 openFileDialog4->ShowDialog();
		 }
private: System::Void openFileDialog4_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 using namespace System::Runtime::InteropServices;
			 const char* str = (const char*)(void*)Marshal::StringToHGlobalAnsi(openFileDialog4->FileName);
			 render->LoadDoors(str);
		 }
};
}
