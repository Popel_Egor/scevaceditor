//***********************************************************************
//Render.h
//������������ ���� � ����������� �������� � �������,
//������������ ��� �������� ���������� �� ��������� ������ � �����
//***********************************************************************
#pragma once

#ifndef RenderH
#define RenderH

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gl\glut.h>
#include <math.h>
#include <stdlib.h>

#include <iostream>
#include <string.h>
#include <string>
#include <conio.h>
#include <vector>
using namespace std;
using namespace System::ComponentModel;
using namespace ToolsLib;


//***********************************************************************
//���������
//***********************************************************************


//��������� ��� ������� ������
struct TFloor
{
	int id, tp, number;
//	const char* name;
	string name;
};


//��������� ��� ������� ������
struct TBox
{
	int id, tp, TRoom_Type;
	const char* name;
	double x1, y1, z1, x2, y2, z2;
};


//��������� ��� ������� ���������� ��������
struct TFlight
{
	int id, tp;
	const char* name;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3, l, w, h, angle;
	int floor1, floor2;
};


//��������� ��� ������� ���������� ��������
struct TPlatform
{
	int id, tp;
	const char* name;
	double x1, y1, z1, x2, y2, z2;
	int floor;
};


//��������� ��� ������� �������
struct TAperture
{
	int id, tp;
	const char* name;
	double x1, y1, z1, x2, y2, z2;
	int box1, box2;
};


//��������� ��� ������� ������
struct TDoor
{
	int id, tp;
	double x1, y1, z1, x2, y2, z2;
	int box1, box2;
	int lock, closer, antifire, angle;
	int floor;
	bool marked;
};


//��������� ��� ������� ����� ������
struct TPorta
{
	int id, tp;
	const char* name;
	int id1, id2;
};

/*class furniture
{
public:
	vector <furniture> furn;
	[Description("�������������� �����")]
	[DefaultValue(0)]
	property int id
	{
		int get() { return XTextureMap->Clamp; }
		void set(int n) { XTextureMap->Clamp = b; }
	}
};*/


//��������� ��� ������� ������
struct furniture
{
	int id, tp, set;
	double x1, y1, x2, y2, z, width, height, length, angle;
	double x1_real, y1_real, x2_real, y2_real;
	int box_id, floor;
	bool marked;
	bool object_is_in_selected_area;
};


//��������� ��� ������� �����
struct people
{
	int id, tp, color, mobylity, age, sex, emostate, educlevel, role, start_room, start_time, exit, control;
	double size, x, y, z;
	double x_real, y_real, radius;
//	string doors;
	int box_id, floor;
	bool marked;
	bool object_is_in_selected_area;
	int door1, door2, door3, door4, door5;
};



//����� TYPE - ��������� ��������� �������� type ������
public enum class TYPE
{
  [Description("�������")]
  a1 = 1,
  [Description("����")]
  a2 = 2,
  [Description("����")]
  a3 = 3,
  [Description("��-�� �����������")]
  a5 = 5
};


//����� ��� ���������� �������, �� ���������� ������
public ref class furnitureCLI 
{
public:
	furniture *original;

	[DisplayName("a) ID")]
	[Description("���������� ������������� ������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("1. ��������")]
	property int id
	{
		int get() { return original->id; }
		void set(int val) { original->id = val; }
	}

	[DisplayName("b) ���")]
	[Description("��� ������")]
	[DefaultValue(TYPE::a5)]
	[Category("1. ��������")]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property TYPE tp
	{
		TYPE get() { return (TYPE) original->tp; }
		void set(TYPE val) { original->tp = (int) val; }
	}
	
	[DisplayName("c) �����")]
	[Description("���������� ���������� ����, �� ���� ���-�� �����, ������� ����� ���� ������������� � ����� ������� ������")]
	[DefaultValue(0)]
	[Category("1. ��������")]
	[ReadOnly(false)]
	property double Set
	{
		double get() { return original->set; }
		void set(double val) { original->set = val; }
	}
	
	[DisplayName("d) ����")]
	[Description("���� �������� �������")]
	[DefaultValue(0)]
	[Category("1. ��������")]
	[ReadOnly(true)]
	property double angle
	{
		double get() { return original->angle; }
		void set(double val) { original->angle = val; }
	}

	[DisplayName("X")]
	[Description("���������� � ������� ������ ����")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double x1_real
	{
		double get() { return original->x1_real; }
		void set(double val) { original->x1_real = val; }
	}

	[DisplayName("Y")]
	[Description("���������� Y ������� ������ ����")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double y1_real
	{
		double get() { return original->y1_real; }
		void set(double val) { original->y1_real = val; }
	}

	[DisplayName("Z")]
	[Description("���������� Z ������� ������ ����")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double z
	{
		double get() { return original->z; }
		void set(double val) { original->z = val; }
	}

	[DisplayName("������")]
	[Description("������ (�� ���������� X)")]
	[DefaultValue(0)]
	[ReadOnly(false)]
	[Category("2. ����������")]
	property double width
	{
		double get() { return original->width; }
		void set(double val) { original->width = val; }
	}

	[DisplayName("������")]
	[Description("������ �� ���� (�� ���������� Z)")]
	[DefaultValue(0)]
	[ReadOnly(false)]
	[Category("2. ����������")]
	property double height
	{
		double get() { return original->height; }
		void set(double val) { original->height = val; }
	}

	[DisplayName("�����")]
	[Description("����� (�� ���������� Y)")]
	[DefaultValue(0)]
	[ReadOnly(false)]
	[Category("2. ����������")]
	property double length
	{
		double get() { return original->length; }
		void set(double val) { original->length = val; }
	}
};



//������ ��� ���������� ������� ��� ������� ������
public enum class LOCK
{
  [Description("�������")]
  a1 = -1,
  [Description("�������")]
  a0 = 0
};

public enum class CLOSER
{
  [Description("��")]
  a1 = -1,
  [Description("���")]
  a0 = 0
};

public enum class ANTIFIRE
{
  [Description("��")]
  a1 = -1,
  [Description("���")]
  a0 = 0
};


//����� ��� ���������� �������, �� ���������� ������
public ref class doorCLI 
{
public:
	TDoor *original;

	[DisplayName("a) ID")]
	[Description("���������� ������������� �����")]
	[DefaultValue(0)]
	[Category("1. ��������")]
	[ReadOnly(true)]
	property int id
	{
		int get() { return original->id; }
		void set(int val) { original->id = val; }
	}

	[DisplayName("b) �� �����")]
	[Description("����� �� �����")]
	[DefaultValue(LOCK::a0)]
	[Category("1. ��������")]
	[ReadOnly(false)]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property LOCK lock
	{
		LOCK get() { return (LOCK) original->lock; }
		void set(LOCK val) { original->lock = (int) val; }
	}
	
	[DisplayName("c) ��������")]
	[Description("��������� ���������")]
	[DefaultValue(0)]
	[Category("1. ��������")]
	[ReadOnly(false)]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property CLOSER closer
	{
		CLOSER get() { return (CLOSER) original->closer; }
		void set(CLOSER val) { original->closer = (int) val; }
	}
	
	[DisplayName("d) ���������������")]
	[Description("��������������� �����")]
	[DefaultValue(ANTIFIRE::a0)]
	[Category("1. ��������")]
	[ReadOnly(false)]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property ANTIFIRE antifire
	{
		ANTIFIRE get() { return (ANTIFIRE) original->antifire; }
		void set(ANTIFIRE val) { original->antifire = (int) val; }
	}

	[DisplayName("e) ����")]
	[Description("���� ��������")]
	[DefaultValue(0)]
	[Category("1. ��������")]
	[ReadOnly(true)]
	property int angle
	{
		int get() { return original->angle; }
		void set(int val) { original->angle = val; }
	}

	[DisplayName("f) Box1")]
	[Description("���������, �� ������ ����� ����������� (�����) �����")]
	[DefaultValue(0)]
	[Category("1. ��������")]
	[ReadOnly(true)]
	property int box1
	{
		int get() { return original->box1; }
		void set(int val) { original->box1 = val; }
	}

	[DisplayName("g) Box2")]
	[Description("���������, � ������ ����� ����������� (�����) �����")]
	[DefaultValue(0)]
	[Category("1. ��������")]
	[ReadOnly(true)]
	property int box2
	{
		int get() { return original->box2; }
		void set(int val) { original->box2 = val; }
	}

	[DisplayName("X1")]
	[Description("���������� � ������ ������� ���������������, � ������� ������ �����")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double x1
	{
		double get() { return original->x1; }
		void set(double val) { original->x1 = val; }
	}

	[DisplayName("Y1")]
	[Description("���������� Y ������ �������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double y1
	{
		double get() { return original->y1; }
		void set(double val) { original->y1 = val; }
	}

	[DisplayName("Z1")]
	[Description("���������� Z ������ �������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double z1
	{
		double get() { return original->z1; }
		void set(double val) { original->z1 = val; }
	}

	[DisplayName("X2")]
	[Description("���������� � ������ ������� ���������������, � ������� ������ �����")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double x2
	{
		double get() { return original->x2; }
		void set(double val) { original->x2 = val; }
	}

	[DisplayName("Y2")]
	[Description("���������� Y ������ �������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double y2
	{
		double get() { return original->y2; }
		void set(double val) { original->y2 = val; }
	}

	[DisplayName("Z2")]
	[Description("���������� Z ������ �������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("2. ����������")]
	property double z2
	{
		double get() { return original->z2; }
		void set(double val) { original->z2 = val; }
	}

};



//������ ��� ���������� ������� ��� ������� �����
public enum class COLOR
{
  [Description("�������")]
  ared = 255,
  [Description("���������")]
  borange = 42495,
  [Description("������")]
  cyellow = 65535,
  [Description("�������")]
  dgreen = 65280,
  [Description("�������")]
  egoluboi = 16776960,
  [Description("�����")]
  fblue = 16711680,
  [Description("����������")]
  gpurple = 8388736
};

public enum class CONTROL
{
  [Description("��������")]
  ayes = -1,
  [Description("�� ��������")]
  bno = 0
};

public enum class EMOSTATE
{
  [Description("�� ���� ������")]
  a0 = 0,
  [Description("���������� (0,57 �/�)")]
  a1 = 1,
  [Description("��������� (0,96 �/�)")]
  a2 = 2,
  [Description("�������� (1,3 �/�)")]
  a3 = 3,
  [Description("���������� ���������� (1,75 �/�)")]
  a4 = 4
};

public enum class SEX
{
  [Description("�������")]
  a1 = 1,
  [Description("�������")]
  a2 = 2,
  [Description("��� ����")]
  a3 = 3
};

public enum class AGE
{
  [Description("����������")]
  a1 = 1,
  [Description("������� - 7-9 ���")]
  a2 = 2,
  [Description("������� - 10-13 ���")]
  a3 = 3,
  [Description("������� - 14-16 ���")]
  a4 = 4,
  [Description("�������� - >16 ���")]
  a5 = 5
};

public enum class MOBILITY
{
  [Description("1")]
  a1 = 1,
  [Description("2")]
  a2 = 2,
  [Description("3")]
  a3 = 3
};



//����� ��� ���������� �������, �� ���������� �����
public ref class peopleCLI 
{
public:

	people* original;

	[DisplayName("a) ID")]
	[ReadOnly(true)]
	[Description("������������ ������������� ��������")]
	[DefaultValue(NULL)]
	[Category("1. �������� ��������")]
	property int id
	{
		int get() { return original->id; }
		void set(int val) { original->id = val; }
	}

	[DisplayName("b) ���")]
	[Description("��� ��������")]
	[DefaultValue(0)]
	[Category("1. �������� ��������")]
	property int tp
	{
		int get() { return original->tp; }
		void set(int val) { original->tp = val; }
	}

	[DisplayName("c) ����� ������")]
	[Description("����� ������")]
	[DefaultValue(0)]
	[Category("1. �������� ��������")]
	property int start_time
	{
		int get() { return original->start_time; }
		void set(int val) { original->start_time = val; }
	}

	[DisplayName("d) �����")]
	[Description("�����, � ������� ������������ �������")]
	[DefaultValue(0)]
	[Category("1. �������� ��������")]
	property int exit
	{
		int get() { return original->exit; }
		void set(int val) { original->exit = val; }
	}

//	[DisplayName("e) �����")]
//	[Description("������ � ������, ����� ������� ������ ������ ������� �� ����� ���������")]
//	[DefaultValue("0")]
//	[Category("1. �������� ��������")]
//	property string doors
//	{
//		string get() { return original->doors; }
//		void set(string val) { original->doors = val; }
//	}

	[DisplayName("f) ������� ��������")]
	[Description("������� �������� ��������, � ���������� ������")]
	[DefaultValue(0.125)]
	[Category("1. �������� ��������")]
	property double size
	{
		double get() { return original->size; }
		void set(double val) { original->size = val; }
	}

//	[DisplayName("f) ������")]
//	[Description("������ �������� ��������, � ������")]
//	[DefaultValue(0.2)]
//	[Category("1. �������� ��������")]
//	property double radius
//	{
//		double get() { return original->radius; }
//		void set(double val) { original->radius = val; }
//	}

	[DisplayName("g) �����������")]
	[Description("������ �����������")]
	[DefaultValue(MOBILITY::a1)]
	[Category("1. �������� ��������")]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property MOBILITY mobylity
	{
		MOBILITY get() { return (MOBILITY) original->mobylity; }
		void set(MOBILITY val) { original->mobylity = (int) val; }
	}

	[DisplayName("h) �������")]
	[Description("�������")]
	[DefaultValue(AGE::a5)]
	[Category("1. �������� ��������")]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property AGE age
	{
		AGE get() { return (AGE) original->age; }
		void set(AGE val) { original->age = (int) val; }
	}

	[DisplayName("i) ����. ����.")]
	[Description("���������� �������� ���������� ��������")]
	[DefaultValue(EMOSTATE::a0)]
	[Category("1. �������� ��������")]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property EMOSTATE emostate
	{
		EMOSTATE get() { return (EMOSTATE) original->emostate; }
		void set(EMOSTATE val) { original->emostate = (int) val; }
	}

	[DisplayName("a) ���")]
	[Description("���")]
	[DefaultValue(SEX::a2)]
	[Category("2. �������������� ��������")]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property SEX sex
	{
		SEX get() { return (SEX) original->sex; }
		void set(SEX val) { original->sex = (int) val; }
	}

	[DisplayName("b) ����")]
	[Description("����, ������� ������������ �������")]
	[DefaultValue(COLOR::fblue)]
	[Category("2. �������������� ��������")]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property COLOR color
	{
		COLOR get() {
			return (COLOR)original->color;
			}
		void set(COLOR val) { original->color = (int)val; }
	}

	[DisplayName("c) �����������")]
	[Description("�������� �� ������� �����������")]
	[DefaultValue(CONTROL::bno)]
	[Category("2. �������������� ��������")]
	[System::ComponentModel::TypeConverter(ToolsLib::EnumTypeConverter::typeid)]
	property CONTROL control
	{
		CONTROL get() { return (CONTROL) original->control; }
		void set(CONTROL val) { original->control = (int) val; }
	}

	[DisplayName("d) ����")]
	[Description("���� ��������")]
	[DefaultValue(0)]
	[Category("2. �������������� ��������")]
	property int role
	{
		int get() { return original->role; }
		void set(int val) { original->role = val; }
	}

	[DisplayName("e) ������� ����������")]
	[Description("������� ����������������")]
	[DefaultValue(0)]
	[Category("2. �������������� ��������")]
	property int educlevel
	{
		int get() { return original->educlevel; }
		void set(int val) { original->educlevel = val; }
	}

	[DisplayName("f) ��������� �������")]
	[Description("�������, �� ������� �������� �������")]
	[DefaultValue(0)]
	[Category("2. �������������� ��������")]
	property int start_room
	{
		int get() { return original->start_room; }
		void set(int val) { original->start_room = val; }
	}
	
//	[DisplayName("������")]
//	[Description("������ �������� ��������, � ������")]
//	[DefaultValue(0)]
//	property double radius
//	{
//		double get() { return original->radius; }
//		void set(double val) { original->radius = val; }
//	}
	[DisplayName("X")]
	[Description("X ����������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("3. ����������")]
	property double x_real
	{
		double get() { return original->x_real; }
		void set(double val) { original->x_real = val; }
	}
	[DisplayName("Y")]
	[Description("Y ����������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("3. ����������")]
	property double y_real
	{
		double get() { return original->y_real; }
		void set(double val) { original->y_real = val; }
	}
	[DisplayName("Z")]
	[Description("Z ����������")]
	[DefaultValue(0)]
	[ReadOnly(true)]
	[Category("3. ����������")]
	property double z
	{
		double get() { return original->z; }
		void set(double val) { original->z = val; }
	}

	[DisplayName("����� 1")]
	[Description("ID ������, ����� ������� ������ ������ ������� �� ����� ���������")]
	[DefaultValue(0)]
	[Category("4. ���� ���������")]
	property int door1
	{
		int get() { return original->door1; }
		void set(int val) { original->door1 = val; }
	}

	[DisplayName("����� 2")]
	[Description("ID ������, ����� ������� ������ ������ ������� �� ����� ���������")]
	[DefaultValue(0)]
	[Category("4. ���� ���������")]
	property int door2
	{
		int get() { return original->door2; }
		void set(int val) { original->door2 = val; }
	}

	[DisplayName("����� 3")]
	[Description("ID ������, ����� ������� ������ ������ ������� �� ����� ���������")]
	[DefaultValue(0)]
	[Category("4. ���� ���������")]
	property int door3
	{
		int get() { return original->door3; }
		void set(int val) { original->door3 = val; }
	}

	[DisplayName("����� 4")]
	[Description("ID ������, ����� ������� ������ ������ ������� �� ����� ���������")]
	[DefaultValue(0)]
	[Category("4. ���� ���������")]
	property int door4
	{
		int get() { return original->door4; }
		void set(int val) { original->door4 = val; }
	}

	[DisplayName("����� 5")]
	[Description("ID ������, ����� ������� ������ ������ ������� �� ����� ���������")]
	[DefaultValue(0)]
	[Category("4. ���� ���������")]
	property int door5
	{
		int get() { return original->door5; }
		void set(int val) { original->door5 = val; }
	}
};



//�������� ����� � ��������, �������������� � ����� Render.cpp
class Render
{
public:
	Render(HWND handle);
	~Render();

//	std::vector <TestObject ^> obj;
//	vector <abc> obj;
	void Draw();
	void Load();
	void mouseMove(int ax, int ay);
	void MousePressed(int button, int state, int ax, int ay);
	void SwapOpenGLBuffers();
	void Zoom_in();
	void Zoom_out();
	void FurnitureModeON();
	void FurnitureModeOFF();
	void PeopleModeON();
	void PeopleModeOFF();
	void IDModeON();
	void IDModeOFF();
	void MarkingModeON();
	void MarkingModeOFF();
	void ShowInformation();
	void SaveMebel(const char* filename);
	void SavePeople(const char* filename);
	void SaveDoors(const char* filename);
	void SwitchFloor(int FloorNumber);
	void SetInputFile(const char * Filename);
	void ResizePanel(double Width, double Height);
	void LoadPeople(const char* filename_people);
	void LoadMebel(const char* filename_mebel);
	void LoadDoors(const char* filename_doors);
	void LoadBuild();
	void DeleteSelectedObject();
	void DeleteAllFurniture();
	void DeleteAllPeople();
	void RandomPeopleOnFloor(int number);
	void PeopleOnMebel();
	void SelectingArea();
	void StopShowingSelectedArea();
	void CopyObjects();
	void PasteObjects(int ax, int ay);
	void RandomPeopleOnSelectedArea(int number);
	void DensityPeopleOnSelectedArea(double density);
	void PropertyChanged(double OldValue);
	void SaveAction();
	void CancelAction();
	bool SubFunc(TBox j, int &CurrentNumber, double x1, double x2, double y1, double y2);
	string filename_input;
	people emptyField;
	people FieldToCheck;
	vector <TFloor> floor;
	vector <people> peop;
	vector <people> peop_saved_1;
	vector <people> peop_saved_2;
	vector <people> peop_saved_3;
	vector <people> peop_copy;
	vector <furniture> furn;
	vector <furniture> furn_saved_1;
	vector <furniture> furn_saved_2;
	vector <furniture> furn_saved_3;
	vector <furniture> furn_copy;
	vector <TDoor> door;
	vector <TDoor> door_saved_1;
	vector <TDoor> door_saved_2;
	vector <TDoor> door_saved_3;
	vector <vector <TBox>> box;
	vector <vector <TAperture>> aper;
	vector <TFlight> flight;
	vector <TPlatform> plat;
	vector <TPorta> port;
	bool BuildReady;
	bool GridEnabled;
	const char* filename_build;

private:
	
//	vector <TFloor> floor;
	float Scale;

	GLint MySetPixelFormat(HDC hdc);
	HDC m_hDC;
	HGLRC m_hglrc;
};

extern Render *render;

#endif