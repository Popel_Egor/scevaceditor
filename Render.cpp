//***********************************************************************
//Render.cpp
//���� � ���������� ���� ������� ��������� ��������
//������������ � ��������� ��������
//***********************************************************************
#include "stdafx.h"
#include "Render.h"
#include <stdio.h>
#include "tinyxml/tinystr.h"
#include "tinyxml/tinystr.cpp"
#include "tinyxml/tinyxml.h"
#include "tinyxml/tinyxml.cpp"
#include "tinyxml/tinyxmlerror.cpp"
#include "tinyxml/tinyxmlparser.cpp"
#include "glfont.h"
#include <tchar.h>
#include "Form1.h"
#include "time.h"

#pragma comment (lib, "user32.lib")
#pragma comment (lib, "gdi32.lib")
#pragma comment (lib, "glut32.lib")
using namespace HelloWorldWindow;

//***********************************************************************
//����������
//***********************************************************************

Render *render = 0;
bool WeCalledLoadProcess = false;
//������ � ������ ���� (������)
float WinWid = 1140.0;
float WinHei = 622.0;

//������ � ������ ���� (������)
//��� ����������� ����������� ������
float WinWid_f = 600.0;
float WinHei_f = 600.0;
int colors = 0;

//����� �������� (�������������) �����
int floor_number = 0;

//���������� ����������, ������������ �������
//������������ ��������, ������� ����� ��������
bool action1 = false;
bool action2 = false;
bool action3 = false;

//���������� ������ ��� ���
bool show_apertures = true;
bool show_ids = false;

//CommonFieldWorks = true, ���� ������������
//������� ������ �������� � ����� ���������� ���
//��� ����� ��������� �������
bool CommonFieldWorks = false;

//���������� ��� �������������� ������ ������
//������ ������������ ��������� ������ ���������
float x = -WinWid_f / 4;
float y = WinHei_f / 2;

//���������� ��� ����������� ���������� ������
//����������� ������� ��� ��� ��������
float x_atm = 0.0;
float y_atm = 0.0;

//���������� ������������ �������
float x_zoom = 0.0;
float y_zoom = 0.0;

//���������� ��� ����������� ������ (������������ x_atm � y_atm)
//����������� ������� (������) ��� ��� ��������
float x_furn = 0.0;
float y_furn = 0.0;

//��������� ���������� ������ ���������� �������� (����� �������)
float x_select = 0.0;
float y_select = 0.0;

//���������� ������ ������� ���� ������ ���������� ��������
//(��� �����������) - � ����������� ������
float x1_selected = 0.0, y1_selected = 0.0, x2_selected = 0.0, y2_selected = 0.0;

//���������� ������ ������� ���� ������ ���������� ��������
//(��� �����������) - � �������� ����������� ������
float x1_selected_real = 0.0, y1_selected_real = 0.0, x2_selected_real = 0.0, y2_selected_real = 0.0;

//���������� ����������� ��������
float x_pers = 0.0;
float y_pers = 0.0;

//���������� ������ ������� ���� ����������� �������� ������
float x_mebel = 0.0;
float y_mebel = 0.0;

//��������� ���������� ����� ������� ���������� �������� ��� ������
float x_start = 0.0, y_start = 0.0;
float x_start_person = 0.0, y_start_person = 0.0;
float x_start_mebel = 0.0, y_start_mebel = 0.0;

//��� ����� � ���������� ������
const char * InputFileName;

//InputReady = true, ����� ���������� �� ����� ���������
//���� InputReady = false, �� ������������ ������
bool InputReady = false;

//���������� ��� �������������� ������ ������
//������ ������������ ��������� ������ ���������
float dx = -WinWid_f / 4, dy = WinHei_f / 2;

//���� ������ ����������� ������
bool furniture_mode = false;

//���� ������ ����������� �����
bool people_mode = false;

//���� ������ ���������
bool marking_mode = false;

//����, ������������, ��� ������� �����-�� �������
bool person_selected = false;

//����, ������������, ��� ������� �����-�� ������� ������
bool furn_selected = false;

//���� ������ ��������� ������
bool selecting_mode = false;

//����. ���� selecting_mode_free = true, �� �������
//��������� ������� ����������� �������
bool selecting_mode_free = false;

//����. ���� show_selected_area = true, ��
//������������ ���������� �������
bool show_selected_area = false;

//���� ��� ��������� ������� �� ��� ��������� �����,
//� ��� ������ �� ���������� ���������� �����
bool SelectingForGenerating = false;

//���������� ��� ����������� � ������������ ���������� ������
double min_x_box = 9999.0, max_x_box = -9999.0, min_y_box = 9999.0, max_y_box = -9999.0;
double min_x_box2, max_x_box2, min_y_box2, max_y_box2;

//������� � ������������� ID � ������� ��������
int furniture_id = 0;
int people_id = 0;
int it = -1;
int TRoom_Type;
int people_counter = 0;
GLuint tex;
GLFONT font;
vector <people>::iterator iii;
vector <furniture>::iterator iii_m;

//��� ������������ �������� �����
const char * filename_first = "";


//***********************************************************************
//�������
//***********************************************************************


//������� ������������� ���� ��� ��������� (�� ������)
//���������:
//HWND handle -- ����� ���� ������
Render::Render(HWND handle)
{
		render = this;
		m_hDC = GetDC(handle);
		if(m_hDC)
			MySetPixelFormat(m_hDC);
			TCHAR szPath[MAX_PATH];
			GetModuleFileName( NULL, szPath, MAX_PATH );
			/*char prefix[100] = "Bin\\EVA_Edit\\ScEvacEdit.exe";
			char szPath_char[MAX_PATH];
			CharToOem(szPath, szPath_char);
			if (strstr(szPath_char, prefix) != NULL)
			{
				*strstr(szPath_char, prefix) = 0;
			}
			strcat(szPath_char, "project.ini");*/
//			TCHAR prefix[100] = {TCHAR("Bin\\EVA_Edit\\ScEvacEdit.exe")};
			if (_tcsstr(szPath, _T("Bin\\EVA_Edit\\ScEvacEdit.exe")) != NULL)
			{
				*_tcsstr(szPath, _T("Bin\\EVA_Edit\\ScEvacEdit.exe")) = 0;
			}
//			TCHAR project[100] = {TCHAR("project.ini")};
			_tcscat(szPath, _T("project.ini"));
			_tfreopen(szPath, _T("r"), stdin);
			std::getline (cin, filename_input);
			fclose(stdin);
			string filename_input2 = filename_input;
		for(int i = 0; i < filename_input2.length(); i++) if(filename_input2[i]=='\\')
		{
			string str = "\\";
			filename_input2.insert(i,str);
			i++;
		};
		/*for (int i = 0; i < strlen(filename_input); i++)
		{
			j++;
			if ((int) filename_input[i] == (int) '\\')
			{
				filename_input_finish = strcat(filename_input_finish, "\\");
				char temp[255];
				temp = filename_input + i + 1;
			}
		}*/
//		const char * filename_input2 = "C:\\Downloadss\\ScEvacEdit\\YEAH\\�����2_1\\gymnasium.xml";
		const char* tmp = filename_input2.c_str();
		SetInputFile(tmp);
		/*abc field;
		field.x = 1.0;
		field.y = 6.0;
		obj.push_back(field);*/
		Load();
}

Render::~Render()
{
}


//�������, ���������� ��� ��������� �������� ������ (����)
//���������:
//double Width -- ����� ������ ������
//double Height -- ����� ������ ������
void Render::ResizePanel(double Width, double Height)
{
//	if (InputReady == true)
	{
		WinWid = Width;
		WinHei = Height;
		glViewport(0, 0, (GLsizei)Width, (GLsizei)Height);
	}
}

//������� ��� ����� ��������
void Render::SwapOpenGLBuffers()
{
	if (InputReady == true)
	{
		SwapBuffers(m_hDC);
	}
}

//�������, ���������� ��� ������� ������ ����
//���������:
//int button -- ��� ������� ������
//��������� ��������:
//GLUT_LEFT_BUTTON = 0
//GLUT_RIGHT_BUTTON = 1
//GLUT_MIDDLE_BUTTON = 2
//int state -- ��� �������
//��������� ��������:
//GLUT_DOWN = 0 -- ������ ������ ����
//GLUT_UP = 1 -- ������ ������ �����
//int ax, int ay -- ���������� ������� ����
void Render::MousePressed(int button, int state, int ax, int ay)
{
//	if (InputReady == true)
	{
//	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
//		cout << "x = " << ax << " ; y = " << ay << " ; x_c = " << ax / Scale - dx * WinWid / WinWid_f << " ; y_c = " << (WinHei - ay) / Scale + dy * WinHei / WinHei_f << endl;
	if (!furniture_mode && !people_mode && !selecting_mode)
	{
		if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
		{
			Scale *= 1.5;
		}
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{
			x_start = (ax * WinWid_f / WinWid / Scale - dx) * Scale;
			y_start = (ay * WinHei_f / WinHei / Scale - dy) * Scale;
		}
		if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
		{
			dx = x;
			dy = y;
		}
	}
	if (furniture_mode)
	{
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{
			x_start = ax / WinWid * WinWid_f / Scale;
			y_start = ay / WinHei * WinHei_f / Scale;
		}
		if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
		{
			furniture field;
			field.x1 = (x_start * WinWid / WinWid_f * Scale - WinWid / 2) / Scale - dx * WinWid / WinWid_f;
			field.y1 = (WinHei - y_start * WinHei / WinHei_f * Scale - WinHei / 2) / Scale + dy * WinHei / WinHei_f;
			field.x2 = (ax - WinWid / 2) / Scale - dx * WinWid / WinWid_f;
			field.y2 = (WinHei - ay - WinHei / 2) / Scale + dy * WinHei / WinHei_f;
			field.x1_real = field.x1 / WinWid_f * (max_x_box - min_x_box) + min_x_box;// (j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid
			field.x2_real = field.x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box;
			field.y1_real = field.y1 / WinHei_f * (max_y_box - min_y_box) + min_y_box;// (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei
			field.y2_real = field.y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box;
			field.floor = floor_number;
			field.marked = false;
			field.object_is_in_selected_area = false;
			field.box_id = 0;
			//��������
			bool add_new_item = false;
			for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
			{
				if (field.x1_real > j->x1 && field.x1_real < j->x2 && field.y1_real > j->y1 && field.y1_real < j->y2)
				{
					field.box_id = j->id;
					break;
				}
			}
			if (field.box_id != 0)
			{
				vector <TBox>::iterator j;
				for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
				{
					if (j->id == field.box_id)
						break;
				}
				field.z = j->z1;
				if (field.x2_real > j->x1 && field.x2_real < j->x2 && field.y2_real > j->y1 && field.y2_real < j->y2)
				{
					add_new_item = true;
				}
				else
				{
					if (field.x2_real >= j->x2)
					{
						field.x2_real = j->x2;
						field.x2 = (field.x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);// field.x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box
					}
					if (field.x2_real <= j->x1)
					{
						field.x2_real = j->x1;
						field.x2 = (field.x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
					}
					if (field.y2_real >= j->y2)
					{
						field.y2_real = j->y2;
						field.y2 = (field.y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);// field.y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box
					}
					if (field.y2_real <= j->y1)
					{
						field.y2_real = j->y1;
						field.y2 = (field.y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
					}
					add_new_item = true;
				}
			}
			else
			{
				for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
				{
					if (field.x2_real > j->x1 && field.x2_real < j->x2 && field.y2_real > j->y1 && field.y2_real < j->y2)
					{
						field.box_id = j->id;
						break;
					}
				}
				if (field.box_id != 0)
				{
					vector <TBox>::iterator j;
					for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
					{
						if (j->id == field.box_id)
							break;
					}
					field.z = j->z1;
					if (field.x1_real > j->x1 && field.x1_real < j->x2 && field.y1_real > j->y1 && field.y1_real < j->y2)
					{
						add_new_item = true;
					}
					else
					{
						if (field.x1_real >= j->x2)
						{
							field.x1_real = j->x2;
							field.x1 = (field.x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);// field.x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box
						}
						if (field.x1_real <= j->x1)
						{
							field.x1_real = j->x1;
							field.x1 = (field.x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
						}
						if (field.y1_real >= j->y2)
						{
							field.y1_real = j->y2;
							field.y1 = (field.y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);// field.y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box
						}
						if (field.y1_real <= j->y1)
						{
							field.y1_real = j->y1;
							field.y1 = (field.y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
						}
						add_new_item = true;
					}
				}
			}
			if (add_new_item)
			{
				//�������������� ������ (���������)
				if (field.x1 > field.x2)
				{
					double temp = field.x2;
					field.x2 = field.x1;
					field.x1 = temp;
					temp = field.x2_real;
					field.x2_real = field.x1_real;
					field.x1_real = temp;
				}
				if (field.y1 > field.y2)
				{
					double temp = field.y2;
					field.y2 = field.y1;
					field.y1 = temp;
					temp = field.y2_real;
					field.y2_real = field.y1_real;
					field.y1_real = temp;
				}
				//�������� �� ����������� � ������ �������
				for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
				{
					if (i->box_id == field.box_id)
						if (((field.x1_real <= i->x1_real && field.x2_real >= i->x1_real) || (field.x1_real <= i->x2_real && field.x2_real >= i->x2_real) || (field.x2_real <= i->x2_real && field.x1_real >= i->x1_real)) && ((field.y1_real <= i->y1_real && field.y2_real >= i->y1_real) || (field.y1_real <= i->y2_real && field.y2_real >= i->y2_real) || (field.y2_real <= i->y2_real && field.y1_real >= i->y1_real)))
						{
							add_new_item = false;
							break;
						}
				}
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->box_id == field.box_id)
						if (i->x_real + i->radius > field.x1_real && i->x_real - i->radius < field.x2_real && i->y_real + i->radius > field.y1_real && i->y_real - i->radius < field.y2_real)
						{
							add_new_item = false;
							break;
						}
				}
			}
			if (add_new_item)
			{
				furniture_id++;
				field.id = furniture_id;
				field.tp = 5;
				field.set = 0;
				field.angle = 0;
				field.width = field.x2_real - field.x1_real;
				field.height = 1;
				field.length = field.y2_real - field.y1_real;
				Render::SaveAction();
				furn.push_back(field);
			}
			x_furn = 0;
			y_furn = 0;
		}
	}
	if (people_mode)
	{
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{
			people field;
			field.x = (ax - WinWid / 2) / Scale - dx * WinWid / WinWid_f;
			field.y = (WinHei - ay - WinHei / 2) / Scale + dy * WinHei / WinHei_f;
			field.x_real = field.x / WinWid_f * (max_x_box - min_x_box) + min_x_box;// (j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid
			field.y_real = field.y / WinHei_f * (max_y_box - min_y_box) + min_y_box;// (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei
			field.floor = floor_number;
			field.marked = false;
			field.object_is_in_selected_area = false;
			field.box_id = 0;
			//��������
			bool add_new_item = false;
			for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
			{
				if (field.x_real > j->x1 && field.x_real < j->x2 && field.y_real > j->y1 && field.y_real < j->y2)
				{
					field.box_id = j->id;
					break;
				}
			}
			if (field.box_id != 0)
			{
				vector <TBox>::iterator j;
				for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
				{
					if (j->id == field.box_id)
						break;
				}
				field.z = j->z1;
				field.radius = 0.2;
				if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
				{
					add_new_item = true;
				}
				else
				{
					add_new_item = false;
				}
			}
			if (add_new_item)
			{
				//�������� �� ����������� � ������ ������� ��� ������
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->box_id == field.box_id)
						if ((i->x_real - field.x_real) * (i->x_real - field.x_real) + (i->y_real - field.y_real) * (i->y_real - field.y_real) <= 0.4 * 0.4)
						{
							add_new_item = false;
							break;
						}
				}
				for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
				{
					if (i->box_id == field.box_id)
						if (field.x_real + field.radius > i->x1_real && field.x_real - field.radius < i->x2_real && field.y_real + field.radius > i->y1_real && field.y_real - field.radius < i->y2_real)
						{
							add_new_item = false;
							break;
						}
				}
			}
			if (add_new_item)
			{
				people_id++;
				field.id = people_id;
				field.tp = 0;
				field.age = 5;
				field.color = 16776960;
				field.control = 0;
				field.educlevel = 0;
				field.emostate = 0;
				field.exit = 0;
				field.mobylity = 1;
				field.role = 0;
				field.sex = 1;
				field.size = 0.125;
				field.start_room = field.box_id;
				field.start_time = 0;
				field.door1 = 0;
				field.door2 = 0;
				field.door3 = 0;
				field.door4 = 0;
				field.door5 = 0;
//DEMO				if (peop.size() > 19)												//DEMO
//DEMO				{																	//DEMO
//DEMO					MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO				}																	//DEMO
//DEMO				else																//DEMO
//DEMO				{																	//DEMO
					Render::SaveAction();
					peop.push_back(field);
//DEMO				}																	//DEMO
				people_counter = 0;
				for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
				{
					if (pe->floor == floor_number)
					{
						people_counter++;
					}
				}
				Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
			}
		}
	}
	
	if (marking_mode)
	{
		double x_atm_real = ((ax - WinWid / 2) / Scale - dx * WinWid / WinWid_f) / WinWid_f * (max_x_box - min_x_box) + min_x_box;
		double y_atm_real = ((WinHei - ay - WinHei / 2) / Scale + dy * WinHei / WinHei_f) / WinHei_f * (max_y_box - min_y_box) + min_y_box;
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{

			bool AlreadySelected = false;
			for (vector <furniture>::iterator k = furn.begin(); k < furn.end(); k++)
			{
				if (k->object_is_in_selected_area)
				{
					k->marked = true;
					AlreadySelected = true;
				}
			}
			for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
			{
				if (k->object_is_in_selected_area)
				{
					k->marked = true;
					AlreadySelected = true;
				}
			}
			bool CursorOnObject = false;
			if (AlreadySelected)
			{
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (floor_number == i->floor && i->object_is_in_selected_area == true)
						if ((x_atm_real - i->x_real) * (x_atm_real - i->x_real) + (y_atm_real - i->y_real) * (y_atm_real - i->y_real) < i->radius * i->radius)
						{
							CursorOnObject = true;
							break;
						}
				}
				if (!CursorOnObject)
				{
					for (vector <furniture>::iterator i_m = furn.begin(); i_m < furn.end(); i_m++)
					{
						if (floor_number == i_m->floor && i_m->object_is_in_selected_area == true)
							if (x_atm_real >= i_m->x1_real && x_atm_real <= i_m->x2_real && y_atm_real >= i_m->y1_real && y_atm_real <= i_m->y2_real)
							{
								CursorOnObject = true;
								break;
							}
					}
				}
			}

			if (CursorOnObject)
			{
				Render::SaveAction();
				person_selected = true;
				x_start_person = ax / WinWid * WinWid_f / Scale;
				y_start_person = ay / WinHei * WinHei_f / Scale;
				furn_selected = true;
				x_start_mebel = ax / WinWid * WinWid_f / Scale;
				y_start_mebel = ay / WinHei * WinHei_f / Scale;
				selecting_mode = false;
			}
			else
			{

			selecting_mode = false;
			CommonFieldWorks = false;
			for (vector <furniture>::iterator k = furn.begin(); k < furn.end(); k++)
			{
				if (k->object_is_in_selected_area)
				{
					k->object_is_in_selected_area = false;
				}
			}
			for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
			{
				if (k->object_is_in_selected_area)
				{
					k->object_is_in_selected_area = false;
				}
			}
			show_selected_area = false;
			for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
			{
				if (i->marked == true)
				{
					i->marked = false;
				}
			}
			for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
			{
				if (i->marked == true)
				{
					i->marked = false;
				}
			}
			for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
			{
				if (j->marked == true)
					j->marked = false;
			}
			Form1::Instance->propertyGrid1->Enabled = false;
			Form1::Instance->propertyGrid1->SelectedObject = NULL;
			Form1::Instance->propertyGrid1->Enabled = true;
			Form1::Instance->����������ToolStripMenuItem->Visible = false;
			bool click_on_door = false;

			vector <TDoor>::iterator i_d;
			int ccount_d = -1;
			for (i_d = door.begin(); i_d < door.end(); i_d++)
			{
				ccount_d++;
				if (floor_number == i_d->floor)
					if (x_atm_real >= i_d->x1 - 0.1 && x_atm_real <= i_d->x2 + 0.1 && y_atm_real >= i_d->y1 - 0.1 && y_atm_real <= i_d->y2 + 0.1)
					{
						i_d->marked = true;
						for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
						{
							if (j->marked == true && j != i_d)
								j->marked = false;
						}
						for (vector <furniture>::iterator j = furn.begin(); j < furn.end(); j++)
						{
							if (j->marked == true)
								j->marked = false;
						}
						for (vector <people>::iterator j = peop.begin(); j < peop.end(); j++)
						{
							if (j->marked == true)
								j->marked = false;
						}
						break;
					}
			}
			x_atm_real = ((ax - WinWid / 2) / Scale - dx * WinWid / WinWid_f) / WinWid_f * (max_x_box - min_x_box) + min_x_box;
			y_atm_real = ((WinHei - ay - WinHei / 2) / Scale + dy * WinHei / WinHei_f) / WinHei_f * (max_y_box - min_y_box) + min_y_box;
			if (i_d != door.end())
			{
				doorCLI ^ door2 = gcnew doorCLI();
				door2->original = &render->door[ccount_d];
				Form1::Instance->propertyGrid1->Enabled = false;
				Form1::Instance->propertyGrid1->SelectedObject = door2;
				Form1::Instance->propertyGrid1->Enabled = true;
				click_on_door = true;
			}
			if (!click_on_door)
			{
				selecting_mode = true;
				selecting_mode_free = true;
			}
		}
			}
		if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
		{
			if (person_selected)
			{
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area)
					{
						person_selected = false;
						double x_prev = i->x, y_prev = i->y, x_real_prev = i->x_real, y_real_prev = i->y_real, z_prev = i->z;
						int box_id_prev = i->box_id;
						i->x = (((i->x + dx * WinWid / WinWid_f) * Scale + WinWid / 2 + ((x_pers * Scale + x_start_person * Scale) / WinWid_f * WinWid - x_start_person * WinWid / WinWid_f * Scale)) - WinWid / 2) / Scale - dx * WinWid / WinWid_f;
						i->y = (WinHei / 2 - ((((i->y - dy * WinHei / WinHei_f) * Scale - WinHei / 2) * (-1)) + ((y_pers * Scale + y_start_person * Scale) / WinHei_f * WinHei - y_start_person * WinHei / WinHei_f * Scale))) / Scale + dy * WinHei / WinHei_f;
						i->x_real = i->x / WinWid_f * (max_x_box - min_x_box) + min_x_box;
						i->y_real = i->y / WinHei_f * (max_y_box - min_y_box) + min_y_box;
						i->box_id = 0;
						bool add_new_item = false;
						for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
						{
							if (i->x_real > j->x1 && i->x_real < j->x2 && i->y_real > j->y1 && i->y_real < j->y2)
							{
								i->box_id = j->id;
								break;
							}
						}
						if (i->box_id != 0)
						{
							vector <TBox>::iterator j;
							for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
							{
								if (j->id == i->box_id)
									break;
							}
							i->z = j->z1;
							if (i->x_real - i->radius > j->x1 && i->x_real + i->radius < j->x2 && i->y_real - i->radius > j->y1 && i->y_real + i->radius < j->y2)
							{
								add_new_item = true;
							}
							else
							{
								add_new_item = false;
							}
						}
						if (add_new_item)
						{
							//�������� �� ����������� � ������ ������� ��� ������
							for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
							{
								if (k != i && k->object_is_in_selected_area != true)
								{
									if (k->box_id == i->box_id)
										if ((k->x_real - i->x_real) * (k->x_real - i->x_real) + (k->y_real - i->y_real) * (k->y_real - i->y_real) <= (i->radius + k->radius) * (i->radius + k->radius))
										{
											add_new_item = false;
											break;
										}
								}
							}
							for (vector <furniture>::iterator k = furn.begin(); k < furn.end(); k++)
							{
								if (k->box_id == i->box_id && k->object_is_in_selected_area != true)
									if (i->x_real + i->radius > k->x1_real && i->x_real - i->radius < k->x2_real && i->y_real + i->radius > k->y1_real && i->y_real - i->radius < k->y2_real)
									{
										add_new_item = false;
										break;
									}
							}
						}
						if (!add_new_item)
						{
							i->x = x_prev;
							i->y = y_prev;
							i->x_real = x_real_prev;
							i->y_real = y_real_prev;
							i->z = z_prev;
							i->box_id = box_id_prev;
						}
						else
						{
							i->start_room = i->box_id;
						}
						
					}
				}
				x_pers = 0.0;
				y_pers = 0.0;
			}
			if (furn_selected)
			{
				for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
				{
					if (i->object_is_in_selected_area)
					{
						furn_selected = false;
						double x1_prev = i->x1, y1_prev = i->y1, x2_prev = i->x2, y2_prev = i->y2, x1_real_prev = i->x1_real, y1_real_prev = i->y1_real, x2_real_prev = i->x2_real, y2_real_prev = i->y2_real, z_prev = i->z;
						int box_id_prev = i->box_id;
						i->x2 = ((((i->x2 + dx * WinWid / WinWid_f) * Scale + WinWid / 2) + ((x_mebel * Scale + x_start_mebel * Scale) / WinWid_f * WinWid - x_start_mebel * WinWid / WinWid_f * Scale)) - WinWid / 2) / Scale - dx * WinWid / WinWid_f;
						i->y2 = (WinHei / 2 - ((((i->y2 - dy * WinHei / WinHei_f) * Scale - WinHei / 2) * (-1)) + ((y_mebel * Scale + y_start_mebel * Scale) / WinHei_f * WinHei - y_start_mebel * WinHei / WinHei_f * Scale))) / Scale + dy * WinHei / WinHei_f;
						i->x1 = i->x2 - (i->width * WinWid_f / (max_x_box - min_x_box));
						i->y1 = i->y2 - (i->length * WinHei_f / (max_y_box - min_y_box));
						i->x1_real = i->x1 / WinWid_f * (max_x_box - min_x_box) + min_x_box;
						i->x2_real = i->x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box;
						i->y1_real = i->y1 / WinHei_f * (max_y_box - min_y_box) + min_y_box;
						i->y2_real = i->y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box;
						i->box_id = 0;
						bool add_new_item = false;
						for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
						{
							if (i->x1_real > j->x1 && i->x1_real < j->x2 && i->y1_real > j->y1 && i->y1_real < j->y2)
							{
								i->box_id = j->id;
								break;
							}
						}
						if (i->box_id != 0)
						{
							vector <TBox>::iterator j;
							for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
							{
								if (j->id == i->box_id)
									break;
							}
							i->z = j->z1;
							if (i->x2_real > j->x1 && i->x2_real < j->x2 && i->y2_real > j->y1 && i->y2_real < j->y2)
							{
								add_new_item = true;
							}
							/*else
							{
								if (i->x2_real >= j->x2)
								{
									i->x2_real = j->x2;
									i->x2 = (i->x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);// field.x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box
								}
								if (i->x2_real <= j->x1)
								{
									i->x2_real = j->x1;
									i->x2 = (i->x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
								}
								if (i->y2_real >= j->y2)
								{
									i->y2_real = j->y2;
									i->y2 = (i->y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);// field.y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box
								}
								if (i->y2_real <= j->y1)
								{
									i->y2_real = j->y1;
									i->y2 = (i->y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
								}
								add_new_item = true;
							}*/
						}
						else
						{
							for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
							{
								if (i->x2_real > j->x1 && i->x2_real < j->x2 && i->y2_real > j->y1 && i->y2_real < j->y2)
								{
									i->box_id = j->id;
									break;
								}
							}
							if (i->box_id != 0)
							{
								vector <TBox>::iterator j;
								for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
								{
									if (j->id == i->box_id)
										break;
								}
								i->z = j->z1;
								if (i->x1_real > j->x1 && i->x1_real < j->x2 && i->y1_real > j->y1 && i->y1_real < j->y2)
								{
									add_new_item = true;
								}
								/*else
								{
									if (i->x1_real >= j->x2)
									{
										i->x1_real = j->x2;
										i->x1 = (i->x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);// field.x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box
									}
									if (i->x1_real <= j->x1)
									{
										i->x1_real = j->x1;
										i->x1 = (i->x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
									}
									if (i->y1_real >= j->y2)
									{
										i->y1_real = j->y2;
										i->y1 = (i->y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);// field.y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box
									}
									if (i->y1_real <= j->y1)
									{
										i->y1_real = j->y1;
										i->y1 = (i->y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
									}
									add_new_item = true;
								}*/
							}
						}
						if (add_new_item)
						{
							//�������������� ������ (���������)
							/*if (i->x1 > i->x2)
							{
								double temp = i->x2;
								i->x2 = i->x1;
								i->x1 = temp;
								temp = i->x2_real;
								i->x2_real = i->x1_real;
								i->x1_real = temp;
							}
							if (i->y1 > i->y2)
							{
								double temp = i->y2;
								i->y2 = i->y1;
								i->y1 = temp;
								temp = i->y2_real;
								i->y2_real = i->y1_real;
								i->y1_real = temp;
							}*/
							//�������� �� ����������� � ������ �������
							for (vector <furniture>::iterator k = furn.begin(); k < furn.end(); k++)
							{
								if (k != i && k->object_is_in_selected_area != true)
								{
									if (k->box_id == i->box_id)
										if (((i->x1_real <= k->x1_real && i->x2_real >= k->x1_real) || (i->x1_real <= k->x2_real && i->x2_real >= k->x2_real) || (i->x2_real <= k->x2_real && i->x1_real >= k->x1_real)) && ((i->y1_real <= k->y1_real && i->y2_real >= k->y1_real) || (i->y1_real <= k->y2_real && i->y2_real >= k->y2_real) || (i->y2_real <= k->y2_real && i->y1_real >= k->y1_real)))
										{
											add_new_item = false;
											break;
										}
								}
							}
							for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
							{
								if (k->box_id == i->box_id && k->object_is_in_selected_area != true)
									if (k->x_real + k->radius > i->x1_real && k->x_real - k->radius < i->x2_real && k->y_real + k->radius > i->y1_real && k->y_real - k->radius < i->y2_real)
									{
										add_new_item = false;
										break;
									}
							}
						}
						if (!add_new_item)
						{
							i->x1 = x1_prev;
							i->y1 = y1_prev;
							i->x1_real = x1_real_prev;
							i->y1_real = y1_real_prev;
							i->x2 = x2_prev;
							i->y2 = y2_prev;
							i->x2_real = x2_real_prev;
							i->y2_real = y2_real_prev;
							i->z = z_prev;
							i->box_id = box_id_prev;
						}
					}
				}
				x_mebel = 0.0;
				y_mebel = 0.0;
			}
		}
	}

	if (selecting_mode)
	{
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{
			CommonFieldWorks = false;
			for (vector <furniture>::iterator k = furn.begin(); k < furn.end(); k++)
			{
				if (k->object_is_in_selected_area)
				{
					k->object_is_in_selected_area = false;
				}
			}
			for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
			{
				if (k->object_is_in_selected_area)
				{
					k->object_is_in_selected_area = false;
				}
			}
			show_selected_area = false;
			for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
			{
				if (i->marked == true)
				{
					i->marked = false;
				}
			}
			for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
			{
				if (i->marked == true)
				{
					i->marked = false;
				}
			}
			for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
			{
				if (j->marked == true)
					j->marked = false;
			}
			Form1::Instance->propertyGrid1->Enabled = false;
			Form1::Instance->propertyGrid1->SelectedObject = NULL;
			Form1::Instance->propertyGrid1->Enabled = true;
			Form1::Instance->����������ToolStripMenuItem->Visible = false;
			x_start = ax / WinWid * WinWid_f / Scale;
			y_start = ay / WinHei * WinHei_f / Scale;
			x1_selected = (x_start * WinWid / WinWid_f * Scale - WinWid / 2) / Scale - dx * WinWid / WinWid_f;
			y1_selected = (WinHei - y_start * WinHei / WinHei_f * Scale - WinHei / 2) / Scale + dy * WinHei / WinHei_f;
			x1_selected_real = x1_selected / WinWid_f * (max_x_box - min_x_box) + min_x_box;
			y1_selected_real = y1_selected / WinHei_f * (max_y_box - min_y_box) + min_y_box;
		}
		if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
		{
			x_select = 0.0;
			y_select = 0.0;
			
			x2_selected = (ax - WinWid / 2) / Scale - dx * WinWid / WinWid_f;
			y2_selected = (WinHei - ay - WinHei / 2) / Scale + dy * WinHei / WinHei_f;
			x2_selected_real = x2_selected / WinWid_f * (max_x_box - min_x_box) + min_x_box;
			y2_selected_real = y2_selected / WinHei_f * (max_y_box - min_y_box) + min_y_box;
			//���������� ���������
			if (x1_selected > x2_selected)
			{
				double temp = x2_selected;
				x2_selected = x1_selected;
				x1_selected = temp;
				temp = x2_selected_real;
				x2_selected_real = x1_selected_real;
				x1_selected_real = temp;
			}
			if (y1_selected > y2_selected)
			{
				double temp = y2_selected;
				y2_selected = y1_selected;
				y1_selected = temp;
				temp = y2_selected_real;
				y2_selected_real = y1_selected_real;
				y1_selected_real = temp;
			}
			//���������� ���������
			show_selected_area = true;
			selecting_mode = false;
			if (!SelectingForGenerating)
			{
				show_selected_area = false;
				for (vector <furniture>::iterator j = furn.begin(); j < furn.end(); j++)
				{
					if (j->floor == floor_number 
						&& ((j->x1_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x2_real > x2_selected_real && j->x1_real > x1_selected_real && j->x1_real < x2_selected_real && j->y1_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x1_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x1_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real > y1_selected_real && j->y1_real < y2_selected_real && j->y2_real > y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real > y1_selected_real && j->y1_real < y2_selected_real && j->y2_real > y2_selected_real)
						|| (j->x1_real > x1_selected_real && j->x1_real < x2_selected_real && j->x2_real > x2_selected_real && j->y1_real > y1_selected_real && j->y1_real < y2_selected_real && j->y2_real > y2_selected_real)
						|| (j->x1_real > x1_selected_real && j->x1_real < x2_selected_real && j->x2_real > x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x2_selected_real && j->y1_real > y1_selected_real && j->y1_real < y2_selected_real && j->y2_real > y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y2_selected_real)
						|| (j->x1_real > x1_selected_real && j->x1_real < x2_selected_real && j->x2_real > x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y2_selected_real)
						|| (j->x1_real < x1_selected_real && j->x2_real > x2_selected_real && j->y1_real > y1_selected_real && j->y2_real < y2_selected_real)
						|| (j->x1_real > x1_selected_real && j->x2_real < x2_selected_real && j->y1_real < y1_selected_real && j->y2_real > y2_selected_real)))
					{
						j->object_is_in_selected_area = true;
					}
				}
				int PeopleSelectedCounter = 0;
				for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
				{
					if (k->floor == floor_number 
						&& ((k->x_real + k->radius > x1_selected_real && k->x_real - k->radius < x2_selected_real && k->y_real > y1_selected_real && k->y_real < y2_selected_real) 
						|| (k->y_real + k->radius > y1_selected_real && k->y_real - k->radius < y2_selected_real && k->x_real > x1_selected_real && k->x_real < x2_selected_real)
						|| ((k->x_real - x1_selected_real) * (k->x_real - x1_selected_real) + (k->y_real - y1_selected_real) * (k->y_real - y1_selected_real) < k->radius * k->radius)
						|| ((k->x_real - x1_selected_real) * (k->x_real - x1_selected_real) + (k->y_real - y2_selected_real) * (k->y_real - y2_selected_real) < k->radius * k->radius)
						|| ((k->x_real - x2_selected_real) * (k->x_real - x2_selected_real) + (k->y_real - y2_selected_real) * (k->y_real - y2_selected_real) < k->radius * k->radius)
						|| ((k->x_real - x2_selected_real) * (k->x_real - x2_selected_real) + (k->y_real - y1_selected_real) * (k->y_real - y1_selected_real) < k->radius * k->radius)))
					{
						k->object_is_in_selected_area = true;
						PeopleSelectedCounter++;
					}
				}
				Form1::Instance->����������ToolStripMenuItem->Visible = true;
				int Obj = 0;
				int c_to_out = -1;
				bool addingPluses = true;
				for (vector <furniture>::iterator j = furn.begin(); j < furn.end(); j++)
				{
					if (addingPluses)
						c_to_out++;
					if (j->object_is_in_selected_area)
					{
						Obj++;
						addingPluses = false;
					}
				}
				if (Obj == 1)
				{
					furnitureCLI ^ furn2 = gcnew furnitureCLI();
					furn2->original = &render->furn[c_to_out];
					Form1::Instance->propertyGrid1->Enabled = false;
					Form1::Instance->propertyGrid1->SelectedObject = furn2;
					Form1::Instance->propertyGrid1->Enabled = true;
				}
				if (PeopleSelectedCounter > 0)
				{
					emptyField.id = -100;
					emptyField.tp = -100;
					emptyField.start_time = -100;
					emptyField.exit = -100;
					emptyField.size = -100;
					emptyField.mobylity = -100;
					emptyField.age = -100;
					emptyField.emostate = -100;
					emptyField.sex = -100;
					emptyField.color = -100;
					emptyField.control = -100;
					emptyField.role = -100;
					emptyField.educlevel = -100;
					emptyField.start_room = -100;
					emptyField.x_real = -1000;
					emptyField.y_real = -1000;
					emptyField.z = -1000;
					emptyField.door1 = -100;
					emptyField.door2 = -100;
					emptyField.door3 = -100;
					emptyField.door4 = -100;
					emptyField.door5 = -100;
					bool IdDiffer = false;
					bool TpDiffer = false;
					bool StartTimeDiffer = false;
					bool ExitDiffer = false;
					bool SizeDiffer = false;
					bool MobilityDiffer = false;
					bool AgeDiffer = false;
					bool EmostateDiffer = false;
					bool SexDiffer = false;
					bool ColorDiffer = false;
					bool ControlDiffer = false;
					bool RoleDiffer = false;
					bool EduclevelDiffer = false;
					bool StartroomDiffer = false;
					bool XDiffer = false;
					bool YDiffer = false;
					bool ZDiffer = false;
					bool door1Differ = false;
					bool door2Differ = false;
					bool door3Differ = false;
					bool door4Differ = false;
					bool door5Differ = false;
					bool ValueFixed = false;
					int IdToWrite;
					int TpToWrite;
					int StartTimeToWrite;
					int ExitToWrite;
					int MobilityToWrite;
					int AgeToWrite;
					int EmostateToWrite;
					int SexToWrite;
					int ColorToWrite;
					int ControlToWrite;
					int RoleToWrite;
					int EduclevelToWrite;
					int StartroomToWrite;
					int door1ToWrite;
					int door2ToWrite;
					int door3ToWrite;
					int door4ToWrite;
					int door5ToWrite;
					double SizeToWrite;
					double XToWrite;
					double YToWrite;
					double ZToWrite;
					for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
					{
						if (k->object_is_in_selected_area == true)
						{
							if (!ValueFixed)
							{
								IdToWrite = k->id;
								TpToWrite = k->tp;
								StartTimeToWrite = k->start_time;
								ExitToWrite = k->exit;
								SizeToWrite = k->size;
								MobilityToWrite = k->mobylity;
								AgeToWrite = k->age;
								EmostateToWrite = k->emostate;
								SexToWrite = k->sex;
								ColorToWrite = k->color;
								ControlToWrite = k->control;
								RoleToWrite = k->role;
								EduclevelToWrite = k->educlevel;
								StartroomToWrite = k->start_room;
								XToWrite = k->x_real;
								YToWrite = k->y_real;
								ZToWrite = k->z;
								door1ToWrite = k->door1;
								door2ToWrite = k->door2;
								door3ToWrite = k->door3;
								door4ToWrite = k->door4;
								door5ToWrite = k->door5;
								ValueFixed = true;
							}
							else
							{
								if (k->id != IdToWrite)
								{
									IdDiffer = true;
								}
								if (k->tp != TpToWrite)
								{
									TpDiffer = true;
								}
								if (k->start_time != StartTimeToWrite)
								{
									StartTimeDiffer = true;
								}
								if (k->exit != ExitToWrite)
								{
									ExitDiffer = true;
								}
								if (k->door1 != door1ToWrite)
								{
									door1Differ = true;
								}
								if (k->door2 != door2ToWrite)
								{
									door2Differ = true;
								}
								if (k->door3 != door3ToWrite)
								{
									door3Differ = true;
								}
								if (k->door4 != door4ToWrite)
								{
									door4Differ = true;
								}
								if (k->door5 != door5ToWrite)
								{
									door5Differ = true;
								}
								if (fabs(k->size - SizeToWrite) > 0.0001)
								{
									SizeDiffer = true;
								}
								if (k->mobylity != MobilityToWrite)
								{
									MobilityDiffer = true;
								}
								if (k->age != AgeToWrite)
								{
									AgeDiffer = true;
								}
								if (k->emostate != EmostateToWrite)
								{
									EmostateDiffer = true;
								}
								if (k->sex != SexToWrite)
								{
									SexDiffer = true;
								}
								if (k->color != ColorToWrite)
								{
									ColorDiffer = true;
								}
								if (k->control != ControlToWrite)
								{
									ControlDiffer = true;
								}
								if (k->role != RoleToWrite)
								{
									RoleDiffer = true;
								}
								if (k->educlevel != EduclevelToWrite)
								{
									EduclevelDiffer = true;
								}
								if (k->start_room != StartroomToWrite)
								{
									StartroomDiffer = true;
								}
								if (fabs(k->x_real - XToWrite) > 0.000001)
								{
									XDiffer = true;
								}
								if (fabs(k->y_real - YToWrite) > 0.000001)
								{
									YDiffer = true;
								}
								if (fabs(k->z - ZToWrite) > 0.0001)
								{
									ZDiffer = true;
								}
							}
						}
					}
					if (!IdDiffer)
						emptyField.id = IdToWrite;
					if (!TpDiffer)
						emptyField.tp = TpToWrite;
					if (!StartTimeDiffer)
						emptyField.start_time = StartTimeToWrite;
					if (!ExitDiffer)
						emptyField.exit = ExitToWrite;
					if (!door1Differ)
						emptyField.door1 = door1ToWrite;
					if (!door2Differ)
						emptyField.door2 = door2ToWrite;
					if (!door3Differ)
						emptyField.door3 = door3ToWrite;
					if (!door4Differ)
						emptyField.door4 = door4ToWrite;
					if (!door5Differ)
						emptyField.door5 = door5ToWrite;
					if (!SizeDiffer)
						emptyField.size = SizeToWrite;
					if (!MobilityDiffer)
						emptyField.mobylity = MobilityToWrite;
					if (!AgeDiffer)
						emptyField.age = AgeToWrite;
					if (!EmostateDiffer)
						emptyField.emostate = EmostateToWrite;
					if (!SexDiffer)
						emptyField.sex = SexToWrite;
					if (!ColorDiffer)
						emptyField.color = ColorToWrite;
					if (!ControlDiffer)
						emptyField.control = ControlToWrite;
					if (!RoleDiffer)
						emptyField.role = RoleToWrite;
					if (!EduclevelDiffer)
						emptyField.educlevel = EduclevelToWrite;
					if (!StartroomDiffer)
						emptyField.start_room = StartroomToWrite;
					if (!XDiffer)
						emptyField.x_real = XToWrite;
					if (!YDiffer)
						emptyField.y_real = YToWrite;
					if (!ZDiffer)
						emptyField.z = ZToWrite;
					peopleCLI ^ peop2 = gcnew peopleCLI();
					peop2->original = &emptyField;
					Form1::Instance->propertyGrid1->Enabled = false;
					Form1::Instance->propertyGrid1->SelectedObject = peop2;
					Form1::Instance->propertyGrid1->Enabled = true;
					CommonFieldWorks = true;
					FieldToCheck = emptyField;
					
				}
			}
			else
			{
				SelectingForGenerating = false;
			}
			if (!selecting_mode_free)
			{
				Form1::Instance->panel3->Visible = false;
				Form1::Instance->label2->Visible = false;
				Form1::Instance->panel2->Visible = true;
				Form1::Instance->panel4->Visible = true;
				Form1::Instance->label1->Visible = true;
				Form1::Instance->label3->Visible = true;
				Form1::Instance->textBox1->Text = "0";
				Form1::Instance->textBox1->Visible = true;
				Form1::Instance->textBox2->Text = "0";
				Form1::Instance->textBox2->Visible = true;
				Form1::Instance->button3->Visible = true;
				Form1::Instance->button4->Visible = true;
				Form1::Instance->button5->Visible = true;
				Form1::Instance->button6->Visible = true;
			}
			else
			{
				selecting_mode_free = false;
			}
		}
	}
	//state = {GLUT_UP, GLUT_DOWN}
	}
}


//�������, ���������� ��� ����������� �����������
//���������� �������
void Render::StopShowingSelectedArea()
{
	show_selected_area = false;
}

//������� ��� ����������� ������ ���������� �������� � ������
void Render::CopyObjects()
{
	peop_copy.clear();
	furn_copy.clear();
	for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
	{
		if (i->object_is_in_selected_area == true)
		{
			peop_copy.push_back(*i);
		}
	}
	for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
	{
		if (i->object_is_in_selected_area == true)
		{
			furn_copy.push_back(*i);
		}
	}
	double xCopyMin = 9999.0, yCopyMin = 9999.0;
	for (vector <people>::iterator i = peop_copy.begin(); i < peop_copy.end(); i++)
	{
		if (i->x_real < xCopyMin)
			xCopyMin = i->x_real;
		if (i->y_real < yCopyMin)
			yCopyMin = i->y_real;
	}
	for (vector <furniture>::iterator i = furn_copy.begin(); i < furn_copy.end(); i++)
	{
		if (i->x1_real < xCopyMin)
			xCopyMin = i->x1_real;
		if (i->y1_real < yCopyMin)
			yCopyMin = i->y1_real;
	}
	for (vector <people>::iterator i = peop_copy.begin(); i < peop_copy.end(); i++)
	{
		i->x_real = i->x_real - xCopyMin;
		i->y_real = i->y_real - yCopyMin;
	}
	for (vector <furniture>::iterator i = furn_copy.begin(); i < furn_copy.end(); i++)
	{
		i->x1_real = i->x1_real - xCopyMin;
		i->y1_real = i->y1_real - yCopyMin;
		i->x2_real = i->x2_real - xCopyMin;
		i->y2_real = i->y2_real - yCopyMin;
	}
}

//�������, ���������� ��� ������� ������ ��������
//���������:
//int ax, int ay -- ���������� ����� �������
void Render::PasteObjects(int ax, int ay)
{
	Render::SaveAction();
	vector <people> peopReadyToPaste;
	vector <furniture> furnReadyToPaste;
	double xPasteReal = (((ax / WinWid * WinWid_f / Scale) * WinWid / WinWid_f * Scale - WinWid / 2) / Scale - dx * WinWid / WinWid_f) / WinWid_f * (max_x_box - min_x_box) + min_x_box;
	double yPasteReal = ((WinHei - (ay / WinHei * WinHei_f / Scale) * WinHei / WinHei_f * Scale - WinHei / 2) / Scale + dy * WinHei / WinHei_f) / WinHei_f * (max_y_box - min_y_box) + min_y_box;

	bool CallingMessageBox = false;
	for (vector <people>::iterator iter = peop_copy.begin(); iter < peop_copy.end(); iter++)
	{
		people field;
		field = *iter;
		field.x_real = field.x_real + xPasteReal;
		field.y_real = field.y_real + yPasteReal;

		field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
		field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
		//�������� �� ����������� �� �������, ������, �������
		field.box_id = -1;
		bool add_new_item = false;
		for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
		{
			if (field.x_real > j->x1 && field.x_real < j->x2 && field.y_real > j->y1 && field.y_real < j->y2)
			{
				field.box_id = j->id;
				break;
			}
		}
		if (field.box_id != -1)
		{
			vector <TBox>::iterator j;
			for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
			{
				if (j->id == field.box_id)
					break;
			}
			field.z = j->z1;
			field.start_room = j->id;
			if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
			{
				add_new_item = true;
			}
			else
			{
				add_new_item = false;
			}
		}
		if (add_new_item)
		{
			//�������� �� ����������� � ������ ������� ��� ������
			for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
			{
				if (i->box_id == field.box_id)
					if ((i->x_real - field.x_real) * (i->x_real - field.x_real) + (i->y_real - field.y_real) * (i->y_real - field.y_real) <= 0.4 * 0.4)
					{
						add_new_item = false;
						break;
					}
			}
			for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
			{
				if (i->box_id == field.box_id)
					if (field.x_real + field.radius > i->x1_real && field.x_real - field.radius < i->x2_real && field.y_real + field.radius > i->y1_real && field.y_real - field.radius < i->y2_real)
					{
						add_new_item = false;
						break;
					}
			}
		}
		if (add_new_item)
		{
			field.floor = floor_number;
			field.marked = false;
			field.object_is_in_selected_area = false;
			people_id++;
			field.id = people_id;
			peopReadyToPaste.push_back(field);
			/*people_counter = 0;
			for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
			{
				if (pe->floor == floor_number)
				{
					people_counter++;
				}
			}
			Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();*/
		}
		else
		{
			CallingMessageBox = true;
		}
	}

	for (vector <furniture>::iterator iter = furn_copy.begin(); iter < furn_copy.end(); iter++)
	{
		furniture field = *iter;
		field.x1_real = field.x1_real + xPasteReal;
		field.y1_real = field.y1_real + yPasteReal;
		field.x2_real = field.x2_real + xPasteReal;
		field.y2_real = field.y2_real + yPasteReal;

		field.x1 = (field.x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
		field.x2 = (field.x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
		field.y1 = (field.y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
		field.y2 = (field.y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);

		field.floor = floor_number;
		vector <TBox>::iterator j;
		field.box_id = -1;
		for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
		{
			if (field.x1_real > j->x1 && field.x2_real < j->x2 && field.y1_real > j->y1 && field.y2_real < j->y2)
			{
				field.box_id = j->id;
				field.z = j->z1;
				break;
			}
		}
		if (field.box_id == -1)
		{
			CallingMessageBox = true;
		}
		else
		{
			field.marked = false;
			field.object_is_in_selected_area = false;
			bool add_new_item = true;

			if (add_new_item)
			{
				//�������� �� ����������� � ������ �������
				for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
				{
					if (i->box_id == field.box_id)
						if (((field.x1_real <= i->x1_real && field.x2_real >= i->x1_real) || (field.x1_real <= i->x2_real && field.x2_real >= i->x2_real) || (field.x2_real <= i->x2_real && field.x1_real >= i->x1_real)) && ((field.y1_real <= i->y1_real && field.y2_real >= i->y1_real) || (field.y1_real <= i->y2_real && field.y2_real >= i->y2_real) || (field.y2_real <= i->y2_real && field.y1_real >= i->y1_real)))
						{
							add_new_item = false;
							break;
						}
				}
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->box_id == field.box_id)
						if (i->x_real + i->radius > field.x1_real && i->x_real - i->radius < field.x2_real && i->y_real + i->radius > field.y1_real && i->y_real - i->radius < field.y2_real)
						{
							add_new_item = false;
							break;
						}
				}
			}
			if (add_new_item)
			{
				furniture_id++;
				field.id = furniture_id;
				furnReadyToPaste.push_back(field);
			}
			else
			{
				CallingMessageBox = true;
			}
		}
	}
	

	for (vector <furniture>::iterator iter = furnReadyToPaste.begin(); iter < furnReadyToPaste.end(); iter++)
	{
		furn.push_back(*iter);
	}

	for (vector <people>::iterator iter = peopReadyToPaste.begin(); iter < peopReadyToPaste.end(); iter++)
	{
//DEMO		if (peop.size() > 19)												//DEMO
//DEMO		{																	//DEMO
//DEMO			MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO			break;															//DEMO
//DEMO		}																	//DEMO
//DEMO		else																//DEMO
//DEMO		{																	//DEMO
			peop.push_back(*iter);
//DEMO		}																	//DEMO
	}
	int people_counter = 0;
	for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
	{
		if (pe->floor == floor_number)
		{
			people_counter++;
		}
	}
	Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
	if (CallingMessageBox)
	{
		MessageBox::Show("��������� ������� �� ���� ���������", "����������� ��������");
	}
}


//��������������� ������� ��� ��������� �����.
//������� ���������� �������������� ������ ����� � ������ �������.
//���������:
//TBox j -- ����� �����, � ������� ���������� ������������ �����
//int &CurrentNumber -- ������ �� ����������, ����������� ���������� ��� ��������������� �������
//double x1, double x2, double y1, double y2 -- ���������� �����
bool Render::SubFunc(TBox j, int &CurrentNumber, double x1, double x2, double y1, double y2)
{
	int AttemptNumber = (int) (fabs(x2 - x1) * fabs(y2 - y1) / (0.125));
	bool add_new_item = false;
	double Mx, My;
	do
	{
		AttemptNumber--;
		Mx = (rand() % (int) (fabs(x2 - x1) * 100)) / 100. + x1;
		My = (rand() % (int) (fabs(y2 - y1) * 100)) / 100. + y1;
		add_new_item = false;
		if (Mx - 0.2 > x1 && Mx + 0.2 < x2 && My - 0.2 > y1 && My + 0.2 < y2)
		{
			add_new_item = true;
		}
		else
		{
			add_new_item = false;
		}
		if (add_new_item)
		{
			//�������� �� ����������� � ������ ������� ��� ������
			for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
			{
				if (i->box_id == j.id)
					if ((i->x_real - Mx) * (i->x_real - Mx) + (i->y_real - My) * (i->y_real - My) <= 0.4 * 0.4)
					{
						add_new_item = false;
						break;
					}
			}
			for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
			{
				if (i->box_id == j.id)
					if (Mx + 0.2 > i->x1_real && Mx - 0.2 < i->x2_real && My + 0.2 > i->y1_real && My - 0.2 < i->y2_real)
					{
						add_new_item = false;
						break;
					}
			}
		}
	}
	while (!add_new_item && AttemptNumber >= 0);
	if (add_new_item)
	{
		people_id++;
		people field;
		field.id = people_id;
		field.box_id = j.id;
		field.radius = 0.2;
		field.z = j.z1;
		field.marked = false;
		field.object_is_in_selected_area = false;
		field.floor = floor_number;
		field.x_real = Mx;
		field.y_real = My;
		field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
		field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
		field.tp = 0;
		field.age = 5;
		field.color = 16776960;
		field.control = 0;
		field.educlevel = 0;
		field.emostate = 0;
		field.exit = 0;
		field.mobylity = 1;
		field.role = 0;
		field.sex = 1;
		field.size = 0.125;
		field.start_room = field.box_id;
		field.start_time = 0;
		field.door1 = 0;
		field.door2 = 0;
		field.door3 = 0;
		field.door4 = 0;
		field.door5 = 0;
		
//DEMO		if (peop.size() > 19)												//DEMO
//DEMO		{																	//DEMO
//DEMO			MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO			return false;													//DEMO
//DEMO		}																	//DEMO
//DEMO		else																//DEMO
//DEMO		{																	//DEMO
			peop.push_back(field);
//DEMO		}																	//DEMO
		people_counter = 0;
		for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
		{
			if (pe->floor == floor_number)
			{
				people_counter++;
			}
		}
		Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
		CurrentNumber--;
		return true;
	}
	else
	{
		return false;
	}
}


//������� ��� ����������� ����� ��������� ������� �� ���������� �������.
//���������:
//int number -- ����� ����� ��� �����������
void Render::RandomPeopleOnSelectedArea(int number)
{
//DEMO	if (number + peop.size() > 20)										//DEMO
//DEMO	{																	//DEMO
//DEMO		MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO		show_selected_area = false;										//DEMO
//DEMO	}																	//DEMO
//DEMO	else																//DEMO
	{
	Render::SaveAction();
	int BoxNumber = box[floor_number].size();
	double *mas = new double [BoxNumber];
	int counter = -1;
	double CommonArea = 0;
	for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
	{
		double area = 0.0;
		counter++;
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(j->y2 - j->y1);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(j->y2 - j->y1);
		}
		if (j->x2 > x2_selected_real && j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(j->y2 - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(y2_selected_real - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(y2_selected_real - y1_selected_real);
		}
		if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(y2_selected_real - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(j->y2 - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(y2_selected_real - y1_selected_real);
		}
		mas[counter] = area;
		CommonArea += area;
	}
	counter = -1;
	int CurrentNumber = number;
	if (CommonArea > 0.001)
	{
		srand( (unsigned)time( NULL ) );
		for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
		{
			counter++;
			if (mas[counter] > 0.001)
			{
				for (int ii = 0; ((ii < (int) (mas[counter] / CommonArea * number)) && (CurrentNumber > 0)) || ((int) (mas[counter] / CommonArea * number) == 0 && ii == 0 && (CurrentNumber > 0)); ii++)
				{
					bool booltemp = false;
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, j->y2);
					}
					if (j->x2 > x2_selected_real && j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, j->y2);
					}
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, j->y2);
					}
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, y2_selected_real);
					}
					if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, y2_selected_real);
					}
					if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, y2_selected_real);
					}
					if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, j->y2);
					}
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, y2_selected_real);
					}
					if (!booltemp)
					{
						break;
					}
				}
			}
		}
		int counter2 = 0;
		do
		{
			counter2++;
			counter = -1;
			for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
			{
				counter++;
				if (mas[counter] > 0.001)
				{
					for (int ii = 0; ii < 1 && CurrentNumber > 0; ii++)
					{
						bool booltemp = false;
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, j->y2);
						}
						if (j->x2 > x2_selected_real && j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, j->y2);
						}
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, j->y2);
						}
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, y2_selected_real);
						}
						if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, y2_selected_real);
						}
						if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, y2_selected_real);
						}
						if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, j->y2);
						}
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, y2_selected_real);
						}
						if (!booltemp)
						{
							break;
						}
					}
				}
			}
		}
		while (CurrentNumber > 0 && counter2 < 6);
	}
	CurrentNumber = number - CurrentNumber;
	MessageBox::Show("��������� " + CurrentNumber.ToString() + " �������");
	delete [] mas;
	show_selected_area = false;
	}
}


//������� ����������� ����� �� �������� ��������� ������������� ����� � ���������� �������.
//���������:
//double density -- ��������� ������������� �����
void Render::DensityPeopleOnSelectedArea(double density)
{
	int BoxNumber = box[floor_number].size();
	double *mas = new double [BoxNumber];
	int counter = -1;
	double CommonArea = 0;
	for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
	{
		double area = 0.0;
		counter++;
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(j->y2 - j->y1);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(j->y2 - j->y1);
		}
		if (j->x2 > x2_selected_real && j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(j->y2 - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(y2_selected_real - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(j->y2 - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(y2_selected_real - j->y1);
		}
		if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - x1_selected_real) * fabs(y2_selected_real - y1_selected_real);
		}
		if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(x2_selected_real - j->x1) * fabs(y2_selected_real - y1_selected_real);
		}
		if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
		{
			area = fabs(x2_selected_real - x1_selected_real) * fabs(j->y2 - j->y1);
		}
		if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
		{
			area = fabs(j->x2 - j->x1) * fabs(y2_selected_real - y1_selected_real);
		}
		mas[counter] = area;
		CommonArea += area;
	}
	counter = -1;
	int CurrentNumber = (int) CommonArea * density;
	int number = CurrentNumber;
//DEMO	if (number + peop.size() > 20)										//DEMO
//DEMO	{																	//DEMO
//DEMO		MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO		show_selected_area = false;										//DEMO
//DEMO	}																	//DEMO
//DEMO	else																//DEMO
	{
	Render::SaveAction();
	if (CommonArea > 0.001)
	{
		srand( (unsigned)time( NULL ) );
		for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
		{
			counter++;
			if (mas[counter] > 0.001)
			{
				for (int ii = 0; ((ii < (int) (mas[counter] / CommonArea * number)) && (CurrentNumber > 0)) || ((int) (mas[counter] / CommonArea * number) == 0 && ii == 0 && (CurrentNumber > 0)); ii++)
				{
					bool booltemp = false;
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, j->y2);
					}
					if (j->x2 > x2_selected_real && j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, j->y2);
					}
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, j->y2);
					}
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, y2_selected_real);
					}
					if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, y2_selected_real);
					}
					if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, j->y2);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, y2_selected_real);
					}
					if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, y2_selected_real);
					}
					if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, j->y2);
					}
					if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
					{
						booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, y2_selected_real);
					}
					if (!booltemp)
					{
						break;
					}
				}
			}
		}
		int counter2 = 0;
		do
		{
			counter2++;
			counter = -1;
			for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
			{
				counter++;
				if (mas[counter] > 0.001)
				{
					for (int ii = 0; ii < 1 && CurrentNumber > 0; ii++)
					{
						bool booltemp = false;
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, j->y2);
						}
						if (j->x2 > x2_selected_real && j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, j->y2);
						}
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, j->y2);
						}
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, j->y1, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, j->y1, y2_selected_real);
						}
						if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, j->y1, y2_selected_real);
						}
						if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, y1_selected_real, j->y2);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, j->x2, y1_selected_real, y2_selected_real);
						}
						if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, x2_selected_real, y1_selected_real, y2_selected_real);
						}
						if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, x1_selected_real, x2_selected_real, j->y1, j->y2);
						}
						if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
						{
							booltemp = SubFunc(*j, CurrentNumber, j->x1, j->x2, y1_selected_real, y2_selected_real);
						}
						if (!booltemp)
						{
							break;
						}
					}
				}
			}
		}
		while (CurrentNumber > 0 && counter2 < 6);
	}
	CurrentNumber = number - CurrentNumber;
	MessageBox::Show("��������� " + CurrentNumber.ToString() + " �������");
	delete [] mas;
	show_selected_area = false;
	}
}


//�������, ���������� ��� ����� �������� �����.
//���������:
//int FloorNumber -- ����� �����, �� ������� ���� ������� �������
void Render::SwitchFloor(int FloorNumber)
{
	for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
	{
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
	{
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	show_selected_area = false;
	Form1::Instance->����������ToolStripMenuItem->Visible = false;
	for (int i = 0; i < floor.size(); i++)
	{
		if (floor[i].number == FloorNumber)
		{
			floor_number = i;
		}
	}
	people_counter = 0;
	for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
	{
		if (pe->floor == floor_number)
		{
			people_counter++;
		}
	}
	Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
	/*int i = 0;
	for (vector <TFloor>::iterator j = floor.begin(); j < floor.end(); j++)
	{
		if (strcmp(j->name, FloorNumber) == 0)
		{*/
//			floor_number = 1;
		/*}
		i++;
	}*/
}


//������� ��� ������� �������� ����� � �������� ������� ��� �������� �����
//���������:
//const char * Filename -- ��� ����� �����
void Render::SetInputFile(const char * Filename)
{
	filename_first = Filename;
	InputReady = true;
}


//������� ��� ���������� �������� ������ ������ � XML ����
//���������:
//const char* filename -- ��� �����, � ������� ������������ ������
void Render::SaveMebel(const char* filename)
{


	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "windows-1251", "" );
	doc.LinkEndChild( decl );
	
	TiXmlElement * element = new TiXmlElement( "BUILDING" );
	doc.LinkEndChild( element );

	TiXmlElement * element2 = new TiXmlElement( "ROOT" );
	element->LinkEndChild( element2 );

	TiXmlElement * element3 = new TiXmlElement( "BUILDING" );
	element2->LinkEndChild( element3 );

	TiXmlElement * element4 = new TiXmlElement( "CLASSNAME" );
	element3->LinkEndChild( element4 );

	TiXmlText * text = new TiXmlText( "TBuilding" );
	element4->LinkEndChild( text );

	TiXmlElement * element5 = new TiXmlElement( "ID" );
	element3->LinkEndChild( element5 );

	TiXmlText * text2 = new TiXmlText( "0" );
	element5->LinkEndChild( text2 );

	TiXmlElement * element6 = new TiXmlElement( "TP" );
	element3->LinkEndChild( element6 );

	TiXmlText * text3 = new TiXmlText( "1" );
	element6->LinkEndChild( text3 );

	TiXmlElement * element7 = new TiXmlElement( "NAME" );
	element3->LinkEndChild( element7 );

	TiXmlText * text4 = new TiXmlText( "������" );
	element7->LinkEndChild( text4 );

	TiXmlElement * element8 = new TiXmlElement( "FLOORLIST" );
	element3->LinkEndChild( element8 );

	/*TiXmlElement * element9 = new TiXmlElement( "ITEM0" );
	element8->LinkEndChild( element9 );

	TiXmlElement * element10 = new TiXmlElement( "CLASSNAME" );
	element9->LinkEndChild( element10 );

	TiXmlText * text5 = new TiXmlText( "TFloor" );
	element10->LinkEndChild( text5 );

	TiXmlElement * element11 = new TiXmlElement( "ID" );
	element9->LinkEndChild( element11 );

	TiXmlText * text6 = new TiXmlText( "1466" );
	element11->LinkEndChild( text6 );

	TiXmlElement * element12 = new TiXmlElement( "TP" );
	element9->LinkEndChild( element12 );

	TiXmlText * text7 = new TiXmlText( "0" );
	element12->LinkEndChild( text7 );

	TiXmlElement * element13 = new TiXmlElement( "NAME" );
	element9->LinkEndChild( element13 );

	TiXmlText * text8 = new TiXmlText( "��������� ����" );
	element13->LinkEndChild( text8 );

	TiXmlElement * element14 = new TiXmlElement( "NUMBER" );
	element9->LinkEndChild( element14 );

	TiXmlText * text9 = new TiXmlText( "0" );
	element14->LinkEndChild( text9 );

	TiXmlElement * element15 = new TiXmlElement( "FURNITURE" );
	element9->LinkEndChild( element15 );

	TiXmlElement * element16 = new TiXmlElement( "Count" );
	element15->LinkEndChild( element16 );

	TiXmlText * text10 = new TiXmlText( "0" );
	element16->LinkEndChild( text10 );*/

	for (int i = 0; i < floor.size(); i++)
	{
		char ITEM[7];
		switch (i)
		{
		case 0: strcpy(ITEM, "ITEM0"); break;
		case 1: strcpy(ITEM, "ITEM1"); break;
		case 2: strcpy(ITEM, "ITEM2"); break;
		case 3: strcpy(ITEM, "ITEM3"); break;
		case 4: strcpy(ITEM, "ITEM4"); break;
		case 5: strcpy(ITEM, "ITEM5"); break;
		case 6: strcpy(ITEM, "ITEM6"); break;
		case 7: strcpy(ITEM, "ITEM7"); break;
		case 8: strcpy(ITEM, "ITEM8"); break;
		case 9: strcpy(ITEM, "ITEM9"); break;
		case 10: strcpy(ITEM, "ITEM10"); break;
		case 11: strcpy(ITEM, "ITEM11"); break;
		}
		TiXmlElement * element17 = new TiXmlElement( ITEM );
		element8->LinkEndChild( element17 );

		TiXmlElement * element18 = new TiXmlElement( "CLASSNAME" );
		element17->LinkEndChild( element18 );

		TiXmlText * text11 = new TiXmlText( "TFloor" );
		element18->LinkEndChild( text11 );

		TiXmlElement * element19 = new TiXmlElement( "ID" );
		element17->LinkEndChild( element19 );

		char IDI[10];
		itoa(floor[i].id, IDI, 10);
		TiXmlText * text12 = new TiXmlText( IDI );
		element19->LinkEndChild( text12 );

		TiXmlElement * element20 = new TiXmlElement( "TP" );
		element17->LinkEndChild( element20 );

		char TPI[10];
		itoa(floor[i].tp, TPI, 10);
		TiXmlText * text13 = new TiXmlText( TPI );
		element20->LinkEndChild( text13 );

		TiXmlElement * element21 = new TiXmlElement( "NAME" );
		element17->LinkEndChild( element21 );

		TiXmlText * text14 = new TiXmlText( floor[i].name.c_str() );
		element21->LinkEndChild( text14 );

		TiXmlElement * element22 = new TiXmlElement( "NUMBER" );
		element17->LinkEndChild( element22 );

		char NUMBERI[10];
		itoa(floor[i].number, NUMBERI, 10);
		TiXmlText * text15 = new TiXmlText( NUMBERI );
		element22->LinkEndChild( text15 );

		TiXmlElement * element23 = new TiXmlElement( "FURNITURE" );
		element17->LinkEndChild( element23 );

		int item_counter = 0;

		for (vector <furniture>::iterator j = furn.begin(); j < furn.end(); j++)
		{
			if (j->floor == i)
			{
				char EITEM[10] = "ITEM";
				char ITEMX[10];
				itoa(item_counter, ITEMX, 10);
				strcat(EITEM, ITEMX);
				TiXmlElement * element24 = new TiXmlElement( EITEM );
				element23->LinkEndChild( element24 );

				TiXmlElement * element25 = new TiXmlElement( "CLASSNAME" );
				element24->LinkEndChild( element25 );

				TiXmlText * text16 = new TiXmlText( "TFurniture" );
				element25->LinkEndChild( text16 );

				TiXmlElement * element26 = new TiXmlElement( "ID" );
				element24->LinkEndChild( element26 );

				char ID[10];
				itoa(j->id, ID, 10);
				TiXmlText * text17 = new TiXmlText( ID );
				element26->LinkEndChild( text17 );

				TiXmlElement * element27 = new TiXmlElement( "TP" );
				element24->LinkEndChild( element27 );

				char TP[10];
				itoa(j->tp, TP, 10);
				TiXmlText * text18 = new TiXmlText( TP );
				element27->LinkEndChild( text18 );

				TiXmlElement * element28 = new TiXmlElement( "NAME" );
				element24->LinkEndChild( element28 );

				TiXmlText * text19 = new TiXmlText( "������" );
				element28->LinkEndChild( text19 );

				TiXmlElement * element29 = new TiXmlElement( "PX" );
				element24->LinkEndChild( element29 );

				char PX[20];
				sprintf(PX, "%0.2f", j->x1_real);
				if (strchr(PX, (int) ',') != NULL)
				{
					*strchr(PX, (int) ',') = '.';
				}
				TiXmlText * text20 = new TiXmlText( PX );
				element29->LinkEndChild( text20 );

				TiXmlElement * element30 = new TiXmlElement( "PY" );
				element24->LinkEndChild( element30 );

				char PY[20];
				sprintf(PY, "%0.2f", j->y1_real);
				if (strchr(PY, (int) ',') != NULL)
				{
					*strchr(PY, (int) ',') = '.';
				}
				TiXmlText * text21 = new TiXmlText( PY );
				element30->LinkEndChild( text21 );

				TiXmlElement * element31 = new TiXmlElement( "PZ" );
				element24->LinkEndChild( element31 );

				char PZ[20];
				sprintf(PZ, "%0.2f", j->z);
				if (strchr(PZ, (int) ',') != NULL)
				{
					*strchr(PZ, (int) ',') = '.';
				}
				TiXmlText * text22 = new TiXmlText( PZ );
				element31->LinkEndChild( text22 );

				TiXmlElement * element32 = new TiXmlElement( "WIDTH" );
				element24->LinkEndChild( element32 );

				char WIDTH[20];
				sprintf(WIDTH, "%0.2f", j->width);
				if (strchr(WIDTH, (int) ',') != NULL)
				{
					*strchr(WIDTH, (int) ',') = '.';
				}
				TiXmlText * text23 = new TiXmlText( WIDTH );
				element32->LinkEndChild( text23 );

				TiXmlElement * element33 = new TiXmlElement( "HEIGHT" );
				element24->LinkEndChild( element33 );

				char HEIGHT[20];
				sprintf(HEIGHT, "%0.2f", j->height);
				if (strchr(HEIGHT, (int) ',') != NULL)
				{
					*strchr(HEIGHT, (int) ',') = '.';
				}
				TiXmlText * text24 = new TiXmlText( HEIGHT );
				element33->LinkEndChild( text24 );

				TiXmlElement * element34 = new TiXmlElement( "LENGTH" );
				element24->LinkEndChild( element34 );

				char LENGTH[20];
				sprintf(LENGTH, "%0.2f", j->length);
				if (strchr(LENGTH, (int) ',') != NULL)
				{
					*strchr(LENGTH, (int) ',') = '.';
				}
				TiXmlText * text25 = new TiXmlText( LENGTH );
				element34->LinkEndChild( text25 );

				TiXmlElement * element35 = new TiXmlElement( "ANGLE" );
				element24->LinkEndChild( element35 );

				char ANGLE[20];
				sprintf(ANGLE, "%0.2f", j->angle);
				if (strchr(ANGLE, (int) ',') != NULL)
				{
					*strchr(ANGLE, (int) ',') = '.';
				}
				TiXmlText * text26 = new TiXmlText( ANGLE );
				element35->LinkEndChild( text26 );

				TiXmlElement * element36 = new TiXmlElement( "SET" );
				element24->LinkEndChild( element36 );

				char SET[10];
				itoa(j->set, SET, 10);
				TiXmlText * text27 = new TiXmlText( SET );
				element36->LinkEndChild( text27 );

				item_counter++;
			}
		}
		TiXmlElement * element37 = new TiXmlElement( "Count" );
		element23->LinkEndChild( element37 );

		char Count[10];
		itoa(item_counter, Count, 10);
		TiXmlText * text28 = new TiXmlText( Count );
		element37->LinkEndChild( text28 );
	}

	TiXmlElement * element38 = new TiXmlElement( "Count" );
	element8->LinkEndChild( element38 );

	char COUNT[10];
	itoa(floor.size(), COUNT, 10);
	TiXmlText * text36 = new TiXmlText( COUNT );
	element38->LinkEndChild( text36 );
	
	doc.SaveFile( filename );
}


//������� ��� ���������� �������� ������ ����� � XML ����
//���������:
//const char* filename -- ��� �����, � ������� ������������ ������
void Render::SavePeople(const char* filename)
{

	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "windows-1251", "" );
	doc.LinkEndChild( decl );
	
	TiXmlElement * element = new TiXmlElement( "BUILDING" );
	doc.LinkEndChild( element );

	TiXmlElement * element2 = new TiXmlElement( "ROOT" );
	element->LinkEndChild( element2 );

	TiXmlElement * element3 = new TiXmlElement( "BUILDING" );
	element2->LinkEndChild( element3 );

	TiXmlElement * element4 = new TiXmlElement( "CLASSNAME" );
	element3->LinkEndChild( element4 );

	TiXmlText * text = new TiXmlText( "TBuilding" );
	element4->LinkEndChild( text );

	TiXmlElement * element5 = new TiXmlElement( "ID" );
	element3->LinkEndChild( element5 );

	TiXmlText * text2 = new TiXmlText( "0" );
	element5->LinkEndChild( text2 );

	TiXmlElement * element6 = new TiXmlElement( "TP" );
	element3->LinkEndChild( element6 );

	TiXmlText * text3 = new TiXmlText( "1" );
	element6->LinkEndChild( text3 );

	TiXmlElement * element7 = new TiXmlElement( "NAME" );
	element3->LinkEndChild( element7 );

	TiXmlText * text4 = new TiXmlText( "������" );
	element7->LinkEndChild( text4 );

	TiXmlElement * element8 = new TiXmlElement( "FLOORLIST" );
	element3->LinkEndChild( element8 );

	/*TiXmlElement * element9 = new TiXmlElement( "ITEM0" );
	element8->LinkEndChild( element9 );

	TiXmlElement * element10 = new TiXmlElement( "CLASSNAME" );
	element9->LinkEndChild( element10 );

	TiXmlText * text5 = new TiXmlText( "TFloor" );
	element10->LinkEndChild( text5 );

	TiXmlElement * element11 = new TiXmlElement( "ID" );
	element9->LinkEndChild( element11 );

	TiXmlText * text6 = new TiXmlText( "1466" );
	element11->LinkEndChild( text6 );

	TiXmlElement * element12 = new TiXmlElement( "TP" );
	element9->LinkEndChild( element12 );

	TiXmlText * text7 = new TiXmlText( "0" );
	element12->LinkEndChild( text7 );

	TiXmlElement * element13 = new TiXmlElement( "NAME" );
	element9->LinkEndChild( element13 );

	TiXmlText * text8 = new TiXmlText( "��������� ����" );
	element13->LinkEndChild( text8 );

	TiXmlElement * element14 = new TiXmlElement( "NUMBER" );
	element9->LinkEndChild( element14 );

	TiXmlText * text9 = new TiXmlText( "0" );
	element14->LinkEndChild( text9 );

	TiXmlElement * element15 = new TiXmlElement( "PEOPLE" );
	element9->LinkEndChild( element15 );

	TiXmlElement * element16 = new TiXmlElement( "Count" );
	element15->LinkEndChild( element16 );

	TiXmlText * text10 = new TiXmlText( "0" );
	element16->LinkEndChild( text10 );*/

	for (int i = 0; i < floor.size(); i++)
	{
		char ITEM[7];
		switch (i)
		{
		case 0: strcpy(ITEM, "ITEM0"); break;
		case 1: strcpy(ITEM, "ITEM1"); break;
		case 2: strcpy(ITEM, "ITEM2"); break;
		case 3: strcpy(ITEM, "ITEM3"); break;
		case 4: strcpy(ITEM, "ITEM4"); break;
		case 5: strcpy(ITEM, "ITEM5"); break;
		case 6: strcpy(ITEM, "ITEM6"); break;
		case 7: strcpy(ITEM, "ITEM7"); break;
		case 8: strcpy(ITEM, "ITEM8"); break;
		case 9: strcpy(ITEM, "ITEM9"); break;
		case 10: strcpy(ITEM, "ITEM10"); break;
		case 11: strcpy(ITEM, "ITEM11"); break;
		}
		TiXmlElement * element17 = new TiXmlElement( ITEM );
		element8->LinkEndChild( element17 );

		TiXmlElement * element18 = new TiXmlElement( "CLASSNAME" );
		element17->LinkEndChild( element18 );

		TiXmlText * text11 = new TiXmlText( "TFloor" );
		element18->LinkEndChild( text11 );

		TiXmlElement * element19 = new TiXmlElement( "ID" );
		element17->LinkEndChild( element19 );

		char IDI[10];
		itoa(floor[i].id, IDI, 10);
		TiXmlText * text12 = new TiXmlText( IDI );
		element19->LinkEndChild( text12 );

		TiXmlElement * element20 = new TiXmlElement( "TP" );
		element17->LinkEndChild( element20 );

		char TPI[10];
		itoa(floor[i].tp, TPI, 10);
		TiXmlText * text13 = new TiXmlText( TPI );
		element20->LinkEndChild( text13 );

		TiXmlElement * element21 = new TiXmlElement( "NAME" );
		element17->LinkEndChild( element21 );

		TiXmlText * text14 = new TiXmlText( floor[i].name.c_str() );
		element21->LinkEndChild( text14 );

		TiXmlElement * element22 = new TiXmlElement( "NUMBER" );
		element17->LinkEndChild( element22 );

		char NUMBERI[10];
		itoa(floor[i].number, NUMBERI, 10);
		TiXmlText * text15 = new TiXmlText( NUMBERI );
		element22->LinkEndChild( text15 );

		TiXmlElement * element23 = new TiXmlElement( "PEOPLE" );
		element17->LinkEndChild( element23 );

		int item_counter = 0;

		for (vector <people>::iterator j = peop.begin(); j < peop.end(); j++)
		{
			if (j->floor == i)
			{
				char EITEM[10] = "ITEM";
				char ITEMX[10];
				itoa(item_counter, ITEMX, 10);
				strcat(EITEM, ITEMX);
				TiXmlElement * element24 = new TiXmlElement( EITEM );
				element23->LinkEndChild( element24 );

				TiXmlElement * element25 = new TiXmlElement( "CLASSNAME" );
				element24->LinkEndChild( element25 );

				TiXmlText * text16 = new TiXmlText( "TMan" );
				element25->LinkEndChild( text16 );

				TiXmlElement * element26 = new TiXmlElement( "ID" );
				element24->LinkEndChild( element26 );

				char ID[10];
				itoa(j->id, ID, 10);
				TiXmlText * text17 = new TiXmlText( ID );
				element26->LinkEndChild( text17 );

				TiXmlElement * element27 = new TiXmlElement( "TP" );
				element24->LinkEndChild( element27 );

				char TP[10];
				itoa(j->tp, TP, 10);
				TiXmlText * text18 = new TiXmlText( TP );
				element27->LinkEndChild( text18 );

//				TiXmlElement * element28 = new TiXmlElement( "NAME" );
//				element24->LinkEndChild( element28 );

//				TiXmlText * text19 = new TiXmlText( "������" );
//				element28->LinkEndChild( text19 );

				TiXmlElement * element29 = new TiXmlElement( "PX" );
				element24->LinkEndChild( element29 );

				char PX[20];
				sprintf(PX, "%0.2f", j->x_real);
				if (strchr(PX, (int) ',') != NULL)
				{
					*strchr(PX, (int) ',') = '.';
				}
				TiXmlText * text20 = new TiXmlText( PX );
				element29->LinkEndChild( text20 );

				TiXmlElement * element30 = new TiXmlElement( "PY" );
				element24->LinkEndChild( element30 );

				char PY[20];
				sprintf(PY, "%0.2f", j->y_real);
				if (strchr(PY, (int) ',') != NULL)
				{
					*strchr(PY, (int) ',') = '.';
				}
				TiXmlText * text21 = new TiXmlText( PY );
				element30->LinkEndChild( text21 );

				TiXmlElement * element31 = new TiXmlElement( "PZ" );
				element24->LinkEndChild( element31 );

				char PZ[20];
				sprintf(PZ, "%0.2f", j->z);
				if (strchr(PZ, (int) ',') != NULL)
				{
					*strchr(PZ, (int) ',') = '.';
				}
				TiXmlText * text22 = new TiXmlText( PZ );
				element31->LinkEndChild( text22 );

				TiXmlElement * element32 = new TiXmlElement( "COLOR" );
				element24->LinkEndChild( element32 );

				char COLOR[10];
				itoa(j->color, COLOR, 10);
				TiXmlText * text23 = new TiXmlText( COLOR );
				element32->LinkEndChild( text23 );

				TiXmlElement * element33 = new TiXmlElement( "MOBYLITY" );
				element24->LinkEndChild( element33 );

				char MOBYLITY[10];
				itoa(j->mobylity, MOBYLITY, 10);
				TiXmlText * text24 = new TiXmlText( MOBYLITY );
				element33->LinkEndChild( text24 );

				TiXmlElement * element34 = new TiXmlElement( "AGE" );
				element24->LinkEndChild( element34 );

				char AGE[10];
				itoa(j->age, AGE, 10);
				TiXmlText * text25 = new TiXmlText( AGE );
				element34->LinkEndChild( text25 );

				TiXmlElement * element35 = new TiXmlElement( "SIZE" );
				element24->LinkEndChild( element35 );

				char SIZE[20];
				sprintf(SIZE, "%0.3f", j->size);
				if (strchr(SIZE, (int) ',') != NULL)
				{
					*strchr(SIZE, (int) ',') = '.';
				}
				TiXmlText * text26 = new TiXmlText( SIZE );
				element35->LinkEndChild( text26 );

				TiXmlElement * element36 = new TiXmlElement( "SEX" );
				element24->LinkEndChild( element36 );

				char SEX[10];
				itoa(j->sex, SEX, 10);
				TiXmlText * text27 = new TiXmlText( SEX );
				element36->LinkEndChild( text27 );

				TiXmlElement * element37 = new TiXmlElement( "EMOSTATE" );
				element24->LinkEndChild( element37 );

				char EMOSTATE[10];
				itoa(j->emostate, EMOSTATE, 10);
				TiXmlText * text28 = new TiXmlText( EMOSTATE );
				element37->LinkEndChild( text28 );

				TiXmlElement * element38 = new TiXmlElement( "EDUCLEVEL" );
				element24->LinkEndChild( element38 );

				char EDUCLEVEL[10];
				itoa(j->educlevel, EDUCLEVEL, 10);
				TiXmlText * text29 = new TiXmlText( EDUCLEVEL );
				element38->LinkEndChild( text29 );

				TiXmlElement * element39 = new TiXmlElement( "ROLE" );
				element24->LinkEndChild( element39 );

				char ROLE[10];
				itoa(j->role, ROLE, 10);
				TiXmlText * text30 = new TiXmlText( ROLE );
				element39->LinkEndChild( text30 );

				TiXmlElement * element40 = new TiXmlElement( "START_ROOM" );
				element24->LinkEndChild( element40 );

				char START_ROOM[10];
				itoa(j->start_room, START_ROOM, 10);
				TiXmlText * text31 = new TiXmlText( START_ROOM );
				element40->LinkEndChild( text31 );

				TiXmlElement * element41 = new TiXmlElement( "START_TIME" );
				element24->LinkEndChild( element41 );

				char START_TIME[10];
				itoa(j->start_time, START_TIME, 10);
				TiXmlText * text32 = new TiXmlText( START_TIME );
				element41->LinkEndChild( text32 );

				TiXmlElement * element42 = new TiXmlElement( "CONTROL" );
				element24->LinkEndChild( element42 );

				char CONTROL[10];
				itoa(j->control, CONTROL, 10);
				TiXmlText * text33 = new TiXmlText( CONTROL );
				element42->LinkEndChild( text33 );

				TiXmlElement * element43 = new TiXmlElement( "EXIT" );
				element24->LinkEndChild( element43 );

				char EXIT[10];
				itoa(j->exit, EXIT, 10);
				TiXmlText * text34 = new TiXmlText( EXIT );
				element43->LinkEndChild( text34 );

				System::String ^ string_doors = "0";

				if (j->door1 != 0)
				{
					string_doors = j->door1.ToString();
					if (j->door2 != 0)
					{
						string_doors += "," + j->door2.ToString();
						if (j->door3 != 0)
						{
							string_doors += "," + j->door3.ToString();
							if (j->door4 != 0)
							{
								string_doors += "," + j->door4.ToString();
								if (j->door5 != 0)
								{
									string_doors += "," + j->door5.ToString();
								}
							}
						}
					}
				}

				using namespace System::Runtime::InteropServices;
				const char* str_doors = (const char*)(void*)Marshal::StringToHGlobalAnsi(string_doors);
				
				TiXmlElement * element109 = new TiXmlElement( "doors" );
				element24->LinkEndChild( element109 );

				TiXmlText * text109 = new TiXmlText( str_doors );
				element109->LinkEndChild( text109 );

				item_counter++;
			}
		}
		TiXmlElement * element44 = new TiXmlElement( "Count" );
		element23->LinkEndChild( element44 );

		char Count[10];
		itoa(item_counter, Count, 10);
		TiXmlText * text35 = new TiXmlText( Count );
		element44->LinkEndChild( text35 );
	}

	TiXmlElement * element38 = new TiXmlElement( "Count" );
	element8->LinkEndChild( element38 );

	char COUNT[10];
	itoa(floor.size(), COUNT, 10);
	TiXmlText * text36 = new TiXmlText( COUNT );
	element38->LinkEndChild( text36 );
	
	doc.SaveFile( filename );
}


//������� ��� ���������� �������� ������ ������ � XML ����
//���������:
//const char* filename -- ��� �����, � ������� ������������ ������
void Render::SaveDoors(const char* filename)
{
	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "windows-1251", "" );
	doc.LinkEndChild( decl );
	
	TiXmlElement * element = new TiXmlElement( "BUILDING" );
	doc.LinkEndChild( element );

	TiXmlElement * element2 = new TiXmlElement( "ROOT" );
	element->LinkEndChild( element2 );

	TiXmlElement * element3 = new TiXmlElement( "BUILDING" );
	element2->LinkEndChild( element3 );

	TiXmlElement * element4 = new TiXmlElement( "CLASSNAME" );
	element3->LinkEndChild( element4 );

	TiXmlText * text = new TiXmlText( "TBuilding" );
	element4->LinkEndChild( text );

	TiXmlElement * element5 = new TiXmlElement( "ID" );
	element3->LinkEndChild( element5 );

	TiXmlText * text2 = new TiXmlText( "0" );
	element5->LinkEndChild( text2 );

	TiXmlElement * element6 = new TiXmlElement( "TP" );
	element3->LinkEndChild( element6 );

	TiXmlText * text3 = new TiXmlText( "1" );
	element6->LinkEndChild( text3 );

	TiXmlElement * element7 = new TiXmlElement( "NAME" );
	element3->LinkEndChild( element7 );

	TiXmlText * text4 = new TiXmlText( "������" );
	element7->LinkEndChild( text4 );

	TiXmlElement * element8 = new TiXmlElement( "FLOORLIST" );
	element3->LinkEndChild( element8 );

	/*TiXmlElement * element9 = new TiXmlElement( "ITEM0" );
	element8->LinkEndChild( element9 );

	TiXmlElement * element10 = new TiXmlElement( "CLASSNAME" );
	element9->LinkEndChild( element10 );

	TiXmlText * text5 = new TiXmlText( "TFloor" );
	element10->LinkEndChild( text5 );

	TiXmlElement * element11 = new TiXmlElement( "ID" );
	element9->LinkEndChild( element11 );

	TiXmlText * text6 = new TiXmlText( "1466" );
	element11->LinkEndChild( text6 );

	TiXmlElement * element12 = new TiXmlElement( "TP" );
	element9->LinkEndChild( element12 );

	TiXmlText * text7 = new TiXmlText( "0" );
	element12->LinkEndChild( text7 );

	TiXmlElement * element13 = new TiXmlElement( "NAME" );
	element9->LinkEndChild( element13 );

	TiXmlText * text8 = new TiXmlText( "��������� ����" );
	element13->LinkEndChild( text8 );

	TiXmlElement * element14 = new TiXmlElement( "NUMBER" );
	element9->LinkEndChild( element14 );

	TiXmlText * text9 = new TiXmlText( "0" );
	element14->LinkEndChild( text9 );

	TiXmlElement * element15 = new TiXmlElement( "PEOPLE" );
	element9->LinkEndChild( element15 );

	TiXmlElement * element16 = new TiXmlElement( "Count" );
	element15->LinkEndChild( element16 );

	TiXmlText * text10 = new TiXmlText( "0" );
	element16->LinkEndChild( text10 );*/

	for (int i = 0; i < floor.size(); i++)
	{
		char ITEM[7];
		switch (i)
		{
		case 0: strcpy(ITEM, "ITEM0"); break;
		case 1: strcpy(ITEM, "ITEM1"); break;
		case 2: strcpy(ITEM, "ITEM2"); break;
		case 3: strcpy(ITEM, "ITEM3"); break;
		case 4: strcpy(ITEM, "ITEM4"); break;
		case 5: strcpy(ITEM, "ITEM5"); break;
		case 6: strcpy(ITEM, "ITEM6"); break;
		case 7: strcpy(ITEM, "ITEM7"); break;
		case 8: strcpy(ITEM, "ITEM8"); break;
		case 9: strcpy(ITEM, "ITEM9"); break;
		case 10: strcpy(ITEM, "ITEM10"); break;
		case 11: strcpy(ITEM, "ITEM11"); break;
		}
		TiXmlElement * element17 = new TiXmlElement( ITEM );
		element8->LinkEndChild( element17 );

		TiXmlElement * element18 = new TiXmlElement( "CLASSNAME" );
		element17->LinkEndChild( element18 );

		TiXmlText * text11 = new TiXmlText( "TFloor" );
		element18->LinkEndChild( text11 );

		TiXmlElement * element19 = new TiXmlElement( "ID" );
		element17->LinkEndChild( element19 );

		char IDI[10];
		itoa(floor[i].id, IDI, 10);
		TiXmlText * text12 = new TiXmlText( IDI );
		element19->LinkEndChild( text12 );

		TiXmlElement * element20 = new TiXmlElement( "TP" );
		element17->LinkEndChild( element20 );

		char TPI[10];
		itoa(floor[i].tp, TPI, 10);
		TiXmlText * text13 = new TiXmlText( TPI );
		element20->LinkEndChild( text13 );

		TiXmlElement * element21 = new TiXmlElement( "NAME" );
		element17->LinkEndChild( element21 );

		TiXmlText * text14 = new TiXmlText( floor[i].name.c_str() );
		element21->LinkEndChild( text14 );

		TiXmlElement * element22 = new TiXmlElement( "NUMBER" );
		element17->LinkEndChild( element22 );

		char NUMBERI[10];
		itoa(floor[i].number, NUMBERI, 10);
		TiXmlText * text15 = new TiXmlText( NUMBERI );
		element22->LinkEndChild( text15 );

		TiXmlElement * element23 = new TiXmlElement( "APERTURELIST" );
		element17->LinkEndChild( element23 );

		int item_counter = 0;

		for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
		{
			if (j->floor == i)
			{
				char EITEM[10] = "ITEM";
				char ITEMX[10];
				itoa(item_counter, ITEMX, 10);
				strcat(EITEM, ITEMX);
				TiXmlElement * element24 = new TiXmlElement( EITEM );
				element23->LinkEndChild( element24 );

				TiXmlElement * element25 = new TiXmlElement( "CLASSNAME" );
				element24->LinkEndChild( element25 );

				TiXmlText * text16 = new TiXmlText( "TAperture" );
				element25->LinkEndChild( text16 );

				TiXmlElement * element26 = new TiXmlElement( "ID" );
				element24->LinkEndChild( element26 );

				char ID[10];
				itoa(j->id, ID, 10);
				TiXmlText * text17 = new TiXmlText( ID );
				element26->LinkEndChild( text17 );

//				TiXmlElement * element27 = new TiXmlElement( "TP" );
//				element24->LinkEndChild( element27 );

//				char TP[10];
//				itoa(j->tp, TP, 10);
//				TiXmlText * text18 = new TiXmlText( TP );
//				element27->LinkEndChild( text18 );

//				TiXmlElement * element28 = new TiXmlElement( "NAME" );
//				element24->LinkEndChild( element28 );

//				TiXmlText * text19 = new TiXmlText( "������" );
//				element28->LinkEndChild( text19 );

				TiXmlElement * element29 = new TiXmlElement( "P1X" );
				element24->LinkEndChild( element29 );

				char P1X[20];
				sprintf(P1X, "%0.2f", j->x1);
				if (strchr(P1X, (int) ',') != NULL)
				{
					*strchr(P1X, (int) ',') = '.';
				}
				TiXmlText * text20 = new TiXmlText( P1X );
				element29->LinkEndChild( text20 );

				TiXmlElement * element30 = new TiXmlElement( "P1Y" );
				element24->LinkEndChild( element30 );

				char P1Y[20];
				sprintf(P1Y, "%0.2f", j->y1);
				if (strchr(P1Y, (int) ',') != NULL)
				{
					*strchr(P1Y, (int) ',') = '.';
				}
				TiXmlText * text21 = new TiXmlText( P1Y );
				element30->LinkEndChild( text21 );

				TiXmlElement * element31 = new TiXmlElement( "P1Z" );
				element24->LinkEndChild( element31 );

				char P1Z[20];
				sprintf(P1Z, "%0.2f", j->z1);
				if (strchr(P1Z, (int) ',') != NULL)
				{
					*strchr(P1Z, (int) ',') = '.';
				}
				TiXmlText * text22 = new TiXmlText( P1Z );
				element31->LinkEndChild( text22 );

				TiXmlElement * element70 = new TiXmlElement( "P2X" );
				element24->LinkEndChild( element70 );

				char P2X[20];
				sprintf(P2X, "%0.2f", j->x2);
				if (strchr(P2X, (int) ',') != NULL)
				{
					*strchr(P2X, (int) ',') = '.';
				}
				TiXmlText * text70 = new TiXmlText( P2X );
				element70->LinkEndChild( text70 );

				TiXmlElement * element71 = new TiXmlElement( "P2Y" );
				element24->LinkEndChild( element71 );

				char P2Y[20];
				sprintf(P2Y, "%0.2f", j->y2);
				if (strchr(P2Y, (int) ',') != NULL)
				{
					*strchr(P2Y, (int) ',') = '.';
				}
				TiXmlText * text71 = new TiXmlText( P2Y );
				element71->LinkEndChild( text71 );

				TiXmlElement * element72 = new TiXmlElement( "P2Z" );
				element24->LinkEndChild( element72 );

				char P2Z[20];
				sprintf(P2Z, "%0.2f", j->z2);
				if (strchr(P2Z, (int) ',') != NULL)
				{
					*strchr(P2Z, (int) ',') = '.';
				}
				TiXmlText * text72 = new TiXmlText( P2Z );
				element72->LinkEndChild( text72 );

				TiXmlElement * element32 = new TiXmlElement( "LOCK" );
				element24->LinkEndChild( element32 );

				char LOCK[10];
				itoa(j->lock, LOCK, 10);
				TiXmlText * text23 = new TiXmlText( LOCK );
				element32->LinkEndChild( text23 );

				TiXmlElement * element33 = new TiXmlElement( "CLOSER" );
				element24->LinkEndChild( element33 );

				char CLOSER[10];
				itoa(j->closer, CLOSER, 10);
				TiXmlText * text24 = new TiXmlText( CLOSER );
				element33->LinkEndChild( text24 );

				TiXmlElement * element34 = new TiXmlElement( "ANTIFIRE" );
				element24->LinkEndChild( element34 );

				char ANTIFIRE[10];
				itoa(j->antifire, ANTIFIRE, 10);
				TiXmlText * text25 = new TiXmlText( ANTIFIRE );
				element34->LinkEndChild( text25 );

				TiXmlElement * element36 = new TiXmlElement( "ANGLE" );
				element24->LinkEndChild( element36 );

				char ANGLE[10];
				itoa(j->angle, ANGLE, 10);
				TiXmlText * text27 = new TiXmlText( ANGLE );
				element36->LinkEndChild( text27 );

				item_counter++;
			}
		}
		TiXmlElement * element44 = new TiXmlElement( "Count" );
		element23->LinkEndChild( element44 );

		char Count[10];
		itoa(item_counter, Count, 10);
		TiXmlText * text35 = new TiXmlText( Count );
		element44->LinkEndChild( text35 );
	}

	TiXmlElement * element38 = new TiXmlElement( "Count" );
	element8->LinkEndChild( element38 );

	char COUNT[10];
	itoa(floor.size(), COUNT, 10);
	TiXmlText * text36 = new TiXmlText( COUNT );
	element38->LinkEndChild( text36 );
	
	doc.SaveFile( filename );
}


//������� ���������������
void Render::Zoom_in()
{
	Scale *= 1.5;
}

void Render::Zoom_out()
{
	Scale /= 1.5;
}


//������� ��������� ������ ����������� ������
void Render::FurnitureModeON()
{
	people_mode = false;
	marking_mode = false;
	for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
	{
		if (i->marked == true)
		{
			i->marked = false;
		}
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
	{
		if (i->marked == true)
		{
			i->marked = false;
		}
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
	{
		if (j->marked == true)
			j->marked = false;
	}
	furniture_mode = true;
	show_selected_area = false;
	Form1::Instance->����������ToolStripMenuItem->Visible = false;
}


//������� ���������� ������ ����������� ������
void Render::FurnitureModeOFF()
{
	furniture_mode = false;
}


//������� ��������� ������ ����������� �����
void Render::PeopleModeON()
{
	furniture_mode = false;
	marking_mode = false;
	for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
	{
		if (i->marked == true)
		{
			i->marked = false;
		}
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
	{
		if (i->marked == true)
		{
			i->marked = false;
		}
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
	{
		if (j->marked == true)
			j->marked = false;
	}
	people_mode = true;
	show_selected_area = false;
	Form1::Instance->����������ToolStripMenuItem->Visible = false;
}


//������� ���������� ������ ����������� �����
void Render::PeopleModeOFF()
{
	people_mode = false;
}


//������� ����������� ����� �� ������ (�� �������� set)
void Render::PeopleOnMebel()
{
	int TotalPeopleNumber = 0;
	for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
	{
		int PeopleLeft = i->set;
//DEMO		if (PeopleLeft + peop.size() > 20)									//DEMO
//DEMO		{																	//DEMO
//DEMO			MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO			break;															//DEMO
//DEMO		}																	//DEMO
//DEMO		else																//DEMO
		{
		if (PeopleLeft > 0)
		{
			if (i->length > i->width)
			{
				int separate = (int) (i->length / 0.4);
				if (separate == 0)
					separate = 1;
				double separate_length = i->length / (separate * 1.);
				for (int g = 0; g < separate && PeopleLeft > 0; g++)
				{
					people field;
					field.x_real = i->x1_real - 0.22;
					field.y_real = (i->y1_real + g * separate_length + i->y1_real + (g + 1) * separate_length) / 2.;
					field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
					field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
					field.floor = i->floor;
					field.marked = false;
					field.object_is_in_selected_area = false;
					field.box_id = i->box_id;
					bool add_new_item = false;
					if (field.box_id != 0)
					{
						vector <TBox>::iterator j;
						for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
						{
							if (j->id == field.box_id)
								break;
						}
						field.z = j->z1;
						field.radius = 0.2;
						if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
						{
							add_new_item = true;
						}
						else
						{
							add_new_item = false;
						}
					}
					if (add_new_item)
					{
						//�������� �� ����������� � ������ ������� ��� ������
						for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
						{
							if (iu->box_id == field.box_id)
								if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
								{
									add_new_item = false;
									break;
								}
						}
						for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
						{
							if (iu->box_id == field.box_id)
								if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
								{
									add_new_item = false;
									break;
								}
						}
					}
					if (add_new_item)
					{
						people_id++;
						field.id = people_id;
						field.tp = 0;
						field.age = 5;
						field.color = 16776960;
						field.control = 0;
						field.educlevel = 0;
						field.emostate = 0;
						field.exit = 0;
						field.mobylity = 1;
						field.role = 0;
						field.sex = 1;
						field.size = 0.125;
						field.start_room = field.box_id;
						field.start_time = 0;
						field.door1 = 0;
						field.door2 = 0;
						field.door3 = 0;
						field.door4 = 0;
						field.door5 = 0;
						
//DEMO						if (peop.size() > 19)												//DEMO
//DEMO						{																	//DEMO
//DEMO							MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO							break;															//DEMO
//DEMO						}																	//DEMO
//DEMO						else																//DEMO
//DEMO						{																	//DEMO
							peop.push_back(field);
//DEMO						}																	//DEMO
						people_counter = 0;
						for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
						{
							if (pe->floor == floor_number)
							{
								people_counter++;
							}
						}
						Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
						PeopleLeft--;
						TotalPeopleNumber++;
					}
				}
				if (PeopleLeft > 0)
				{
					for (int g = 0; g < separate && PeopleLeft > 0; g++)
					{
						people field;
						field.x_real = i->x2_real + 0.22;
						field.y_real = (i->y1_real + g * separate_length + i->y1_real + (g + 1) * separate_length) / 2.;
						field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
						field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
						field.floor = i->floor;
						field.marked = false;
						field.object_is_in_selected_area = false;
						field.box_id = i->box_id;
						bool add_new_item = false;
						if (field.box_id != 0)
						{
							vector <TBox>::iterator j;
							for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
							{
								if (j->id == field.box_id)
									break;
							}
							field.z = j->z1;
							field.radius = 0.2;
							if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
							{
								add_new_item = true;
							}
							else
							{
								add_new_item = false;
							}
						}
						if (add_new_item)
						{
							//�������� �� ����������� � ������ ������� ��� ������
							for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
							{
								if (iu->box_id == field.box_id)
									if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
									{
										add_new_item = false;
										break;
									}
							}
							for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
							{
								if (iu->box_id == field.box_id)
									if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
									{
										add_new_item = false;
										break;
									}
							}
						}
						if (add_new_item)
						{
							people_id++;
							field.id = people_id;
							field.tp = 0;
							field.age = 5;
							field.color = 16776960;
							field.control = 0;
							field.educlevel = 0;
							field.emostate = 0;
							field.exit = 0;
							field.mobylity = 1;
							field.role = 0;
							field.sex = 1;
							field.size = 0.125;
							field.start_room = field.box_id;
							field.start_time = 0;
							field.door1 = 0;
							field.door2 = 0;
							field.door3 = 0;
							field.door4 = 0;
							field.door5 = 0;
							
//DEMO							if (peop.size() > 19)												//DEMO
//DEMO							{																	//DEMO
//DEMO								MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO								break;															//DEMO
//DEMO							}																	//DEMO
//DEMO							else																//DEMO
//DEMO							{																	//DEMO
								peop.push_back(field);
//DEMO							}																	//DEMO
							people_counter = 0;
							for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
							{
								if (pe->floor == floor_number)
								{
									people_counter++;
								}
							}
							Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
							PeopleLeft--;
							TotalPeopleNumber++;
						}
					}
					if (PeopleLeft > 0)
					{
						int separate2 = (int) (i->width / 0.4);
						if (separate2 == 0)
							separate2 = 1;
						double separate_length2 = i->width / (separate2 * 1.);
						for (int g = 0; g < separate2 && PeopleLeft > 0; g++)
						{
							people field;
							field.y_real = i->y1_real - 0.22;
							field.x_real = (i->x1_real + g * separate_length2 + i->x1_real + (g + 1) * separate_length2) / 2.;
							field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
							field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
							field.floor = i->floor;
							field.marked = false;
							field.object_is_in_selected_area = false;
							field.box_id = i->box_id;
							bool add_new_item = false;
							if (field.box_id != 0)
							{
								vector <TBox>::iterator j;
								for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
								{
									if (j->id == field.box_id)
										break;
								}
								field.z = j->z1;
								field.radius = 0.2;
								if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
								{
									add_new_item = true;
								}
								else
								{
									add_new_item = false;
								}
							}
							if (add_new_item)
							{
								//�������� �� ����������� � ������ ������� ��� ������
								for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
								{
									if (iu->box_id == field.box_id)
										if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
										{
											add_new_item = false;
											break;
										}
								}
								for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
								{
									if (iu->box_id == field.box_id)
										if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
										{
											add_new_item = false;
											break;
										}
								}
							}
							if (add_new_item)
							{
								people_id++;
								field.id = people_id;
								field.tp = 0;
								field.age = 5;
								field.color = 16776960;
								field.control = 0;
								field.educlevel = 0;
								field.emostate = 0;
								field.exit = 0;
								field.mobylity = 1;
								field.role = 0;
								field.sex = 1;
								field.size = 0.125;
								field.start_room = field.box_id;
								field.start_time = 0;
								field.door1 = 0;
								field.door2 = 0;
								field.door3 = 0;
								field.door4 = 0;
								field.door5 = 0;
								
//DEMO								if (peop.size() > 19)												//DEMO
//DEMO								{																	//DEMO
//DEMO									MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO									break;															//DEMO
//DEMO								}																	//DEMO
//DEMO								else																//DEMO
//DEMO								{																	//DEMO
									peop.push_back(field);
//DEMO								}																	//DEMO
								people_counter = 0;
								for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
								{
									if (pe->floor == floor_number)
									{
										people_counter++;
									}
								}
								Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
								PeopleLeft--;
								TotalPeopleNumber++;
							}
						}
						if (PeopleLeft > 0)
						{
							for (int g = 0; g < separate2 && PeopleLeft > 0; g++)
							{
								people field;
								field.y_real = i->y2_real + 0.22;
								field.x_real = (i->x1_real + g * separate_length2 + i->x1_real + (g + 1) * separate_length2) / 2.;
								field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
								field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
								field.floor = i->floor;
								field.marked = false;
								field.object_is_in_selected_area = false;
								field.box_id = i->box_id;
								bool add_new_item = false;
								if (field.box_id != 0)
								{
									vector <TBox>::iterator j;
									for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
									{
										if (j->id == field.box_id)
											break;
									}
									field.z = j->z1;
									field.radius = 0.2;
									if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
									{
										add_new_item = true;
									}
									else
									{
										add_new_item = false;
									}
								}
								if (add_new_item)
								{
									//�������� �� ����������� � ������ ������� ��� ������
									for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
									{
										if (iu->box_id == field.box_id)
											if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
											{
												add_new_item = false;
												break;
											}
									}
									for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
									{
										if (iu->box_id == field.box_id)
											if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
											{
												add_new_item = false;
												break;
											}
									}
								}
								if (add_new_item)
								{
									people_id++;
									field.id = people_id;
									field.tp = 0;
									field.age = 5;
									field.color = 16776960;
									field.control = 0;
									field.educlevel = 0;
									field.emostate = 0;
									field.exit = 0;
									field.mobylity = 1;
									field.role = 0;
									field.sex = 1;
									field.size = 0.125;
									field.start_room = field.box_id;
									field.start_time = 0;
									field.door1 = 0;
									field.door2 = 0;
									field.door3 = 0;
									field.door4 = 0;
									field.door5 = 0;
														
//DEMO									if (peop.size() > 19)												//DEMO
//DEMO									{																	//DEMO
//DEMO										MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO										break;															//DEMO
//DEMO									}																	//DEMO
//DEMO									else																//DEMO
//DEMO									{																	//DEMO
										peop.push_back(field);
//DEMO									}																	//DEMO
									people_counter = 0;
									for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
									{
										if (pe->floor == floor_number)
										{
											people_counter++;
										}
									}
									Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
									PeopleLeft--;
									TotalPeopleNumber++;
								}
							}
						}
					}
				}
			}
			else//i->length < i->width
			{
				int separate = (int) (i->width / 0.4);
				if (separate == 0)
					separate = 1;
				double separate_length = i->width / (separate * 1.);
				for (int g = 0; g < separate && PeopleLeft > 0; g++)
				{
					people field;
					field.y_real = i->y1_real - 0.22;
					field.x_real = (i->x1_real + g * separate_length + i->x1_real + (g + 1) * separate_length) / 2.;
					field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
					field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
					field.floor = i->floor;
					field.marked = false;
					field.object_is_in_selected_area = false;
					field.box_id = i->box_id;
					bool add_new_item = false;
					if (field.box_id != 0)
					{
						vector <TBox>::iterator j;
						for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
						{
							if (j->id == field.box_id)
								break;
						}
						field.z = j->z1;
						field.radius = 0.2;
						if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
						{
							add_new_item = true;
						}
						else
						{
							add_new_item = false;
						}
					}
					if (add_new_item)
					{
						//�������� �� ����������� � ������ ������� ��� ������
						for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
						{
							if (iu->box_id == field.box_id)
								if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
								{
									add_new_item = false;
									break;
								}
						}
						for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
						{
							if (iu->box_id == field.box_id)
								if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
								{
									add_new_item = false;
									break;
								}
						}
					}
					if (add_new_item)
					{
						people_id++;
						field.id = people_id;
						field.tp = 0;
						field.age = 5;
						field.color = 16776960;
						field.control = 0;
						field.educlevel = 0;
						field.emostate = 0;
						field.exit = 0;
						field.mobylity = 1;
						field.role = 0;
						field.sex = 1;
						field.size = 0.125;
						field.start_room = field.box_id;
						field.start_time = 0;
						field.door1 = 0;
						field.door2 = 0;
						field.door3 = 0;
						field.door4 = 0;
						field.door5 = 0;
						
//DEMO						if (peop.size() > 19)												//DEMO
//DEMO						{																	//DEMO
//DEMO							MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO							break;															//DEMO
//DEMO						}																	//DEMO
//DEMO						else																//DEMO
//DEMO						{																	//DEMO
							peop.push_back(field);
//DEMO						}																	//DEMO

						people_counter = 0;
						for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
						{
							if (pe->floor == floor_number)
							{
								people_counter++;
							}
						}
						Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
						PeopleLeft--;
						TotalPeopleNumber++;
					}
				}
				if (PeopleLeft > 0)
				{
					for (int g = 0; g < separate && PeopleLeft > 0; g++)
					{
						people field;
						field.y_real = i->y2_real + 0.22;
						field.x_real = (i->x1_real + g * separate_length + i->x1_real + (g + 1) * separate_length) / 2.;
						field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
						field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
						field.floor = i->floor;
						field.marked = false;
						field.object_is_in_selected_area = false;
						field.box_id = i->box_id;
						bool add_new_item = false;
						if (field.box_id != 0)
						{
							vector <TBox>::iterator j;
							for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
							{
								if (j->id == field.box_id)
									break;
							}
							field.z = j->z1;
							field.radius = 0.2;
							if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
							{
								add_new_item = true;
							}
							else
							{
								add_new_item = false;
							}
						}
						if (add_new_item)
						{
							//�������� �� ����������� � ������ ������� ��� ������
							for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
							{
								if (iu->box_id == field.box_id)
									if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
									{
										add_new_item = false;
										break;
									}
							}
							for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
							{
								if (iu->box_id == field.box_id)
									if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
									{
										add_new_item = false;
										break;
									}
							}
						}
						if (add_new_item)
						{
							people_id++;
							field.id = people_id;
							field.tp = 0;
							field.age = 5;
							field.color = 16776960;
							field.control = 0;
							field.educlevel = 0;
							field.emostate = 0;
							field.exit = 0;
							field.mobylity = 1;
							field.role = 0;
							field.sex = 1;
							field.size = 0.125;
							field.start_room = field.box_id;
							field.start_time = 0;
							field.door1 = 0;
							field.door2 = 0;
							field.door3 = 0;
							field.door4 = 0;
							field.door5 = 0;
							
//DEMO							if (peop.size() > 19)												//DEMO
//DEMO							{																	//DEMO
//DEMO								MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO								break;															//DEMO
//DEMO							}																	//DEMO
//DEMO							else																//DEMO
//DEMO							{																	//DEMO
								peop.push_back(field);
//DEMO							}																	//DEMO
							people_counter = 0;
							for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
							{
								if (pe->floor == floor_number)
								{
									people_counter++;
								}
							}
							Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
							PeopleLeft--;
							TotalPeopleNumber++;
						}
					}
					if (PeopleLeft > 0)
					{
						int separate2 = (int) (i->length / 0.4);
						if (separate2 == 0)
							separate2 = 1;
						double separate_length2 = i->length / (separate2 * 1.);
						for (int g = 0; g < separate2 && PeopleLeft > 0; g++)
						{
							people field;
							field.x_real = i->x1_real - 0.22;
							field.y_real = (i->y1_real + g * separate_length2 + i->y1_real + (g + 1) * separate_length2) / 2.;
							field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
							field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
							field.floor = i->floor;
							field.marked = false;
							field.object_is_in_selected_area = false;
							field.box_id = i->box_id;
							bool add_new_item = false;
							if (field.box_id != 0)
							{
								vector <TBox>::iterator j;
								for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
								{
									if (j->id == field.box_id)
										break;
								}
								field.z = j->z1;
								field.radius = 0.2;
								if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
								{
									add_new_item = true;
								}
								else
								{
									add_new_item = false;
								}
							}
							if (add_new_item)
							{
								//�������� �� ����������� � ������ ������� ��� ������
								for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
								{
									if (iu->box_id == field.box_id)
										if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
										{
											add_new_item = false;
											break;
										}
								}
								for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
								{
									if (iu->box_id == field.box_id)
										if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
										{
											add_new_item = false;
											break;
										}
								}
							}
							if (add_new_item)
							{
								people_id++;
								field.id = people_id;
								field.tp = 0;
								field.age = 5;
								field.color = 16776960;
								field.control = 0;
								field.educlevel = 0;
								field.emostate = 0;
								field.exit = 0;
								field.mobylity = 1;
								field.role = 0;
								field.sex = 1;
								field.size = 0.125;
								field.start_room = field.box_id;
								field.start_time = 0;
								field.door1 = 0;
								field.door2 = 0;
								field.door3 = 0;
								field.door4 = 0;
								field.door5 = 0;
								
//DEMO								if (peop.size() > 19)												//DEMO
//DEMO								{																	//DEMO
//DEMO									MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO									break;															//DEMO
//DEMO								}																	//DEMO
//DEMO								else																//DEMO
//DEMO								{																	//DEMO
									peop.push_back(field);
//DEMO								}																	//DEMO
								people_counter = 0;
								for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
								{
									if (pe->floor == floor_number)
									{
										people_counter++;
									}
								}
								Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
								PeopleLeft--;
								TotalPeopleNumber++;
							}
						}
						if (PeopleLeft > 0)
						{
							for (int g = 0; g < separate2 && PeopleLeft > 0; g++)
							{
								people field;
								field.x_real = i->x2_real + 0.22;
								field.y_real = (i->y1_real + g * separate_length2 + i->y1_real + (g + 1) * separate_length2) / 2.;
								field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
								field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
								field.floor = i->floor;
								field.marked = false;
								field.object_is_in_selected_area = false;
								field.box_id = i->box_id;
								bool add_new_item = false;
								if (field.box_id != 0)
								{
									vector <TBox>::iterator j;
									for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
									{
										if (j->id == field.box_id)
											break;
									}
									field.z = j->z1;
									field.radius = 0.2;
									if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
									{
										add_new_item = true;
									}
									else
									{
										add_new_item = false;
									}
								}
								if (add_new_item)
								{
									//�������� �� ����������� � ������ ������� ��� ������
									for (vector <people>::iterator iu = peop.begin(); iu < peop.end(); iu++)
									{
										if (iu->box_id == field.box_id)
											if ((iu->x_real - field.x_real) * (iu->x_real - field.x_real) + (iu->y_real - field.y_real) * (iu->y_real - field.y_real) <= 0.4 * 0.4)
											{
												add_new_item = false;
												break;
											}
									}
									for (vector <furniture>::iterator iu = furn.begin(); iu < furn.end(); iu++)
									{
										if (iu->box_id == field.box_id)
											if (field.x_real + field.radius > iu->x1_real && field.x_real - field.radius < iu->x2_real && field.y_real + field.radius > iu->y1_real && field.y_real - field.radius < iu->y2_real)
											{
												add_new_item = false;
												break;
											}
									}
								}
								if (add_new_item)
								{
									people_id++;
									field.id = people_id;
									field.tp = 0;
									field.age = 5;
									field.color = 16776960;
									field.control = 0;
									field.educlevel = 0;
									field.emostate = 0;
									field.exit = 0;
									field.mobylity = 1;
									field.role = 0;
									field.sex = 1;
									field.size = 0.125;
									field.start_room = field.box_id;
									field.start_time = 0;
									field.door1 = 0;
									field.door2 = 0;
									field.door3 = 0;
									field.door4 = 0;
									field.door5 = 0;
									
//DEMO									if (peop.size() > 19)												//DEMO
//DEMO									{																	//DEMO
//DEMO										MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO										break;															//DEMO
//DEMO									}																	//DEMO
//DEMO									else																//DEMO
//DEMO									{																	//DEMO
										peop.push_back(field);
//DEMO									}																	//DEMO
									people_counter = 0;
									for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
									{
										if (pe->floor == floor_number)
										{
											people_counter++;
										}
									}
									Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
									PeopleLeft--;
									TotalPeopleNumber++;
								}
							}
						}
					}
				}
			}
		}
		}
	}
	MessageBox::Show("��������� " + TotalPeopleNumber + " �������");
}


//������� ����������� ����� ��������� ������� �� ������� ���� �� ��� TRoom != 1
void Render::RandomPeopleOnFloor(int number)
{
//DEMO	if (number + peop.size() > 20)										//DEMO
//DEMO	{																	//DEMO
//DEMO		MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO	}																	//DEMO
//DEMO	else																//DEMO
	{
	Render::SaveAction();
	int BoxNumber = box[floor_number].size();
	double *mas = new double [BoxNumber];
	int counter = -1;
	double CommonArea = 0;
	for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
	{
		counter++;
		double area = fabs(j->x2 - j->x1) * fabs(j->y2 - j->y1);
		if (j->TRoom_Type == 0)
		{
			for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
			{
				if (i->box_id == j->id)
					area -= fabs(i->length * i->width);
			}
			for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
			{
				if (i->box_id == j->id)
					area -= fabs(i->size);
			}
		}
		else
		{
			area = 0.0;
		}
		mas[counter] = area;
		CommonArea += area;
	}
	counter = -1;
	int CurrentNumber = number;
	srand( (unsigned)time( NULL ) );
	for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
	{
		counter++;
		if (j->TRoom_Type == 0)
		{
			for (int ii = 0; ((ii < (int) (mas[counter] / CommonArea * number)) && (CurrentNumber > 0)) || ((int) (mas[counter] / CommonArea * number) == 0 && ii == 0 && (CurrentNumber > 0)); ii++)
			{
				int AttemptNumber = (int) (fabs(j->x2 - j->x1) * fabs(j->y2 - j->y1) / (0.125));
				bool add_new_item = false;
				double Mx, My;
				do
				{
					AttemptNumber--;
					Mx = (rand() % (int) (fabs(j->x2 - j->x1) * 100)) / 100. + j->x1;
					My = (rand() % (int) (fabs(j->y2 - j->y1) * 100)) / 100. + j->y1;
					add_new_item = false;
					if (Mx - 0.2 > j->x1 && Mx + 0.2 < j->x2 && My - 0.2 > j->y1 && My + 0.2 < j->y2)
					{
						add_new_item = true;
					}
					else
					{
						add_new_item = false;
					}
					if (add_new_item)
					{
						//�������� �� ����������� � ������ ������� ��� ������
						for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
						{
							if (i->box_id == j->id)
								if ((i->x_real - Mx) * (i->x_real - Mx) + (i->y_real - My) * (i->y_real - My) <= 0.4 * 0.4)
								{
									add_new_item = false;
									break;
								}
						}
						for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
						{
							if (i->box_id == j->id)
								if (Mx + 0.2 > i->x1_real && Mx - 0.2 < i->x2_real && My + 0.2 > i->y1_real && My - 0.2 < i->y2_real)
								{
									add_new_item = false;
									break;
								}
						}
					}
				}
				while (!add_new_item && AttemptNumber >= 0);
				if (add_new_item)
				{
					people_id++;
					people field;
					field.id = people_id;
					field.box_id = j->id;
					field.radius = 0.2;
					field.z = j->z1;
					field.marked = false;
					field.object_is_in_selected_area = false;
					field.floor = floor_number;
					field.x_real = Mx;
					field.y_real = My;
					field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
					field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
					field.tp = 0;
					field.age = 5;
					field.color = 16776960;
					field.control = 0;
					field.educlevel = 0;
					field.emostate = 0;
					field.exit = 0;
					field.mobylity = 1;
					field.role = 0;
					field.sex = 1;
					field.size = 0.125;
					field.start_room = field.box_id;
					field.start_time = 0;
					field.door1 = 0;
					field.door2 = 0;
					field.door3 = 0;
					field.door4 = 0;
					field.door5 = 0;
					
//DEMO					if (peop.size() > 19)												//DEMO
//DEMO					{																	//DEMO
//DEMO						MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO						break;															//DEMO
//DEMO					}																	//DEMO
//DEMO					else																//DEMO
//DEMO					{																	//DEMO
						peop.push_back(field);
//DEMO					}																	//DEMO
					people_counter = 0;
					for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
					{
						if (pe->floor == floor_number)
						{
							people_counter++;
						}
					}
					Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
					CurrentNumber--;
				}
				else
				{
					break;
				}
			}
		}
	}
	counter = 0;
	do
	{
		counter++;
		for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
		{
			if (j->TRoom_Type == 0)
			{
				for (int ii = 0; ii < 1 && CurrentNumber > 0; ii++)
				{
					int AttemptNumber = (int) (fabs(j->x2 - j->x1) * fabs(j->y2 - j->y1) / (0.125));
					AttemptNumber *= 10;
					bool add_new_item = false;
					double Mx, My;
					do
					{
						AttemptNumber--;
						Mx = (rand() % (int) (fabs(j->x2 - j->x1) * 100)) / 100. + j->x1;
						My = (rand() % (int) (fabs(j->y2 - j->y1) * 100)) / 100. + j->y1;
						add_new_item = false;
						if (Mx - 0.2 > j->x1 && Mx + 0.2 < j->x2 && My - 0.2 > j->y1 && My + 0.2 < j->y2)
						{
							add_new_item = true;
						}
						else
						{
							add_new_item = false;
						}
						if (add_new_item)
						{
							//�������� �� ����������� � ������ ������� ��� ������
							for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
							{
								if (i->box_id == j->id)
									if ((i->x_real - Mx) * (i->x_real - Mx) + (i->y_real - My) * (i->y_real - My) <= 0.4 * 0.4)
									{
										add_new_item = false;
										break;
									}
							}
							for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
							{
								if (i->box_id == j->id)
									if (Mx + 0.2 > i->x1_real && Mx - 0.2 < i->x2_real && My + 0.2 > i->y1_real && My - 0.2 < i->y2_real)
									{
										add_new_item = false;
										break;
									}
							}
						}
					}
					while (!add_new_item && AttemptNumber >= 0);
					if (add_new_item)
					{
						people_id++;
						people field;
						field.id = people_id;
						field.box_id = j->id;
						field.radius = 0.2;
						field.z = j->z1;
						field.marked = false;
						field.object_is_in_selected_area = false;
						field.floor = floor_number;
						field.x_real = Mx;
						field.y_real = My;
						field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
						field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
						field.tp = 0;
						field.age = 5;
						field.color = 16776960;
						field.control = 0;
						field.educlevel = 0;
						field.emostate = 0;
						field.exit = 0;
						field.mobylity = 1;
						field.role = 0;
						field.sex = 1;
						field.size = 0.125;
						field.start_room = field.box_id;
						field.start_time = 0;
						field.door1 = 0;
						field.door2 = 0;
						field.door3 = 0;
						field.door4 = 0;
						field.door5 = 0;
						
//DEMO						if (peop.size() > 19)												//DEMO
//DEMO						{																	//DEMO
//DEMO							MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO							break;															//DEMO
//DEMO						}																	//DEMO
//DEMO						else																//DEMO
//DEMO						{																	//DEMO
							peop.push_back(field);
//DEMO						}																	//DEMO
						people_counter = 0;
						for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
						{
							if (pe->floor == floor_number)
							{
								people_counter++;
							}
						}
						Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
						CurrentNumber--;
					}
					else
					{
						break;
					}
				}
			}
		}
	}
	while (CurrentNumber > 0 && counter < 6);
	CurrentNumber = number - CurrentNumber;
	MessageBox::Show("��������� " + CurrentNumber.ToString() + " �������");
	delete [] mas;
	}
}


//������� ��������� ������ ����������� ID �������� � ��������� ������
void Render::IDModeON()
{
	show_ids = true;
}


//������� ���������� ������ ����������� ID �������� � ��������� ������
void Render::IDModeOFF()
{
	show_ids = false;
}


//������� ��������� ������ ���������
void Render::MarkingModeON()
{
	furniture_mode = false;
	people_mode = false;
	marking_mode = true;
}


//�������, ���������� ��� ����� �������� ������� � ��� ���������� �������
void Render::PropertyChanged(double OldValue)
{
	
}


//������� ���������� ������ ���������
void Render::MarkingModeOFF()
{
	marking_mode = false;
	for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
	{
		if (i->marked == true)
		{
			i->marked = false;
		}
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
	{
		if (i->marked == true)
		{
			i->marked = false;
		}
		if (i->object_is_in_selected_area == true)
		{
			i->object_is_in_selected_area = false;
		}
	}
	for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
	{
		if (j->marked == true)
			j->marked = false;
	}
	Form1::Instance->propertyGrid1->Enabled = false;
	Form1::Instance->propertyGrid1->SelectedObject = NULL;
	Form1::Instance->propertyGrid1->Enabled = true;
	show_selected_area = false;
	Form1::Instance->����������ToolStripMenuItem->Visible = false;
}


//������� ��������� ������ ��������� �������
void Render::SelectingArea()
{
	selecting_mode = true;
	SelectingForGenerating = true;
}


//������� ��� ����������� ���������� � ���������� �������� � ������
void Render::ShowInformation()
{
	int selected_people_counter = 0;
	int selected_furniture_counter = 0;
	for (vector <furniture>::iterator k = furn.begin(); k < furn.end(); k++)
	{
		if (k->object_is_in_selected_area)
		{
			selected_furniture_counter++;
		}
	}
	for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
	{
		if (k->object_is_in_selected_area)
		{
			selected_people_counter++;
		}
	}
	MessageBox::Show("��������:\n\n�������: " + selected_people_counter.ToString() + "\n��������� ������: " + selected_furniture_counter.ToString(), "����������");
}


//������� ��� ���������� ���������� ������������ ��������
void Render::SaveAction()
{
	if (!action1)
	{
		action1 = true;
		peop_saved_1 = peop;
		furn_saved_1 = furn;
		door_saved_1 = door;
		for (vector <people>::iterator i = peop_saved_1.begin(); i < peop_saved_1.end(); i++)
		{
			if (i->marked == true)
			{
				i->marked = false;
			}
			if (i->object_is_in_selected_area == true)
			{
				i->object_is_in_selected_area = false;
			}
		}
		for (vector <furniture>::iterator i = furn_saved_1.begin(); i < furn_saved_1.end(); i++)
		{
			if (i->marked == true)
			{
				i->marked = false;
			}
			if (i->object_is_in_selected_area == true)
			{
				i->object_is_in_selected_area = false;
			}
		}
	}
	else
	{
		if (!action2)
		{
			action2 = true;
			peop_saved_2 = peop_saved_1;
			furn_saved_2 = furn_saved_1;
			door_saved_2 = door_saved_1;
			peop_saved_1 = peop;
			furn_saved_1 = furn;
			door_saved_1 = door;
			for (vector <people>::iterator i = peop_saved_1.begin(); i < peop_saved_1.end(); i++)
			{
				if (i->marked == true)
				{
					i->marked = false;
				}
				if (i->object_is_in_selected_area == true)
				{
					i->object_is_in_selected_area = false;
				}
			}
			for (vector <furniture>::iterator i = furn_saved_1.begin(); i < furn_saved_1.end(); i++)
			{
				if (i->marked == true)
				{
					i->marked = false;
				}
				if (i->object_is_in_selected_area == true)
				{
					i->object_is_in_selected_area = false;
				}
			}
		}
		else
		{
			if (!action3)
			{
				action3 = true;
				peop_saved_3 = peop_saved_2;
				furn_saved_3 = furn_saved_2;
				door_saved_3 = door_saved_2;
				peop_saved_2 = peop_saved_1;
				furn_saved_2 = furn_saved_1;
				door_saved_2 = door_saved_1;
				peop_saved_1 = peop;
				furn_saved_1 = furn;
				door_saved_1 = door;
				for (vector <people>::iterator i = peop_saved_1.begin(); i < peop_saved_1.end(); i++)
				{
					if (i->marked == true)
					{
						i->marked = false;
					}
					if (i->object_is_in_selected_area == true)
					{
						i->object_is_in_selected_area = false;
					}
				}
				for (vector <furniture>::iterator i = furn_saved_1.begin(); i < furn_saved_1.end(); i++)
				{
					if (i->marked == true)
					{
						i->marked = false;
					}
					if (i->object_is_in_selected_area == true)
					{
						i->object_is_in_selected_area = false;
					}
				}
			}
			else
			{
				peop_saved_3 = peop_saved_2;
				furn_saved_3 = furn_saved_2;
				door_saved_3 = door_saved_2;
				peop_saved_2 = peop_saved_1;
				furn_saved_2 = furn_saved_1;
				door_saved_2 = door_saved_1;
				peop_saved_1 = peop;
				furn_saved_1 = furn;
				door_saved_1 = door;
				for (vector <people>::iterator i = peop_saved_1.begin(); i < peop_saved_1.end(); i++)
				{
					if (i->marked == true)
					{
						i->marked = false;
					}
					if (i->object_is_in_selected_area == true)
					{
						i->object_is_in_selected_area = false;
					}
				}
				for (vector <furniture>::iterator i = furn_saved_1.begin(); i < furn_saved_1.end(); i++)
				{
					if (i->marked == true)
					{
						i->marked = false;
					}
					if (i->object_is_in_selected_area == true)
					{
						i->object_is_in_selected_area = false;
					}
				}
			}
		}
	}
}



//������� ��� ������ ���� ��������� ����������� ��������
void Render::CancelAction()
{
	if (action1)
	{
		if (marking_mode)
		{
			Render::MarkingModeOFF();
			Render::MarkingModeON();
		}
		peop = peop_saved_1;
		furn = furn_saved_1;
		door = door_saved_1;
		if (action2)
		{
			peop_saved_1 = peop_saved_2;
			furn_saved_1 = furn_saved_2;
			door_saved_1 = door_saved_2;
			if (action3)
			{
				action3 = false;
				peop_saved_2 = peop_saved_3;
				furn_saved_2 = furn_saved_3;
				door_saved_2 = door_saved_3;
			}
			else
			{
				action2 = false;
			}
		}
		else
		{
			action1 = false;
		}
		people_counter = 0;
		for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
		{
			if (pe->floor == floor_number)
			{
				people_counter++;
			}
		}
		Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
	}
}


//�������, ���������� ��� �������� ���� � ������� ��������.
//���������:
//int ax, int ay -- ���������� �������� ��������� �������
void Render::mouseMove(int ax, int ay)
{
	if (!furniture_mode && !people_mode && !marking_mode && !selecting_mode)
	{
		x = (ax * WinWid_f / WinWid - x_start) / Scale;
		y = (ay * WinHei_f / WinHei - y_start) / Scale;
	}
	if (furniture_mode)
	{
		x_furn = (ax * WinWid_f / WinWid - x_start * Scale) / Scale;
		y_furn = (ay * WinHei_f / WinHei - y_start * Scale) / Scale;
	}
	if (marking_mode && person_selected)
	{
		x_pers = (ax * WinWid_f / WinWid - x_start_person * Scale) / Scale;
		y_pers = (ay * WinHei_f / WinHei - y_start_person * Scale) / Scale;
	}
	if (marking_mode && furn_selected)
	{
		x_mebel = (ax * WinWid_f / WinWid - x_start_mebel * Scale) / Scale;
		y_mebel = (ay * WinHei_f / WinHei - y_start_mebel * Scale) / Scale;
	}
	if (selecting_mode)
	{
		x_select = (ax * WinWid_f / WinWid - x_start * Scale) / Scale;
		y_select = (ay * WinHei_f / WinHei - y_start * Scale) / Scale;
	}
}


//������� �������� ���������� ��������
void Render::DeleteSelectedObject()
{
	Render::DeleteAllFurniture();
	Render::DeleteAllPeople();
	/*for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
	{
		if (i->marked == true)
		{
				peop.erase(i);
				Form1::Instance->propertyGrid1->SelectedObject = NULL;
				break;
		}
	}
	people_counter = 0;
	for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
	{
		if (pe->floor == floor_number)
		{
			people_counter++;
		}
	}
	Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
	for (vector <furniture>::iterator i = furn.begin(); i != furn.end(); i++)
	{
		if (i->marked == true)
		{
				furn.erase(i);
				Form1::Instance->propertyGrid1->SelectedObject = NULL;
				break;
		}
	}*/
}


//������� ��� �������� ���� ���������� ��������� ������
void Render::DeleteAllFurniture()
{
	bool IsThereSelectedFurniture = false;
	for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
	{
		if (i->object_is_in_selected_area)
		{
			IsThereSelectedFurniture = true;
		}
	}
	if (marking_mode && IsThereSelectedFurniture)
	{
		Render::SaveAction();
		int delete_counter = 0;
		int isDeleted = 0;
		for (vector <furniture>::iterator i = furn.begin(); i != furn.end(); i = i + isDeleted)
		{
			if (i->object_is_in_selected_area == true)
			{
				furn.erase(i);
				delete_counter++;
				isDeleted = 0;
				if (furn.empty())
				{
					break;
				}
			}
			else
			{
				isDeleted = 1;
			}
		}
		MessageBox::Show("��������� ������ �������: " + delete_counter.ToString(), "�������� ������");
	}
	else
	{
		for (vector <furniture>::iterator i = furn.begin(); i != furn.end(); i++)
		{
			if (i->marked == true)
			{
					furn.erase(i);
					Form1::Instance->propertyGrid1->SelectedObject = NULL;
					break;
			}
		}
	}
}


//������� ��� �������� ���� ���������� �����
void Render::DeleteAllPeople()
{
	bool IsThereSelectedPeople = false;
	for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
	{
		if (i->object_is_in_selected_area)
		{
			IsThereSelectedPeople = true;
		}
	}
	if (marking_mode && IsThereSelectedPeople)
	{
		Render::SaveAction();
		int delete_counter = 0;
		int isDeleted = 0;
		for (vector <people>::iterator i = peop.begin(); i < peop.end(); i = i + isDeleted)
		{
			if (i->object_is_in_selected_area == true)
			{
				peop.erase(i);
				delete_counter++;
				isDeleted = 0;
				if (peop.empty())
				{
					break;
				}
			}
			else
			{
				isDeleted = 1;
			}
		}
		people_counter = 0;
		for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
		{
			if (pe->floor == floor_number)
			{
				people_counter++;
			}
		}
		Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
		MessageBox::Show("����� �������: " + delete_counter.ToString(), "�������� �����");
	}
	else
	{
		for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
		{
			if (i->marked == true)
			{
					peop.erase(i);
					Form1::Instance->propertyGrid1->SelectedObject = NULL;
					break;
			}
		}
		people_counter = 0;
		for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
		{
			if (pe->floor == floor_number)
			{
				people_counter++;
			}
		}
		Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
	}
}



//������� �������� ������ ����� �� XML �����
//���������:
//const char* filename_people -- ��� �������� ����� � ������
void Render::LoadPeople(const char* filename_people)
{
//	if (InputReady == true)
	{
		action1 = false;
		action2 = false;
		action3 = false;
		show_selected_area = false;
		Form1::Instance->����������ToolStripMenuItem->Visible = false;
		bool CallingMessageBox = false;
		peop.erase(peop.begin(), peop.end());
		people_counter = 0;
		for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
		{
			if (pe->floor == floor_number)
			{
				people_counter++;
			}
		}
		Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
		int FloorNumber;
		bool f = true;
		TiXmlDocument document( filename_people );
		if (document.LoadFile());
		{
		TiXmlElement *BUILDING = document.FirstChildElement("BUILDING");
		TiXmlElement *ITERATOR;
		if (BUILDING)
		{
			TiXmlElement *ROOT = BUILDING->FirstChildElement();
			if (ROOT)
			{
				const char* trol = "";
				do
				{
					ITERATOR = ROOT->FirstChildElement();
					if (!ITERATOR)
					{
						ITERATOR = ROOT->NextSiblingElement();
						if (!ITERATOR)
						{
							TiXmlNode *TEMP = ROOT->Parent();
							TiXmlNode *TEMP2;
							if (!TEMP)
								f = false;
							TEMP2 = TEMP->NextSibling();
							while (!TEMP2)
							{
								TEMP2 = TEMP->Parent();
								if (!TEMP2)
								{
									f = false;
									break;
								}
								TEMP = TEMP2;
								TEMP2 = TEMP2->NextSibling();
							}
							if (f)
								ITERATOR = TEMP2->ToElement();
						}
					}
					if (f)
					{
						ROOT = ITERATOR;
						if (ROOT->GetText() != NULL)
						{
							trol = ROOT->GetText();
							if (strcmp(trol, "TFloor") == 0)
							{
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								FloorNumber = x;
							}
							if (strcmp(trol, "TMan") == 0)
							{
								people field;
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								field.id = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.tp = x;
								ROOT = ROOT->NextSiblingElement();
								const char* str = ROOT->GetText();
								char str2[32];
								double y = 0;
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.x_real = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.y_real = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.z = y;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.color = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.mobylity = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.age = x;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.size = y;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.sex = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.emostate = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.educlevel = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.role = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.start_room = x;
								field.box_id = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.start_time = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.control = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.exit = x;
								field.door1 = 0;
								field.door2 = 0;
								field.door3 = 0;
								field.door4 = 0;
								field.door5 = 0;
								if (ROOT->NextSiblingElement() != NULL)
								{
									ROOT = ROOT->NextSiblingElement();
									char doors_temp[255];
									for (int i = 0; i < 255; i++)
									{
										doors_temp[i] = 0;
									}
									strcpy(doors_temp, ROOT->GetText());
									if (doors_temp[0] != '0')
									{
										int doors_counter = 0;
										int i_doors = 0;
										char doors_temp2[255];
										while (doors_temp[doors_counter] != ',' && doors_counter != strlen(doors_temp))
										{
											doors_temp2[i_doors] = doors_temp[doors_counter];
											doors_counter++;
											i_doors++;
										}
										doors_temp2[i_doors] = 0;
										field.door1 = atoi(doors_temp2);
										if (doors_counter != strlen(doors_temp))
										{
											doors_counter++;
											i_doors = 0;
											while (doors_temp[doors_counter] != ',' && doors_counter != strlen(doors_temp))
											{
												doors_temp2[i_doors] = doors_temp[doors_counter];
												doors_counter++;
												i_doors++;
											}
											doors_temp2[i_doors] = 0;
											field.door2 = atoi(doors_temp2);
											if (doors_counter != strlen(doors_temp))
											{
												doors_counter++;
												i_doors = 0;
												while (doors_temp[doors_counter] != ',' && doors_counter != strlen(doors_temp))
												{
													doors_temp2[i_doors] = doors_temp[doors_counter];
													doors_counter++;
													i_doors++;
												}
												doors_temp2[i_doors] = 0;
												field.door3 = atoi(doors_temp2);
												if (doors_counter != strlen(doors_temp))
												{
													doors_counter++;
													i_doors = 0;
													while (doors_temp[doors_counter] != ',' && doors_counter != strlen(doors_temp))
													{
														doors_temp2[i_doors] = doors_temp[doors_counter];
														doors_counter++;
														i_doors++;
													}
													doors_temp2[i_doors] = 0;
													field.door4 = atoi(doors_temp2);
													if (doors_counter != strlen(doors_temp))
													{
														doors_counter++;
														i_doors = 0;
														while (doors_temp[doors_counter] != ',' && doors_counter != strlen(doors_temp))
														{
															doors_temp2[i_doors] = doors_temp[doors_counter];
															doors_counter++;
															i_doors++;
														}
														doors_temp2[i_doors] = 0;
														field.door5 = atoi(doors_temp2);
													}
												}
											}
										}
									}
								}
								for (int i = 0; i < floor.size(); i++)
								{
									if (floor[i].number == FloorNumber)
									{
										field.floor = i;
									}
								}
								field.x = (field.x_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
								field.y = (field.y_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
								field.radius = 0.2;
								field.marked = false;
								field.object_is_in_selected_area = false;
								//�������� �� ����������� �� �������, ������, �������
								field.box_id = -1;
								bool add_new_item = false;
								for (vector <TBox>::iterator j = box[field.floor].begin(); j < box[field.floor].end(); j++)
								{
									if (field.x_real > j->x1 && field.x_real < j->x2 && field.y_real > j->y1 && field.y_real < j->y2)
									{
										field.box_id = j->id;
										break;
									}
								}
								if (field.box_id != -1)
								{
									vector <TBox>::iterator j;
									for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
									{
										if (j->id == field.box_id)
											break;
									}
									field.z = j->z1;
									if (field.x_real - field.radius > j->x1 && field.x_real + field.radius < j->x2 && field.y_real - field.radius > j->y1 && field.y_real + field.radius < j->y2)
									{
										add_new_item = true;
									}
									else
									{
										add_new_item = false;
									}
								}
								if (add_new_item)
								{
									//�������� �� ����������� � ������ ������� ��� ������
									for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
									{
										if (i->box_id == field.box_id)
											if ((i->x_real - field.x_real) * (i->x_real - field.x_real) + (i->y_real - field.y_real) * (i->y_real - field.y_real) <= 0.4 * 0.4)
											{
												add_new_item = false;
												break;
											}
									}
									for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
									{
										if (i->box_id == field.box_id)
											if (field.x_real + field.radius > i->x1_real && field.x_real - field.radius < i->x2_real && field.y_real + field.radius > i->y1_real && field.y_real - field.radius < i->y2_real)
											{
												add_new_item = false;
												break;
											}
									}
								}
								if (add_new_item)
								{
//DEMO									if (peop.size() > 19)												//DEMO
//DEMO									{																	//DEMO
//DEMO										MessageBox::Show("����������� � 20 �������", "���� ������");	//DEMO
//DEMO										break;															//DEMO
//DEMO									}																	//DEMO
//DEMO									else																//DEMO
//DEMO									{																	//DEMO
										peop.push_back(field);
//DEMO									}																	//DEMO
									people_counter = 0;
									for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
									{
										if (pe->floor == floor_number)
										{
											people_counter++;
										}
									}
									Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
								}
								else
								{
									CallingMessageBox = true;
								}
							}
						}
					}
				}
				while (f);
			}
		}
		}
		int max_people_id = 0;
		for (vector <people>::iterator k = peop.begin(); k < peop.end(); k++)
		{
			if (k->id > max_people_id)
				max_people_id = k->id;
		}
		people_id = max_people_id;
		if (CallingMessageBox)
		{
			MessageBox::Show("��������� ���� �� ����� ������ �� �����������. ��� ���� �������.");
		}
		}
}


//������� �������� ������ ������ �� XML �����
//���������:
//const char* filename_mebel -- ��� �������� ����� � �������
void Render::LoadMebel(const char* filename_mebel)
{
//	if (InputReady == true)
	{
		action1 = false;
		action2 = false;
		action3 = false;
		show_selected_area = false;
		Form1::Instance->����������ToolStripMenuItem->Visible = false;
		bool CallingMessageBox = false;
		furn.erase(furn.begin(), furn.end());
		int FloorNumber;
		bool f = true;
		TiXmlDocument document( filename_mebel );
		if (document.LoadFile());
		{
		TiXmlElement *BUILDING = document.FirstChildElement("BUILDING");
		TiXmlElement *ITERATOR;
		if (BUILDING)
		{
			TiXmlElement *ROOT = BUILDING->FirstChildElement();
			if (ROOT)
			{
				const char* trol = "";
				do
				{
					ITERATOR = ROOT->FirstChildElement();
					if (!ITERATOR)
					{
						ITERATOR = ROOT->NextSiblingElement();
						if (!ITERATOR)
						{
							TiXmlNode *TEMP = ROOT->Parent();
							TiXmlNode *TEMP2;
							if (!TEMP)
								f = false;
							TEMP2 = TEMP->NextSibling();
							while (!TEMP2)
							{
								TEMP2 = TEMP->Parent();
								if (!TEMP2)
								{
									f = false;
									break;
								}
								TEMP = TEMP2;
								TEMP2 = TEMP2->NextSibling();
							}
							if (f)
								ITERATOR = TEMP2->ToElement();
						}
					}
					if (f)
					{
						ROOT = ITERATOR;
						if (ROOT->GetText() != NULL)
						{
							trol = ROOT->GetText();
							if (strcmp(trol, "TFloor") == 0)
							{
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								FloorNumber = x;
							}
							if (strcmp(trol, "TFurniture") == 0)
							{
								furniture field;
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								field.id = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.tp = x;
								ROOT = ROOT->NextSiblingElement();
//								field.name = ROOT->GetText();
								ROOT = ROOT->NextSiblingElement();
								const char* str = ROOT->GetText();
								char str2[32];
								double y = 0;
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.x1_real = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.y1_real = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.z = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.width = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.height = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.length = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.angle = y;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.set = x;
								field.x2_real = field.x1_real + field.width;
								field.y2_real = field.y1_real + field.length;
								for (int i = 0; i < floor.size(); i++)
								{
									if (floor[i].number == FloorNumber)
									{
										field.floor = i;
									}
								}
								vector <TBox>::iterator j;
								for (j = box[field.floor].begin(); j < box[field.floor].end(); j++)
								{
									if (field.x1_real > j->x1 && field.x1_real < j->x2 && field.y1_real > j->y1 && field.y1_real < j->y2)
									{
										field.box_id = j->id;
										break;
									}
								}
								field.x1 = (field.x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
								field.x2 = (field.x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
								field.y1 = (field.y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
								field.y2 = (field.y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
								field.marked = false;
								field.object_is_in_selected_area = false;
								//��������
								bool add_new_item = true;
								/*if (field.x1_real > j->x1 && field.x2_real < j->x2 && field.y1_real > j->y1 && field.y2_real < j->y2)
								{
									add_new_item = true;
								}*/
								/*bool check = false;
								for (int floor_number1 = 0; floor_number1 < floor.size(); floor_number1++)
								{
									for (j = box[floor_number1].begin(); j < box[floor_number1].end(); j++)
									{
										if (field.x1_real > j->x1 && field.x1_real < j->x2 && field.y1_real > j->y1 && field.y1_real < j->y2)
										{
											field.box_id = j->id;
											check = true;
											break;
										}
									}
									if (check)
									{
										if (field.x1_real > j->x1 && field.x2_real < j->x2 && field.y1_real > j->y1 && field.y2_real < j->y2)
										{
											field.floor = floor_number1;
											add_new_item = true;
											break;
										}
									}
								}*/
								/*if (field.box_id != -1)
								{
									vector <TBox>::iterator j;
									for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
									{
										if (j->id == field.box_id)
											break;
									}
									if (field.x2_real > j->x1 && field.x2_real < j->x2 && field.y2_real > j->y1 && field.y2_real < j->y2)
									{
										add_new_item = true;
									}
									else
									{
										if (field.x2_real >= j->x2)
										{
											field.x2_real = j->x2;
											field.x2 = (field.x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);// field.x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box
										}
										if (field.x2_real <= j->x1)
										{
											field.x2_real = j->x1;
											field.x2 = (field.x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
										}
										if (field.y2_real >= j->y2)
										{
											field.y2_real = j->y2;
											field.y2 = (field.y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);// field.y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box
										}
										if (field.y2_real <= j->y1)
										{
											field.y2_real = j->y1;
											field.y2 = (field.y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
										}
										add_new_item = true;
									}
								}*/
								/*else
								{
									for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
									{
										if (field.x2_real > j->x1 && field.x2_real < j->x2 && field.y2_real > j->y1 && field.y2_real < j->y2)
										{
											field.box_id = j->id;
											break;
										}
									}
									if (field.box_id != 0)
									{
										vector <TBox>::iterator j;
										for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
										{
											if (j->id == field.box_id)
												break;
										}
										field.z = j->z1;
										if (field.x1_real > j->x1 && field.x1_real < j->x2 && field.y1_real > j->y1 && field.y1_real < j->y2)
										{
											add_new_item = true;
										}
										else
										{
											if (field.x1_real >= j->x2)
											{
												field.x1_real = j->x2;
												field.x1 = (field.x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);// field.x2 / WinWid_f * (max_x_box - min_x_box) + min_x_box
											}
											if (field.x1_real <= j->x1)
											{
												field.x1_real = j->x1;
												field.x1 = (field.x1_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
											}
											if (field.y1_real >= j->y2)
											{
												field.y1_real = j->y2;
												field.y1 = (field.y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);// field.y2 / WinHei_f * (max_y_box - min_y_box) + min_y_box
											}
											if (field.y1_real <= j->y1)
											{
												field.y1_real = j->y1;
												field.y1 = (field.y1_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);
											}
											add_new_item = true;
										}
									}
								}*/
								if (add_new_item)
								{
									//�������������� ������ (���������)
									/*if (field.x1 > field.x2)
									{
										double temp = field.x2;
										field.x2 = field.x1;
										field.x1 = temp;
										temp = field.x2_real;
										field.x2_real = field.x1_real;
										field.x1_real = temp;
									}
									if (field.y1 > field.y2)
									{
										double temp = field.y2;
										field.y2 = field.y1;
										field.y1 = temp;
										temp = field.y2_real;
										field.y2_real = field.y1_real;
										field.y1_real = temp;
									}*/
									//�������� �� ����������� � ������ �������
									for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
									{
										if (i->box_id == field.box_id)
											if (((field.x1_real <= i->x1_real && field.x2_real >= i->x1_real) || (field.x1_real <= i->x2_real && field.x2_real >= i->x2_real) || (field.x2_real <= i->x2_real && field.x1_real >= i->x1_real)) && ((field.y1_real <= i->y1_real && field.y2_real >= i->y1_real) || (field.y1_real <= i->y2_real && field.y2_real >= i->y2_real) || (field.y2_real <= i->y2_real && field.y1_real >= i->y1_real)))
											{
												add_new_item = false;
												break;
											}
									}
									for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
									{
										if (i->box_id == field.box_id)
											if (i->x_real + i->radius > field.x1_real && i->x_real - i->radius < field.x2_real && i->y_real + i->radius > field.y1_real && i->y_real - i->radius < field.y2_real)
											{
												add_new_item = false;
												break;
											}
									}
								}
								if (add_new_item)
								{
									furn.push_back(field);
								}
								else
								{
									CallingMessageBox = true;
								}
							}
						}
					}
				}
				while (f);
			}
		}
		}
		int max_furniture_id = 0;
		for (vector <furniture>::iterator k = furn.begin(); k < furn.end(); k++)
		{
			if (k->id > max_furniture_id)
				max_furniture_id = k->id;
		}
		furniture_id = max_furniture_id;
		if (CallingMessageBox)
		{
			MessageBox::Show("��������� �������� ������ �� ����� ������ �� �����������. ��� ���� �������.");
		}
		}
}


//������� �������� ������ ������ �� XML �����
//���������:
//const char* filename_doors -- ��� �������� ����� � �������
void Render::LoadDoors(const char* filename_doors)
{
	//	if (InputReady == true)
	{
		action1 = false;
		action2 = false;
		action3 = false;
		show_selected_area = false;
		Form1::Instance->����������ToolStripMenuItem->Visible = false;
		door.erase(door.begin(), door.end());
		int FloorNumber;
		bool f = true;
		TiXmlDocument document( filename_doors );
		if (document.LoadFile());
		{
		TiXmlElement *BUILDING = document.FirstChildElement("BUILDING");
		TiXmlElement *ITERATOR;
		if (BUILDING)
		{
			TiXmlElement *ROOT = BUILDING->FirstChildElement();
			if (ROOT)
			{
				const char* trol = "";
				do
				{
					ITERATOR = ROOT->FirstChildElement();
					if (!ITERATOR)
					{
						ITERATOR = ROOT->NextSiblingElement();
						if (!ITERATOR)
						{
							TiXmlNode *TEMP = ROOT->Parent();
							TiXmlNode *TEMP2;
							if (!TEMP)
								f = false;
							TEMP2 = TEMP->NextSibling();
							while (!TEMP2)
							{
								TEMP2 = TEMP->Parent();
								if (!TEMP2)
								{
									f = false;
									break;
								}
								TEMP = TEMP2;
								TEMP2 = TEMP2->NextSibling();
							}
							if (f)
								ITERATOR = TEMP2->ToElement();
						}
					}
					if (f)
					{
						ROOT = ITERATOR;
						if (ROOT->GetText() != NULL)
						{
							trol = ROOT->GetText();
							if (strcmp(trol, "TFloor") == 0)
							{
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								FloorNumber = x;
							}
							if (strcmp(trol, "TAperture") == 0)
							{
								TDoor field;
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								field.id = x;
								ROOT = ROOT->NextSiblingElement();
								const char* str = ROOT->GetText();
								char str2[32];
								double y = 0;
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.x1 = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.y1 = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.z1 = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.x2 = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.y2 = y;
								ROOT = ROOT->NextSiblingElement();
								str = ROOT->GetText();
								if (strchr(str, (int) '.') != NULL)
								{
									strcpy(str2, str);
									*strchr(str2, (int) '.') = ',';
									y = strtod(str2, NULL);
								}
								else
								{
									y = strtod(str, NULL);
								}
								field.z2 = y;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.lock = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.closer = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.antifire = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.angle = x;
								for (int i = 0; i < floor.size(); i++)
								{
									if (floor[i].number == FloorNumber)
									{
										field.floor = i;
									}
								}
								for (vector <TAperture>::iterator j = aper[field.floor].begin(); j < aper[field.floor].end(); j++)
								{
									if (j->id == field.id)
									{
										field.box1 = j->box1;
										field.box2 = j->box2;
										field.tp = j->tp;
									}
								}
								field.marked = false;
								door.push_back(field);
							}
						}
					}
				}
				while (f);
			}
		}
		}
		}
}


//������� �������� ��������� ������ �� XML �����
void Render::LoadBuild()
{
	action1 = false;
	action2 = false;
	action3 = false;
	colors = 0;
	floor_number = 0;
	x = -WinWid_f / 4;
	y = WinHei_f / 2;
	x_atm = 0.0;
	y_atm = 0.0;
	x_zoom = 0.0;
	y_zoom = 0.0;
	x_furn = 0.0;
	y_furn = 0.0;
	x_pers = 0.0;
	y_pers = 0.0;
	x_start = 0.0, y_start = 0.0;
	x_start_person = 0.0, y_start_person = 0.0;
	dx = -WinWid_f / 4, dy = WinHei_f / 2;
	person_selected = false;
	furn_selected = false;
	min_x_box = 9999.0;
	max_x_box = -9999.0;
	min_y_box = 9999.0;
	max_y_box = -9999.0;
	furniture_id = 0;
	people_id = 0;
	it = -1;
	marking_mode = false;
	Form1::Instance->MarkingModeButton->Checked = false;
	furn.erase(furn.begin(), furn.end());
	peop.erase(peop.begin(), peop.end());
	people_counter = 0;
	for (vector <people>::iterator pe = peop.begin(); pe < peop.end(); pe++)
	{
		if (pe->floor == floor_number)
		{
			people_counter++;
		}
	}
	Form1::Instance->label4->Text = "����� �� �����: " + people_counter.ToString();
	floor.erase(floor.begin(), floor.end());
	box.erase(box.begin(), box.end());
	aper.erase(aper.begin(), aper.end());
	door.erase(door.begin(), door.end());
	flight.erase(flight.begin(), flight.end());
	plat.erase(plat.begin(), plat.end());
	port.erase(port.begin(), port.end());
	show_selected_area = false;
	Form1::Instance->����������ToolStripMenuItem->Visible = false;
	bool TPorta1 = false, TPorta2 = false;
	int TPortai = -1;
	Scale = 1.0;
	bool f = true;
	bool AllCool = true;
	setlocale(LC_ALL, "");
	TiXmlDocument document( Render::filename_build );
	if (document.LoadFile());
	{
	TiXmlElement *BUILDING = document.FirstChildElement("BUILDING");
	TiXmlElement *ITERATOR;
	if (BUILDING)
	{
		TiXmlElement *ROOT = BUILDING->FirstChildElement();
		if (ROOT)
		{
			const char* trol = "";
			do
			{
				ITERATOR = ROOT->FirstChildElement();
				if (!ITERATOR)
				{
					ITERATOR = ROOT->NextSiblingElement();
					if (!ITERATOR)
					{
						TiXmlNode *TEMP = ROOT->Parent();
						TiXmlNode *TEMP2;
						if (!TEMP)
							f = false;
						TEMP2 = TEMP->NextSibling();
						while (!TEMP2)
						{
							TEMP2 = TEMP->Parent();
							if (!TEMP2)
							{
								f = false;
								break;
							}
							TEMP = TEMP2;
							TEMP2 = TEMP2->NextSibling();
						}
						if (f)
							ITERATOR = TEMP2->ToElement();
					}
				}
				if (f)
				{
					ROOT = ITERATOR;
					if (ROOT->GetText() != NULL)
					{
						trol = ROOT->GetText();
						if (strcmp(trol, "TFloor") == 0)
						{
							vector <TBox> tr1;
							vector <TAperture> tr2;
							box.push_back(tr1);
							aper.push_back(tr2);
							it++;
							TFloor field;
							ROOT = ROOT->NextSiblingElement();
							int x = atoi(ROOT->GetText());
							field.id = x;
							ROOT = ROOT->NextSiblingElement();
							x = atoi(ROOT->GetText());
							field.tp = x;
							ROOT = ROOT->NextSiblingElement();
							const char * TEMP = ROOT->GetText();
							string str( TEMP );
							field.name = TEMP;
							ROOT = ROOT->NextSiblingElement();
							x = atoi(ROOT->GetText());
							field.number = x;
							floor.push_back(field);
						}
						if (strcmp(trol, "TRoom") == 0)
						{
							ROOT = ROOT->NextSiblingElement();
							ROOT = ROOT->NextSiblingElement();
							int x = atoi(ROOT->GetText());
							TRoom_Type = x;
						}
						if (strcmp(trol, "TBox") == 0)
						{
							TBox field;
							for (int i = 0; i < 9; i++)
							{
								if (ROOT->NextSiblingElement() != NULL)
								{
									ROOT = ROOT->NextSiblingElement();
									if (strcmp(ROOT->Value(), "ID") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.id = x;
										if (TPorta1)
										{
											port[TPortai].id1 = x;
											TPorta1 = false;
											TPorta2 = true;
										}
										if (TPorta2)
										{
											port[TPortai].id2 = x;
											TPorta2 = false;
										}
									}
									if (strcmp(ROOT->Value(), "TP") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.tp = x;
									}
									if (strcmp(ROOT->Value(), "NAME") == 0)
									{
										field.name = ROOT->GetText();
									}
									if (strcmp(ROOT->Value(), "P1X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z1 = y;
									}
									if (strcmp(ROOT->Value(), "P2X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z2 = y;
									}
								}
								else
								{
									AllCool = false;
									break;
								}
							}
							field.TRoom_Type = TRoom_Type;
							box[it].push_back(field);
						}
						if (strcmp(trol, "TPorta") == 0)
						{
							TPorta field2;
							ROOT = ROOT->NextSiblingElement();
							int x = atoi(ROOT->GetText());
							field2.id = x;
							ROOT = ROOT->NextSiblingElement();
							x = atoi(ROOT->GetText());
							field2.tp = x;
							ROOT = ROOT->NextSiblingElement();
							field2.name = ROOT->GetText();
							port.push_back(field2);
							TPorta1 = true;
							TPortai++;
						}
						if (strcmp(trol, "TAperture") == 0)
						{
							TAperture field;
							for (int i = 0; i < 11; i++)
							{
								if (ROOT->NextSiblingElement() != NULL)
								{
									ROOT = ROOT->NextSiblingElement();
									if (strcmp(ROOT->Value(), "ID") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.id = x;
										if (TPorta1)
										{
											port[TPortai].id1 = x;
											TPorta1 = false;
											TPorta2 = true;
										}
										if (TPorta2)
										{
											port[TPortai].id2 = x;
											TPorta2 = false;
										}
									}
									if (strcmp(ROOT->Value(), "TP") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.tp = x;
									}
									if (strcmp(ROOT->Value(), "NAME") == 0)
									{
										field.name = ROOT->GetText();
									}
									if (strcmp(ROOT->Value(), "BOX1") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.box1 = x;
									}
									if (strcmp(ROOT->Value(), "BOX2") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.box2 = x;
									}
									if (strcmp(ROOT->Value(), "P1X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z1 = y;
									}
									if (strcmp(ROOT->Value(), "P2X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z2 = y;
									}
								}
								else
								{
									AllCool = false;
									break;
								}
							}
							aper[it].push_back(field);
						}
						if (strcmp(trol, "TPlatform") == 0)
						{
							TPlatform field;
							for (int i = 0; i < 9; i++)
							{
								if (ROOT->NextSiblingElement() != NULL)
								{
									ROOT = ROOT->NextSiblingElement();
									if (strcmp(ROOT->Value(), "ID") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.id = x;
										if (TPorta1)
										{
											port[TPortai].id1 = x;
											TPorta1 = false;
											TPorta2 = true;
										}
										if (TPorta2)
										{
											port[TPortai].id2 = x;
											TPorta2 = false;
										}
									}
									if (strcmp(ROOT->Value(), "TP") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.tp = x;
									}
									if (strcmp(ROOT->Value(), "NAME") == 0)
									{
										field.name = ROOT->GetText();
									}
									if (strcmp(ROOT->Value(), "P1X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z1 = y;
									}
									if (strcmp(ROOT->Value(), "P2X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z2 = y;
									}
								}
								else
								{
									AllCool = false;
									break;
								}
							}
							plat.push_back(field);
						}
						if (strcmp(trol, "TFlight") == 0)
						{
							TFlight field;
							for (int i = 0; i < 16; i++)
							{
								if (ROOT->NextSiblingElement() != NULL)
								{
									ROOT = ROOT->NextSiblingElement();
									if (strcmp(ROOT->Value(), "ID") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.id = x;
										if (TPorta1)
										{
											port[TPortai].id1 = x;
											TPorta1 = false;
											TPorta2 = true;
										}
										if (TPorta2)
										{
											port[TPortai].id2 = x;
											TPorta2 = false;
										}
									}
									if (strcmp(ROOT->Value(), "TP") == 0)
									{
										int x = atoi(ROOT->GetText());
										field.tp = x;
									}
									if (strcmp(ROOT->Value(), "NAME") == 0)
									{
										field.name = ROOT->GetText();
									}
									if (strcmp(ROOT->Value(), "P1X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y1 = y;
									}
									if (strcmp(ROOT->Value(), "P1Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z1 = y;
									}
									if (strcmp(ROOT->Value(), "P2X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y2 = y;
									}
									if (strcmp(ROOT->Value(), "P2Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z2 = y;
									}
									if (strcmp(ROOT->Value(), "P3X") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.x3 = y;
									}
									if (strcmp(ROOT->Value(), "P3Y") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.y3 = y;
									}
									if (strcmp(ROOT->Value(), "P3Z") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.z3 = y;
									}
									if (strcmp(ROOT->Value(), "L") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.l = y;
									}
									if (strcmp(ROOT->Value(), "H") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.h = y;
									}
									if (strcmp(ROOT->Value(), "W") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.w = y;
									}
									if (strcmp(ROOT->Value(), "ANGLE") == 0)
									{
										const char* str = ROOT->GetText();
										char str2[32];
										double y = 0;
										if (strchr(str, (int) '.') != NULL)
										{
											strcpy(str2, str);
											*strchr(str2, (int) '.') = ',';
											y = strtod(str2, NULL);
										}
										else
										{
											y = strtod(str, NULL);
										}
										field.angle = y;
									}
								}
								else
								{
									AllCool = false;
									break;
								}
							}
							field.floor1 = -1;
							field.floor2 = -1;
							flight.push_back(field);
						}
					}
				}
			}
			while (f);
			if (!AllCool)
			{
				MessageBox::Show("� ����������� ����� ������������ ������", "Error");
			}
			//������� �����!
			box[it].pop_back();
			
			for (int i = 0; i <= it; i++)
			{
				for (vector <TBox>::iterator j = box[i].begin(); j < box[i].end(); j++)
				{
					if (j->x1 < min_x_box)
						min_x_box = j->x1;
					if (j->x2 < min_x_box)
						min_x_box = j->x2;
					if (j->y1 < min_y_box)
						min_y_box = j->y1;
					if (j->y2 < min_y_box)
						min_y_box = j->y2;
					if (j->x1 > max_x_box)
						max_x_box = j->x1;
					if (j->x2 > max_x_box)
						max_x_box = j->x2;
					if (j->y1 > max_y_box)
						max_y_box = j->y1;
					if (j->y2 > max_y_box)
						max_y_box = j->y2;
				}
			}
			min_x_box2 = min_x_box;
			min_y_box2 = min_y_box;
			max_x_box2 = max_x_box;
			max_y_box2 = max_y_box;
			if (min_x_box < min_y_box)
			{
				min_y_box = min_x_box;
			}
			else
			{
				min_x_box = min_y_box;
			}
			if (max_x_box < max_y_box)
			{
				max_x_box = max_y_box;
			}
			else
			{
				max_y_box = max_x_box;
			}
			for (int i = 0; i < floor.size() - 1; i++)
			{
				int imin = i;
				for (int j = i + 1; j < floor.size(); j++)
					if (floor[j].number < floor[imin].number)
					{
						imin = j;
					}
				if (imin != i)
				{
					TFloor temp = floor[imin];
					floor[imin] = floor[i];
					floor[i] = temp;
					vector <TBox> temp2 = box[imin];
					box[imin] = box[i];
					box[i] = temp2;
					vector <TAperture> temp3 = aper[imin];
					aper[imin] = aper[i];
					aper[i] = temp3;
				}
			}
			for (int i = 0; i < floor.size(); i++)
			{
				if (floor[i].number == 1)
				{
					floor_number = i;
					break;
				}
			}
			for (vector <TPlatform>::iterator j = plat.begin(); j < plat.end(); j++)
			{
					for (int i = 0; i <= it; i++)
					{
						for (vector <TBox>::iterator k = box[i].begin(); k < box[i].end(); k++)
						{
							if (fabs(j->z1 - k->z1) < 0.00001 || fabs(j->z1 - k->z2) < 0.00001 || fabs(j->z2 - k->z2) < 0.00001 || fabs(j->z2 - k->z1) < 0.00001)
							{
								j->floor = i;
							}
						}
					}
			}
			for (vector <TFlight>::iterator j = flight.begin(); j < flight.end(); j++)
			{
					for (int i = 0; i <= it; i++)
					{
						for (vector <TBox>::iterator k = box[i].begin(); k < box[i].end(); k++)
						{
							if (fabs(j->z1 - k->z1) < 0.00001 || fabs(j->z1 - k->z2) < 0.00001 || fabs(j->z3 - k->z2) < 0.00001 || fabs(j->z3 - k->z1) < 0.00001)
							{
								if (j->floor1 == -1)
									j->floor1 = i;
								else
									if (j->floor1 != i)
										j->floor2 = i;
							}
						}
					}
			}
			for (int floor_number1 = 0; floor_number1 < floor.size(); floor_number1++)
			{
				for (vector <TAperture>::iterator j = aper[floor_number1].begin(); j <aper[floor_number1].end(); j++)
				{
					if (j->x2 < j->x1)
					{
						double temp = j->x1;
						j->x1 = j->x2;
						j->x2 = temp;
					}
					if (j->y2 < j->y1)
					{
						double temp = j->y1;
						j->y1 = j->y2;
						j->y2 = temp;
					}
				}
			}
			for (int floor_number1 = 0; floor_number1 < floor.size(); floor_number1++)
			{
				for (vector <TAperture>::iterator j = aper[floor_number1].begin(); j <aper[floor_number1].end(); j++)
				{
					if (j->tp == 0 || j->tp == 3)
					{
						TDoor field2;
						field2.id = j->id;
						field2.tp = j->tp;
						field2.x1 = j->x1;
						field2.y1 = j->y1;
						field2.z1 = j->z1;
						field2.x2 = j->x2;
						field2.y2 = j->y2;
						field2.z2 = j->z2;
						field2.marked = false;
						field2.floor = floor_number1;
						field2.box1 = j->box1;
						field2.box2 = j->box2;
						field2.lock = 0;
						field2.closer = 0;
						field2.antifire = 0;
						field2.angle = 0;
						door.push_back(field2);
					}
				}
			}
			/*cout << "APERTURES:" << endl;
			for (int i = 0; i <= it; i++)
			{
				for (vector <TAperture>::iterator j = aper[i].begin(); j <aper[i].end(); j++)
				{
					cout << "TAPERTURE#" << i << ": " << j->id << " " << j->tp << " " << j->name
						<< " " << j->x1 << " " << j->y1 << " " << j->z1 << " " << j->x2
						<< " " << j->y2 << " " << j->z2 << " " << j->box1 << " " << j->box2 << endl;
				}
			}*/
		}
	}
	string sss = "��������� ����";
	int minus = 0;
	if (render->floor[0].name == sss)
		minus = 1;
	if (render->floor[0].number == 0)
		minus = 1;
	Form1::Instance->����1ToolStripMenuItem->Visible = false;
	Form1::Instance->����2ToolStripMenuItem->Visible = false;
	Form1::Instance->����3ToolStripMenuItem->Visible = false;
	Form1::Instance->����4ToolStripMenuItem->Visible = false;
	Form1::Instance->����5ToolStripMenuItem->Visible = false;
	Form1::Instance->����6ToolStripMenuItem->Visible = false;
	Form1::Instance->����7ToolStripMenuItem->Visible = false;
	Form1::Instance->����8ToolStripMenuItem->Visible = false;
	Form1::Instance->����9ToolStripMenuItem->Visible = false;
	Form1::Instance->����10ToolStripMenuItem->Visible = false;
	Form1::Instance->����11ToolStripMenuItem->Visible = false;
	Form1::Instance->toolStripLabel1->Text = "���� 1";
	switch(render->floor.size() - minus)
	{
	case 1:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
		}
		break;
	case 2:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
		}
		break;
	case 3:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
		}
		break;
	case 4:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
		}
		break;
	case 5:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
			Form1::Instance->����5ToolStripMenuItem->Visible = true;
		}
		break;
	case 6:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
			Form1::Instance->����5ToolStripMenuItem->Visible = true;
			Form1::Instance->����6ToolStripMenuItem->Visible = true;
		}
		break;
	case 7:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
			Form1::Instance->����5ToolStripMenuItem->Visible = true;
			Form1::Instance->����6ToolStripMenuItem->Visible = true;
			Form1::Instance->����7ToolStripMenuItem->Visible = true;
		}
		break;
	case 8:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
			Form1::Instance->����5ToolStripMenuItem->Visible = true;
			Form1::Instance->����6ToolStripMenuItem->Visible = true;
			Form1::Instance->����7ToolStripMenuItem->Visible = true;
			Form1::Instance->����8ToolStripMenuItem->Visible = true;
		}
		break;
	case 9:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
			Form1::Instance->����5ToolStripMenuItem->Visible = true;
			Form1::Instance->����6ToolStripMenuItem->Visible = true;
			Form1::Instance->����7ToolStripMenuItem->Visible = true;
			Form1::Instance->����8ToolStripMenuItem->Visible = true;
			Form1::Instance->����9ToolStripMenuItem->Visible = true;
		}
		break;
	case 10:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
			Form1::Instance->����5ToolStripMenuItem->Visible = true;
			Form1::Instance->����6ToolStripMenuItem->Visible = true;
			Form1::Instance->����7ToolStripMenuItem->Visible = true;
			Form1::Instance->����8ToolStripMenuItem->Visible = true;
			Form1::Instance->����9ToolStripMenuItem->Visible = true;
			Form1::Instance->����10ToolStripMenuItem->Visible = true;
		}
		break;
	case 11:
		{
			Form1::Instance->����1ToolStripMenuItem->Visible = true;
			Form1::Instance->����2ToolStripMenuItem->Visible = true;
			Form1::Instance->����3ToolStripMenuItem->Visible = true;
			Form1::Instance->����4ToolStripMenuItem->Visible = true;
			Form1::Instance->����5ToolStripMenuItem->Visible = true;
			Form1::Instance->����6ToolStripMenuItem->Visible = true;
			Form1::Instance->����7ToolStripMenuItem->Visible = true;
			Form1::Instance->����8ToolStripMenuItem->Visible = true;
			Form1::Instance->����9ToolStripMenuItem->Visible = true;
			Form1::Instance->����10ToolStripMenuItem->Visible = true;
			Form1::Instance->����11ToolStripMenuItem->Visible = true;
		}
		break;
	}
	}
}


//������� ����������� �������� ��������� ������
void Render::Load()
{
//	if (InputReady == true)
	{
		action1 = false;
		action2 = false;
		action3 = false;
		peop_copy.clear();
		furn_copy.clear();
		GridEnabled = false;
		bool TPorta1 = false, TPorta2 = false;
		int TPortai = -1;
		it = -1;
		BuildReady = true;
		Scale = 1.0;
		bool f = true;
		bool AllCool = true;
		setlocale(LC_ALL, "");
		TiXmlDocument document( filename_first );
		if (document.LoadFile());
		{
		TiXmlElement *BUILDING = document.FirstChildElement("BUILDING");
		TiXmlElement *ITERATOR;
		if (BUILDING)
		{
			TiXmlElement *ROOT = BUILDING->FirstChildElement();
			if (ROOT)
			{
				const char* trol = "";
				do
				{
					ITERATOR = ROOT->FirstChildElement();
					if (!ITERATOR)
					{
						ITERATOR = ROOT->NextSiblingElement();
						if (!ITERATOR)
						{
							TiXmlNode *TEMP = ROOT->Parent();
							TiXmlNode *TEMP2;
							if (!TEMP)
								f = false;
							TEMP2 = TEMP->NextSibling();
							while (!TEMP2)
							{
								TEMP2 = TEMP->Parent();
								if (!TEMP2)
								{
									f = false;
									break;
								}
								TEMP = TEMP2;
								TEMP2 = TEMP2->NextSibling();
							}
							if (f)
								ITERATOR = TEMP2->ToElement();
						}
					}
					if (f)
					{
						ROOT = ITERATOR;
						if (ROOT->GetText() != NULL)
						{
							trol = ROOT->GetText();
							if (strcmp(trol, "TFloor") == 0)
							{
								vector <TBox> tr1;
								vector <TAperture> tr2;
								box.push_back(tr1);
								aper.push_back(tr2);
								it++;
								TFloor field;
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								field.id = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.tp = x;
								ROOT = ROOT->NextSiblingElement();
								const char * TEMP = ROOT->GetText();
								string str( TEMP );
								field.name = TEMP;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field.number = x;
								floor.push_back(field);
							}
							if (strcmp(trol, "TRoom") == 0)
							{
								ROOT = ROOT->NextSiblingElement();
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								TRoom_Type = x;
							}
							if (strcmp(trol, "TBox") == 0)
							{
								TBox field;
								for (int i = 0; i < 9; i++)
								{
									if (ROOT->NextSiblingElement() != NULL)
									{
										ROOT = ROOT->NextSiblingElement();
										if (strcmp(ROOT->Value(), "ID") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.id = x;
											if (TPorta1)
											{
												port[TPortai].id1 = x;
												TPorta1 = false;
												TPorta2 = true;
											}
											if (TPorta2)
											{
												port[TPortai].id2 = x;
												TPorta2 = false;
											}
										}
										if (strcmp(ROOT->Value(), "TP") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.tp = x;
										}
										if (strcmp(ROOT->Value(), "NAME") == 0)
										{
											field.name = ROOT->GetText();
										}
										if (strcmp(ROOT->Value(), "P1X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z1 = y;
										}
										if (strcmp(ROOT->Value(), "P2X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z2 = y;
										}
									}
									else
									{
										AllCool = false;
										break;
									}
								}
								field.TRoom_Type = TRoom_Type;
								box[it].push_back(field);
							}
							if (strcmp(trol, "TPorta") == 0)
							{
								TPorta field2;
								ROOT = ROOT->NextSiblingElement();
								int x = atoi(ROOT->GetText());
								field2.id = x;
								ROOT = ROOT->NextSiblingElement();
								x = atoi(ROOT->GetText());
								field2.tp = x;
								ROOT = ROOT->NextSiblingElement();
								field2.name = ROOT->GetText();
								port.push_back(field2);
								TPorta1 = true;
								TPortai++;
							}
							if (strcmp(trol, "TAperture") == 0)
							{
								TAperture field;
								for (int i = 0; i < 11; i++)
								{
									if (ROOT->NextSiblingElement() != NULL)
									{
										ROOT = ROOT->NextSiblingElement();
										if (strcmp(ROOT->Value(), "ID") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.id = x;
											if (TPorta1)
											{
												port[TPortai].id1 = x;
												TPorta1 = false;
												TPorta2 = true;
											}
											if (TPorta2)
											{
												port[TPortai].id2 = x;
												TPorta2 = false;
											}
										}
										if (strcmp(ROOT->Value(), "TP") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.tp = x;
										}
										if (strcmp(ROOT->Value(), "NAME") == 0)
										{
											field.name = ROOT->GetText();
										}
										if (strcmp(ROOT->Value(), "BOX1") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.box1 = x;
										}
										if (strcmp(ROOT->Value(), "BOX2") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.box2 = x;
										}
										if (strcmp(ROOT->Value(), "P1X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z1 = y;
										}
										if (strcmp(ROOT->Value(), "P2X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z2 = y;
										}
									}
									else
									{
										AllCool = false;
										break;
									}
								}
								aper[it].push_back(field);
							}
							if (strcmp(trol, "TPlatform") == 0)
							{
								TPlatform field;
								for (int i = 0; i < 9; i++)
								{
									if (ROOT->NextSiblingElement() != NULL)
									{
										ROOT = ROOT->NextSiblingElement();
										if (strcmp(ROOT->Value(), "ID") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.id = x;
											if (TPorta1)
											{
												port[TPortai].id1 = x;
												TPorta1 = false;
												TPorta2 = true;
											}
											if (TPorta2)
											{
												port[TPortai].id2 = x;
												TPorta2 = false;
											}
										}
										if (strcmp(ROOT->Value(), "TP") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.tp = x;
										}
										if (strcmp(ROOT->Value(), "NAME") == 0)
										{
											field.name = ROOT->GetText();
										}
										if (strcmp(ROOT->Value(), "P1X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z1 = y;
										}
										if (strcmp(ROOT->Value(), "P2X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z2 = y;
										}
									}
									else
									{
										AllCool = false;
										break;
									}
								}
								plat.push_back(field);
							}
							if (strcmp(trol, "TFlight") == 0)
							{
								TFlight field;
								for (int i = 0; i < 16; i++)
								{
									if (ROOT->NextSiblingElement() != NULL)
									{
										ROOT = ROOT->NextSiblingElement();
										if (strcmp(ROOT->Value(), "ID") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.id = x;
											if (TPorta1)
											{
												port[TPortai].id1 = x;
												TPorta1 = false;
												TPorta2 = true;
											}
											if (TPorta2)
											{
												port[TPortai].id2 = x;
												TPorta2 = false;
											}
										}
										if (strcmp(ROOT->Value(), "TP") == 0)
										{
											int x = atoi(ROOT->GetText());
											field.tp = x;
										}
										if (strcmp(ROOT->Value(), "NAME") == 0)
										{
											field.name = ROOT->GetText();
										}
										if (strcmp(ROOT->Value(), "P1X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y1 = y;
										}
										if (strcmp(ROOT->Value(), "P1Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z1 = y;
										}
										if (strcmp(ROOT->Value(), "P2X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y2 = y;
										}
										if (strcmp(ROOT->Value(), "P2Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z2 = y;
										}
										if (strcmp(ROOT->Value(), "P3X") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.x3 = y;
										}
										if (strcmp(ROOT->Value(), "P3Y") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.y3 = y;
										}
										if (strcmp(ROOT->Value(), "P3Z") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.z3 = y;
										}
										if (strcmp(ROOT->Value(), "L") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.l = y;
										}
										if (strcmp(ROOT->Value(), "H") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.h = y;
										}
										if (strcmp(ROOT->Value(), "W") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.w = y;
										}
										if (strcmp(ROOT->Value(), "ANGLE") == 0)
										{
											const char* str = ROOT->GetText();
											char str2[32];
											double y = 0;
											if (strchr(str, (int) '.') != NULL)
											{
												strcpy(str2, str);
												*strchr(str2, (int) '.') = ',';
												y = strtod(str2, NULL);
											}
											else
											{
												y = strtod(str, NULL);
											}
											field.angle = y;
										}
									}
									else
									{
										AllCool = false;
										break;
									}
								}
								field.floor1 = -1;
								field.floor2 = -1;
								flight.push_back(field);
							}
						}
					}
				}
				while (f);
				if (!AllCool)
				{
					MessageBox::Show("� ����������� ����� ������������ ������", "Error");
				}
				//������� �����!
				box[it].pop_back();
				
				for (int i = 0; i <= it; i++)
				{
					for (vector <TBox>::iterator j = box[i].begin(); j < box[i].end(); j++)
					{
						if (j->x1 < min_x_box)
							min_x_box = j->x1;
						if (j->x2 < min_x_box)
							min_x_box = j->x2;
						if (j->y1 < min_y_box)
							min_y_box = j->y1;
						if (j->y2 < min_y_box)
							min_y_box = j->y2;
						if (j->x1 > max_x_box)
							max_x_box = j->x1;
						if (j->x2 > max_x_box)
							max_x_box = j->x2;
						if (j->y1 > max_y_box)
							max_y_box = j->y1;
						if (j->y2 > max_y_box)
							max_y_box = j->y2;
					}
				}
				min_x_box2 = min_x_box;
				min_y_box2 = min_y_box;
				max_x_box2 = max_x_box;
				max_y_box2 = max_y_box;
				if (min_x_box < min_y_box)
				{
					min_y_box = min_x_box;
				}
				else
				{
					min_x_box = min_y_box;
				}
				if (max_x_box < max_y_box)
				{
					max_x_box = max_y_box;
				}
				else
				{
					max_y_box = max_x_box;
				}
				for (int i = 0; i < floor.size() - 1; i++)
				{
					int imin = i;
					for (int j = i + 1; j < floor.size(); j++)
						if (floor[j].number < floor[imin].number)
						{
							imin = j;
						}
					if (imin != i)
					{
						TFloor temp = floor[imin];
						floor[imin] = floor[i];
						floor[i] = temp;
						vector <TBox> temp2 = box[imin];
						box[imin] = box[i];
						box[i] = temp2;
						vector <TAperture> temp3 = aper[imin];
						aper[imin] = aper[i];
						aper[i] = temp3;
					}
				}
				for (int i = 0; i < floor.size(); i++)
				{
					if (floor[i].number == 1)
					{
						floor_number = i;
						break;
					}
				}
				for (vector <TPlatform>::iterator j = plat.begin(); j < plat.end(); j++)
				{
						for (int i = 0; i <= it; i++)
						{
							for (vector <TBox>::iterator k = box[i].begin(); k < box[i].end(); k++)
							{
								if (fabs(j->z1 - k->z1) < 0.00001 || fabs(j->z1 - k->z2) < 0.00001 || fabs(j->z2 - k->z2) < 0.00001 || fabs(j->z2 - k->z1) < 0.00001)
								{
									j->floor = i;
								}
							}
						}
				}
				for (vector <TFlight>::iterator j = flight.begin(); j < flight.end(); j++)
				{
						for (int i = 0; i <= it; i++)
						{
							for (vector <TBox>::iterator k = box[i].begin(); k < box[i].end(); k++)
							{
								if (fabs(j->z1 - k->z1) < 0.00001 || fabs(j->z1 - k->z2) < 0.00001 || fabs(j->z3 - k->z2) < 0.00001 || fabs(j->z3 - k->z1) < 0.00001)
								{
									if (j->floor1 == -1)
										j->floor1 = i;
									else
										if (j->floor1 != i)
											j->floor2 = i;
								}
							}
						}
				}
				for (int floor_number1 = 0; floor_number1 < floor.size(); floor_number1++)
				{
					for (vector <TAperture>::iterator j = aper[floor_number1].begin(); j <aper[floor_number1].end(); j++)
					{
						if (j->x2 < j->x1)
						{
							double temp = j->x1;
							j->x1 = j->x2;
							j->x2 = temp;
						}
						if (j->y2 < j->y1)
						{
							double temp = j->y1;
							j->y1 = j->y2;
							j->y2 = temp;
						}
					}
				}
				for (int floor_number1 = 0; floor_number1 < floor.size(); floor_number1++)
				{
					for (vector <TAperture>::iterator j = aper[floor_number1].begin(); j <aper[floor_number1].end(); j++)
					{
						if (j->tp == 0 || j->tp == 3)
						{
							TDoor field2;
							field2.id = j->id;
							field2.tp = j->tp;
							field2.x1 = j->x1;
							field2.y1 = j->y1;
							field2.z1 = j->z1;
							field2.x2 = j->x2;
							field2.y2 = j->y2;
							field2.z2 = j->z2;
							field2.marked = false;
							field2.floor = floor_number1;
							field2.box1 = j->box1;
							field2.box2 = j->box2;
							field2.lock = 0;
							field2.closer = 0;
							field2.antifire = 0;
							field2.angle = 0;
							door.push_back(field2);
						}
					}
				}
				/*cout << "APERTURES:" << endl;
				for (int i = 0; i <= it; i++)
				{
					for (vector <TAperture>::iterator j = aper[i].begin(); j <aper[i].end(); j++)
					{
						cout << "TAPERTURE#" << i << ": " << j->id << " " << j->tp << " " << j->name
							<< " " << j->x1 << " " << j->y1 << " " << j->z1 << " " << j->x2
							<< " " << j->y2 << " " << j->z2 << " " << j->box1 << " " << j->box2 << endl;
					}
				}*/
			}
		}
		}
		}
}



//������� ������������� ������ ���������
//���������:
//HDC hdc - handle device context
GLint Render::MySetPixelFormat(HDC hdc)
{
//	if (InputReady == true)
	{
		PIXELFORMATDESCRIPTOR pfd = { 
			sizeof(PIXELFORMATDESCRIPTOR),    // size of this pfd 
			1,                                // version number 
			PFD_DRAW_TO_WINDOW |              // support window 
			PFD_SUPPORT_OPENGL |              // support OpenGL 
			PFD_DOUBLEBUFFER,                 // double buffered 
			PFD_TYPE_RGBA,                    // RGBA type 
			24,                               // 24-bit color depth 
			0, 0, 0, 0, 0, 0,                 // color bits ignored 
			0,                                // no alpha buffer 
			0,                                // shift bit ignored 
			0,                                // no accumulation buffer 
			0, 0, 0, 0,                       // accum bits ignored 
			32,                               // 32-bit z-buffer     
			0,                                // no stencil buffer 
			0,                                // no auxiliary buffer 
			PFD_MAIN_PLANE,                   // main layer 
			0,                                // reserved 
			0, 0, 0                           // layer masks ignored 
		}; 
		
		GLint  iPixelFormat;
		if((iPixelFormat = ChoosePixelFormat(hdc, &pfd)) == 0)
		{
			printf("ChoosePixelFormat Failed");
			return 0;
		}
		if(SetPixelFormat(hdc, iPixelFormat, &pfd) == FALSE)
		{
			printf("SetPixelFormat Failed");
			return 0;
		}
		if((m_hglrc = wglCreateContext(m_hDC)) == NULL)
		{
			printf("wglCreateContext Failed");
			return 0;
		}	
		if((wglMakeCurrent(m_hDC, m_hglrc)) == NULL)
		{
			printf("wglMakeCurrent Failed");
			return 0;
		}
		glClearColor(1.0, 1.0, 1.0, 1.0);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WinWid_f, 0, WinHei_f, -100.0, 100.0);
		glMatrixMode(GL_MODELVIEW);
		glGenTextures(1, &tex);
		glFontCreate(&font, "glFontTimesNewRoman.glf", tex);
		int error = glGetError();
		return 1;
	}
}



//������� ������� ���������
void Render::Draw()
{
	if (BuildReady == true)
	{
		if (!action1)
		{
			Form1::Instance->toolStripButton4->Enabled = false;
		}
		else
		{
			Form1::Instance->toolStripButton4->Enabled = true;
		}
		if (CommonFieldWorks)
		{
			
			if (emptyField.id != FieldToCheck.id)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->id = emptyField.id;
					}
				}
			}
			if (emptyField.tp != FieldToCheck.tp)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->tp = emptyField.tp;
					}
				}
			}
			if (emptyField.start_time != FieldToCheck.start_time)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->start_time = emptyField.start_time;
					}
				}
			}
			if (emptyField.exit != FieldToCheck.exit)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->exit = emptyField.exit;
					}
				}
			}
			if (emptyField.door1 != FieldToCheck.door1)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->door1 = emptyField.door1;
					}
				}
			}
			if (emptyField.door2 != FieldToCheck.door2)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->door2 = emptyField.door2;
					}
				}
			}
			if (emptyField.door3 != FieldToCheck.door3)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->door3 = emptyField.door3;
					}
				}
			}
			if (emptyField.door4 != FieldToCheck.door4)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->door4 = emptyField.door4;
					}
				}
			}
			if (emptyField.door5 != FieldToCheck.door5)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->door5 = emptyField.door5;
					}
				}
			}
			if (fabs(emptyField.size - FieldToCheck.size) > 0.00001)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->size = emptyField.size;
					}
				}
			}
			if (emptyField.mobylity != FieldToCheck.mobylity)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->mobylity = emptyField.mobylity;
					}
				}
			}
			if (emptyField.age != FieldToCheck.age)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->age = emptyField.age;
					}
				}
			}
			if (emptyField.emostate != FieldToCheck.emostate)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->emostate = emptyField.emostate;
					}
				}
			}
			if (emptyField.sex != FieldToCheck.sex)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->sex = emptyField.sex;
					}
				}
			}
			if (emptyField.color != FieldToCheck.color)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->color = emptyField.color;
					}
				}
			}
			if (emptyField.control != FieldToCheck.control)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->control = emptyField.control;
					}
				}
			}
			if (emptyField.role != FieldToCheck.role)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->role = emptyField.role;
					}
				}
			}
			if (emptyField.educlevel != FieldToCheck.educlevel)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->educlevel = emptyField.educlevel;
					}
				}
			}
			if (emptyField.start_room != FieldToCheck.start_room)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->start_room = emptyField.start_room;
					}
				}
			}
			if (fabs(emptyField.x_real - FieldToCheck.x_real) > 0.00001)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->x_real = emptyField.x_real;
					}
				}
			}
			if (fabs(emptyField.y_real - FieldToCheck.y_real) > 0.00001)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->y_real = emptyField.y_real;
					}
				}
			}
			if (fabs(emptyField.z - FieldToCheck.z) > 0.00001)
			{
				Render::SaveAction();
				for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
				{
					if (i->object_is_in_selected_area == true)
					{
						i->z = emptyField.z;
					}
				}
			}
			FieldToCheck = emptyField;
			
		}

		for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
		{
			if (i->object_is_in_selected_area == true)
			{
				if (fabs((i->x2_real - i->x1_real) - i->width) > 0.000001 || fabs((i->y2_real - i->y1_real) - i->length) > 0.000001)
				{
					double x2_real_old = i->x2_real;
					double y2_real_old = i->y2_real;
					double x2_old = i->x2;
					double y2_old = i->y2;
					i->x2_real = i->x1_real + i->width;
					i->y2_real = i->y1_real + i->length;
					i->x2 = (i->x2_real - min_x_box) * WinWid_f / (max_x_box - min_x_box);
					i->y2 = (i->y2_real - min_y_box) * WinHei_f / (max_y_box - min_y_box);

					furniture field = *i;

					bool add_new_item = false;
					vector <TBox>::iterator j;
					for (j = box[floor_number].begin(); j < box[floor_number].end(); j++)
					{
						if (j->id == field.box_id)
							break;
					}
					if (field.x2_real < j->x2 && field.y2_real < j->y2)
					{
						add_new_item = true;
					}
					if (add_new_item)
					{
						//�������� �� ����������� � ������ �������
						for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
						{
							if (i->box_id == field.box_id && i->id != field.id)
								if (((field.x1_real <= i->x1_real && field.x2_real >= i->x1_real) || (field.x1_real <= i->x2_real && field.x2_real >= i->x2_real) || (field.x2_real <= i->x2_real && field.x1_real >= i->x1_real)) && ((field.y1_real <= i->y1_real && field.y2_real >= i->y1_real) || (field.y1_real <= i->y2_real && field.y2_real >= i->y2_real) || (field.y2_real <= i->y2_real && field.y1_real >= i->y1_real)))
								{
									add_new_item = false;
									break;
								}
						}
						for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
						{
							if (i->box_id == field.box_id)
								if (i->x_real + i->radius > field.x1_real && i->x_real - i->radius < field.x2_real && i->y_real + i->radius > field.y1_real && i->y_real - i->radius < field.y2_real)
								{
									add_new_item = false;
									break;
								}
						}
					}
					if (!add_new_item)
					{
						i->x2_real = x2_real_old;
						i->y2_real = y2_real_old;
						i->x2 = x2_old;
						i->y2 = y2_old;
						i->width = i->x2_real - i->x1_real;
						i->length = i->y2_real - i->y1_real;
						MessageBox::Show("������������ ��������� ��������!", "������");
					}
					else
					{
						double t1 = i->x2_real;
						double t2 = i->y2_real;
						double t3 = i->x2;
						double t4 = i->y2;
						double t5 = i->width;
						double t6 = i->length;
						i->x2_real = x2_real_old;
						i->y2_real = y2_real_old;
						i->x2 = x2_old;
						i->y2 = y2_old;
						i->width = i->x2_real - i->x1_real;
						i->length = i->y2_real - i->y1_real;
						Render::SaveAction();
						i->x2_real = t1;
						i->y2_real = t2;
						i->x2 = t3;
						i->y2 = t4;
						i->width = t5;
						i->length = t6;
					}
				}
				break;
			}
		}



		glClear(GL_COLOR_BUFFER_BIT);
		glPushMatrix();
		glTranslatef(WinWid_f / 2, WinHei_f / 2, 0);
		glColor3f(0.0, 0.0, 0.0);
		glScalef(Scale, Scale, 1.0);

		if (GridEnabled)
		{
			glColor3f(0.95, 0.95, 0.95);
			__int64 counterr = 0;
			double i = min_x_box2;
			do
			{
				counterr++;
				if (counterr % 10 == 0)
					glLineWidth(5);
				else
					glLineWidth(1);
				glBegin(GL_LINES);
				glVertex2f((i - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (max_y_box2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((i - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (min_y_box2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glEnd();
				i += 0.05;
			}
			while(i <= max_x_box2);
			i = min_y_box2;
			counterr = 0;
			do
			{
				counterr++;
				if (counterr % 10 == 0)
					glLineWidth(5);
				else
					glLineWidth(1);
				glBegin(GL_LINES);
				glVertex2f((min_x_box2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (i - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((max_x_box2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (i - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glEnd();
				i += 0.05;
			}
			while(i <= max_y_box2);
			glColor3f(0.0, 0.0, 0.0);
		}

		if (show_selected_area)
		{
			if (GridEnabled)
			{
				glColor4f(0.9, 0.9, 0.9, 0.001);
			}
			else
			{
				glColor4f(0.95, 0.95, 0.95, 0.001);
			}
			for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
			{
				if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x2 > x2_selected_real && j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y1 < y2_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 > x1_selected_real && j->x1 < x2_selected_real && j->x2 > x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 < x1_selected_real && j->x2 > x2_selected_real && j->y1 > y1_selected_real && j->y2 < y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x1_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((x2_selected_real - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
				if (j->x1 > x1_selected_real && j->x2 < x2_selected_real && j->y1 < y1_selected_real && j->y2 > y2_selected_real)
				{
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y2_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (y1_selected_real - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}
			}
		}

		glColor3f(0.0, 0.0, 0.0);

		glLineWidth(1);
		for (vector <TBox>::iterator j = box[floor_number].begin(); j < box[floor_number].end(); j++)
		{
			glBegin(GL_LINES);
			glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
			glEnd();
			if (show_ids)
			{
				glEnable(GL_TEXTURE_2D);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glFontBegin(&font);
	//			glScalef(8.0, 8.0, 1.0);
	//			glTranslatef(30, 30, 0);
				char temp_num[10];
				itoa(j->id, temp_num, 10);
				int bek = strlen(temp_num);
				glFontTextOut(temp_num, (((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom) + ((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom)) / 2. - 0.67 * bek, ((j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom + (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom) / 2 + 2.5, 0);
				glFontEnd();
				glFlush();
				glDisable(GL_BLEND);
				glDisable(GL_TEXTURE_2D);
				glDisable(GL_TEXTURE_COORD_ARRAY);
	//			glScalef(Scale, Scale, 1.0);
			}
		}
		for (vector <TPlatform>::iterator j = plat.begin(); j < plat.end(); j++)
		{
			if (j->floor == floor_number)
			{
				glBegin(GL_LINES);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glEnd();
				/*if (show_ids)
				{
					glEnable(GL_TEXTURE_2D);
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					glFontBegin(&font);
		//			glScalef(8.0, 8.0, 1.0);
		//			glTranslatef(30, 30, 0);
					char temp_num[10];
					itoa(j->id, temp_num, 10);
					glFontTextOut(temp_num, (j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, ((j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom + (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom) / 2, 0);
					glFontEnd();
					glFlush();
					glDisable(GL_BLEND);
					glDisable(GL_TEXTURE_2D);
					glDisable(GL_TEXTURE_COORD_ARRAY);
		//			glScalef(Scale, Scale, 1.0);
				}*/
			}
		}
		glColor3f(47.0 / 255.0, 79.0 / 255.0, 79.0 / 255.0);
		for (vector <TFlight>::iterator j = flight.begin(); j < flight.end(); j++)
		{
			if (j->floor1 == floor_number || j->floor2 == floor_number)
			{
				if (!(fabs(j->x1 - j->x2) < 0.00001))
				{
					for (int i = 0; i < 5; i++)
					{
						glBegin(GL_LINES);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * i / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x3 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * i / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x3 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * i / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x3 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * (i + 1) / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x3 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * (i + 1) / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * (i + 1) / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * (i + 1) / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * i / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glEnd();
					}
					/*if (show_ids)
					{
						glEnable(GL_TEXTURE_2D);
						glEnableClientState(GL_TEXTURE_COORD_ARRAY);
						glEnable(GL_BLEND);
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
						glFontBegin(&font);
			//			glScalef(8.0, 8.0, 1.0);
			//			glTranslatef(30, 30, 0);
						char temp_num[10];
						itoa(j->id, temp_num, 10);
						glFontTextOut(temp_num, (j->x3 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 + (j->y3 - j->y1) * 2 / 5. - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
						glFontEnd();
						glFlush();
						glDisable(GL_BLEND);
						glDisable(GL_TEXTURE_2D);
						glDisable(GL_TEXTURE_COORD_ARRAY);
			//			glScalef(Scale, Scale, 1.0);
					}*/
				}
				else
				{
					for (int i = 0; i < 5; i++)
					{
						glBegin(GL_LINES);
						glVertex2f((j->x1 + (j->x3 - j->x1) * i / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 + (j->x3 - j->x1) * (i + 1) / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 + (j->x3 - j->x1) * (i + 1) / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 + (j->x3 - j->x1) * (i + 1) / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y3 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 + (j->x3 - j->x1) * (i + 1) / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y3 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 + (j->x3 - j->x1) * i / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y3 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 + (j->x3 - j->x1) * i / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y3 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 + (j->x3 - j->x1) * i / 5. - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glEnd();
					}
					/*if (show_ids)
					{
						glEnable(GL_TEXTURE_2D);
						glEnableClientState(GL_TEXTURE_COORD_ARRAY);
						glEnable(GL_BLEND);
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
						glFontBegin(&font);
			//			glScalef(8.0, 8.0, 1.0);
			//			glTranslatef(30, 30, 0);
						char temp_num[10];
						itoa(j->id, temp_num, 10);
						glFontTextOut(temp_num, (j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
						glFontEnd();
						glFlush();
						glDisable(GL_BLEND);
						glDisable(GL_TEXTURE_2D);
						glDisable(GL_TEXTURE_COORD_ARRAY);
			//			glScalef(Scale, Scale, 1.0);
					}*/
				}
			}
		}
		if (show_apertures)
		{
			for (vector <TAperture>::iterator j = aper[floor_number].begin(); j <aper[floor_number].end(); j++)
			{
				switch (j->tp)
				{
				case 0:
					{
						bool kontur = false;
						for (vector <TDoor>::iterator dor = door.begin(); dor < door.end(); dor++)
						{
							if (j->id == dor->id)
							{
								if (dor->lock == 0)
								{
									glColor3f(1.0, 0.0, 1.0);
									if (dor->antifire != 0)
									{
										kontur = true;
									}
									if (dor->closer != 0)
									{
										kontur = true;
									}
								}
								else
								{
									glColor3f(1.0, 0.0, 0.0);
									kontur = true;
								}
								/*if (dor->antifire != 0)
								{
									glLineWidth(5);
								}
								else
								{
									glLineWidth(3);
								}*/
							}
						}
						if (fabs(j->x1 - j->x2) < 0.0001)
						{
							glBegin(GL_POLYGON);
							glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glEnd();
							if (kontur)
							{
								glColor3f(0.0, 0.0, 0.0);
								glBegin(GL_LINES);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glEnd();
							}
						}
						else
						{
							glBegin(GL_POLYGON);
							glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glEnd();
							if (kontur)
							{
								glColor3f(0.0, 0.0, 0.0);
								glBegin(GL_LINES);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glEnd();
							}
						}
					}
					break;
				case 1:
					{
						glColor3f(0.0, 0.0, 1.0);
						if (fabs(j->x1 - j->x2) < 0.0001)
						{
							glBegin(GL_POLYGON);
							glVertex2f((j->x1 - 0.05 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x1 - 0.05 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 + 0.05 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 + 0.05 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glEnd();
						}
						else
						{
							glBegin(GL_POLYGON);
							glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.05 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.05 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.05 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.05 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glEnd();
						}
					}
					break;
				case 2:
					{
						glColor3f(0.75, 0.75, 0.75);
						glBegin(GL_LINES);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
						glEnd();
					}
					break;
				case 3:
					{
						bool kontur = false;
						for (vector <TDoor>::iterator dor = door.begin(); dor < door.end(); dor++)
						{
							if (j->id == dor->id)
							{
								if (dor->lock == 0)
								{
									glColor3f(0.0, 1.0, 0.0);
									if (dor->antifire != 0)
									{
										kontur = true;
									}
									if (dor->closer != 0)
									{
										kontur = true;
									}
								}
								else
								{
									glColor3f(1.0, 0.0, 0.0);
									kontur = true;
								}
								/*if (dor->antifire != 0)
								{
									glLineWidth(5);
								}
								else
								{
									glLineWidth(3);
								}*/
							}
						}
						if (fabs(j->x1 - j->x2) < 0.0001)
						{
							glBegin(GL_POLYGON);
							glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glEnd();
							if (kontur)
							{
								glColor3f(0.0, 0.0, 0.0);
								glBegin(GL_LINES);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glEnd();
							}
						}
						else
						{
							glBegin(GL_POLYGON);
							glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
							glEnd();
							if (kontur)
							{
								glColor3f(0.0, 0.0, 0.0);
								glBegin(GL_LINES);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
								glEnd();
							}
						}
					}
					break;
				}
				
				/*glBegin(GL_LINES);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x2 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glVertex2f((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom);
				glEnd();*/
				if (show_ids)
				{
					if (j->tp != 1)
					{
			//			glRotatef(90.0, 0.0, 0.0, 1.0);
			//			glTranslatef((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, ((j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom + (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom) / 2 + 2.5, 0);
			//			glRotatef(90.0, 0.0, 0.0, 1.0);
			//			glScalef(1.0, 5.0, 1.0);
						glEnable(GL_TEXTURE_2D);
						glEnableClientState(GL_TEXTURE_COORD_ARRAY);
						glEnable(GL_BLEND);
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
						
						glFontBegin(&font);
			//			glTranslatef(30, 30, 0);
						char temp_num[10];
						itoa(j->id, temp_num, 10);
						glFontTextOut(temp_num, (j->x1 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, ((j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom + (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom) / 2 + 2.5, 0);
//						glFontTextOut(temp_num, 0, 0, 0);
						glFontEnd();
						glFlush();
						
						glDisable(GL_BLEND);
						glDisable(GL_TEXTURE_2D);
						glDisable(GL_TEXTURE_COORD_ARRAY);
			//			glRotatef(-90.0, 0.0, 0.0, 1.0);
			//			glTranslatef(-((j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom), -(((j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom + (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom) / 2 + 2.5), 0);
			//			glScalef(1.0, 1.0 / 5.0, 1.0);
						for (vector <TPorta>::iterator kk = port.begin(); kk < port.end(); kk++)
						{
							if (j->id == kk->id1)
							{
								glColor3f(0.0, 0.0, 1.0);
								glEnable(GL_TEXTURE_2D);
								glEnableClientState(GL_TEXTURE_COORD_ARRAY);
								glEnable(GL_BLEND);
								glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
								glFontBegin(&font);
					//			glScalef(8.0, 8.0, 1.0);
					//			glTranslatef(30, 30, 0);
								char temp_num[10];
								itoa(kk->id, temp_num, 10);
								glFontTextOut(temp_num, (j->x1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, ((j->y1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom + (j->y2 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom) / 2 + 5, 0);
								glFontEnd();
								glFlush();
								glDisable(GL_BLEND);
								glDisable(GL_TEXTURE_2D);
								glDisable(GL_TEXTURE_COORD_ARRAY);
							}
						}
					}
				}
			}
		}
		glLineWidth(1);
		for (vector <furniture>::iterator i = furn.begin(); i < furn.end(); i++)
		{
			if (i->floor == floor_number)
			{
		
				if (i->marked == false)
				{
					switch (i->tp)
					{
					case 1: glColor3f(139. / 256., 69. / 256., 19. / 256.);
						break;
					case 2: glColor3f(160. / 256., 82. / 256., 45. / 256.);
						break;
					case 3: glColor3f(210. / 256., 105. / 256., 30. / 256.);
						break;
					case 5: glColor3f(0.5, 0.5, 0.5);
						break;
					}
					glBegin(GL_POLYGON);
					glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);// ax / Scale - dx * WinWid / WinWid_f
					glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);// (WinHei - ay) / Scale + dy * WinHei / WinHei_f
					glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
					glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
					glEnd();
					if (i->object_is_in_selected_area)
					{
						glColor3f(0.0, 0.0, 0.0);
						glBegin(GL_POLYGON);
						glVertex3f((i->x1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();
					}
				}
				else
				{
					if (i->object_is_in_selected_area)
					{
						glColor3f(0.0, 0.0, 0.0);
						glBegin(GL_POLYGON);
						glVertex3f((i->x1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x2 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glEnd();
					}
					switch (i->tp)
					{
					case 1: glColor3f(139. / 256., 69. / 256., 19. / 256.);
						break;
					case 2: glColor3f(160. / 256., 82. / 256., 45. / 256.);
						break;
					case 3: glColor3f(210. / 256., 105. / 256., 30. / 256.);
						break;
					case 5: glColor3f(0.5, 0.5, 0.5);
						break;
					}
					if (furn_selected)
					{
						glBegin(GL_POLYGON);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);// ax / Scale - dx * WinWid / WinWid_f
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);// (WinHei - ay) / Scale + dy * WinHei / WinHei_f
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_mebel, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_mebel, 0);
						glEnd();
					}
					else
					{
						glBegin(GL_POLYGON);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);// ax / Scale - dx * WinWid / WinWid_f
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y1 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);// (WinHei - ay) / Scale + dy * WinHei / WinHei_f
						glVertex3f((i->x2 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();
					}
				}
			}
		}
		for (vector <people>::iterator i = peop.begin(); i < peop.end(); i++)
		{
			if (i->floor == floor_number)
			{
				GLfloat theta;
				GLfloat pi     = acos(-1.0);
				GLfloat rad1 = i->radius * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid;
				GLfloat rad2 = i->radius * WinWid_f / (max_x_box - min_x_box) * WinHei_f / WinHei;
				GLfloat step   = 6.0f;

				
				if (i->marked == false)
				{
					double red = 0.0, green = 0.0, blue = 0.0;
					__int64 color;
					color = i->color;
					red += color % 16;
					color = color / 16;
					red += (color % 16) * 16;
					color = color / 16;
					green += color % 16;
					color = color / 16;
					green += (color % 16) * 16;
					color = color / 16;
					blue += color % 16;
					color = color / 16;
					blue += (color % 16) * 16;
					glColor3f(red / 256., green / 256., blue / 256.);
					glBegin(GL_TRIANGLE_FAN);
					for(GLfloat a = 0.0f; a < 360.0f; a += step)
					{
						theta = 2.0f * pi * a / 180.0f;
						glVertex3f((i->x + x * WinWid / WinWid_f)/ WinWid * WinWid_f + rad1 * cos(theta), (i->y - y * WinHei / WinHei_f)/ WinHei * WinHei_f + rad2 * sin(theta), 0.0f);
					}
					glEnd();
					if (i->object_is_in_selected_area)
					{
						glColor3f(0.0, 0.0, 0.0);
						glBegin(GL_POLYGON);
						glVertex3f((i->x - rad1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x - rad1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x - rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glVertex3f((i->x - rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f, 0);
						glEnd();
					}
				}
				else
				{
					if (i->object_is_in_selected_area)
					{
						glColor3f(0.0, 0.0, 0.0);
						glBegin(GL_POLYGON);
						glVertex3f((i->x - rad1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - 0 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x - rad1 - 0 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y - rad2 - 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x + rad1 + 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x + rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glEnd();

						glBegin(GL_POLYGON);
						glVertex3f((i->x - rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x - rad1 - 1.6 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glVertex3f((i->x - rad1 + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers, (i->y + rad2 + 1.6 - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers, 0);
						glEnd();
					}
					double red = 0.0, green = 0.0, blue = 0.0;
					__int64 color;
					color = i->color;
					red += color % 16;
					color = color / 16;
					red += (color % 16) * 16;
					color = color / 16;
					green += color % 16;
					color = color / 16;
					green += (color % 16) * 16;
					color = color / 16;
					blue += color % 16;
					color = color / 16;
					blue += (color % 16) * 16;
					glColor3f(red / 256., green / 256., blue / 256.);
					if (person_selected)
					{
						glBegin(GL_TRIANGLE_FAN);
						for(GLfloat a = 0.0f; a < 360.0f; a += step)
						{
							theta = 2.0f * pi * a / 180.0f;
							glVertex3f((i->x + x * WinWid / WinWid_f)/ WinWid * WinWid_f + x_pers + rad1 * cos(theta), (i->y - y * WinHei / WinHei_f)/ WinHei * WinHei_f - y_pers + rad2 * sin(theta), 0.0f);
						}
						glEnd();
					}
					else
					{
						glBegin(GL_TRIANGLE_FAN);
						for(GLfloat a = 0.0f; a < 360.0f; a += step)
						{
							theta = 2.0f * pi * a / 180.0f;
							glVertex3f((i->x + x * WinWid / WinWid_f)/ WinWid * WinWid_f + rad1 * cos(theta), (i->y - y * WinHei / WinHei_f)/ WinHei * WinHei_f + rad2 * sin(theta), 0.0f);
						}
						glEnd();
					}
				}
			}
		}
		if (furniture_mode)
		{
			glBegin(GL_POLYGON);
			glColor3f(0.59, 0.29, 0.0);
			glVertex3f(x_start - WinWid_f / 2 / Scale, WinHei_f / Scale - y_start - WinHei_f / 2 / Scale, 0);
			glVertex3f(x_start - WinWid_f / 2 / Scale + x_furn, WinHei_f / Scale - y_start - WinHei_f / 2 / Scale, 0);
			glVertex3f(x_start - WinWid_f / 2 / Scale + x_furn, WinHei_f / Scale - (y_start + WinHei_f / 2 / Scale + y_furn), 0);
			glVertex3f(x_start - WinWid_f / 2 / Scale, WinHei_f / Scale - (y_start + WinHei_f / 2 / Scale + y_furn), 0);
			glEnd();
		}

		if (selecting_mode)
		{
			glBegin(GL_POLYGON);
			if (GridEnabled)
			{
				glColor4f(0.9, 0.9, 0.9, 0.001);
			}
			else
			{
				glColor4f(0.95, 0.95, 0.95, 0.001);
			}
			glVertex3f(x_start - WinWid_f / 2 / Scale, WinHei_f / Scale - y_start - WinHei_f / 2 / Scale, 0);
			glVertex3f(x_start - WinWid_f / 2 / Scale + x_select, WinHei_f / Scale - y_start - WinHei_f / 2 / Scale, 0);
			glVertex3f(x_start - WinWid_f / 2 / Scale + x_select, WinHei_f / Scale - (y_start + WinHei_f / 2 / Scale + y_select), 0);
			glVertex3f(x_start - WinWid_f / 2 / Scale, WinHei_f / Scale - (y_start + WinHei_f / 2 / Scale + y_select), 0);
			glEnd();
		}

		if (marking_mode)
		{
			for (vector <TDoor>::iterator j = door.begin(); j < door.end(); j++)
			{
				if (j->marked == true && j->floor == floor_number)
				{
					glColor3f(1.0, 0.0, 0.0);
					glBegin(GL_POLYGON);
					glVertex3f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x1 - 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y2 + 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glVertex3f((j->x2 + 0.1 - min_x_box) * WinWid_f / (max_x_box - min_x_box) * WinWid_f / WinWid+x+x_zoom, (j->y1 - 0.1 - min_y_box) * WinHei_f / (max_y_box - min_y_box) * WinHei_f / WinHei-y-y_zoom, 0);
					glEnd();
				}					
			}
		}
			
		glPopMatrix();
		SwapOpenGLBuffers();
		}
		else
		{
			if (!WeCalledLoadProcess)
			{
				WeCalledLoadProcess = true;
				LoadBuild();
			}
			BuildReady = true;
			WeCalledLoadProcess = false;
		}
}